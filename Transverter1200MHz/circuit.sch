<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Parts" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Accent_neu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="NXP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="NXP_2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-coax">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="J502-ND">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; End Launch Jack Receptacle - Round Contact&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="-4.65" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.85" x2="-4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-3.5" x2="-4.9" y2="-1.8" width="0" layer="46"/>
<wire x1="-4.9" y1="-1.8" x2="-3.6" y2="-1.8" width="0" layer="46" curve="-180"/>
<wire x1="-3.6" y1="-1.8" x2="-3.6" y2="-3.5" width="0" layer="46"/>
<wire x1="-3.6" y1="-3.5" x2="-4.9" y2="-3.5" width="0" layer="46" curve="-180"/>
<wire x1="3.6" y1="-3.5" x2="3.6" y2="-1.8" width="0" layer="46"/>
<wire x1="3.6" y1="-1.8" x2="4.9" y2="-1.8" width="0" layer="46" curve="-180"/>
<wire x1="4.9" y1="-1.8" x2="4.9" y2="-3.5" width="0" layer="46"/>
<wire x1="4.9" y1="-3.5" x2="3.6" y2="-3.5" width="0" layer="46" curve="-180"/>
<circle x="0" y="0" radius="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1"/>
<pad name="2" x="-4.25" y="0.4" drill="1.5"/>
<pad name="3" x="4.25" y="0.4" drill="1.5"/>
<pad name="4" x="4.25" y="-2.65" drill="1.3" shape="long" rot="R90"/>
<pad name="5" x="-4.25" y="-2.65" drill="1.3" shape="long" rot="R90"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="J502-ND-142-0701-801/806">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.062&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<smd name="1" x="2.5654" y="0" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@1" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@2" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@4" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="16"/>
<smd name="G@3" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="J502-ND-142-0701-851/861">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.062&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<smd name="1" x="2.5654" y="0" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@1" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@2" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@4" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="16"/>
<smd name="G@3" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="J502-ND-142-0701-871/876">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.062&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<smd name="1" x="2.5654" y="0" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@1" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@2" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@4" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="16"/>
<smd name="G@3" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="J502-ND-142-0701-881/886">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.031&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<smd name="1" x="2.5654" y="0" dx="5.08" dy="1.143" layer="1"/>
<smd name="G@1" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@2" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@4" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="16"/>
<smd name="G@3" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="J502-ND-142-0711-821/826">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.062&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.1" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-4.65" x2="4.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="3.85" x2="4.1" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.1" y1="3.85" x2="4.1" y2="4.65" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<smd name="1" x="2.1154" y="0" dx="4.191" dy="1.778" layer="1"/>
<smd name="G@1" x="2.1154" y="3.4946" dx="4.191" dy="2.667" layer="1"/>
<smd name="G@2" x="2.1154" y="-3.4946" dx="4.191" dy="2.667" layer="1"/>
<smd name="G@4" x="2.1154" y="-3.4946" dx="4.191" dy="2.667" layer="16"/>
<smd name="G@3" x="2.1154" y="3.4946" dx="4.191" dy="2.667" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="J502-ND-142-0711-871/876">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.047&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<smd name="1" x="2.5654" y="0" dx="5.08" dy="1.905" layer="1"/>
<smd name="G@1" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@2" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@4" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="16"/>
<smd name="G@3" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="J502-ND-142-0711-881/886">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; Board Thickness &lt;b&gt;0.047&lt;/b&gt; Inch, End Launch Jack Receptacle&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<wire x1="0" y1="5.588" x2="0" y2="-5.588" width="0" layer="20"/>
<wire x1="-1.55" y1="-4.65" x2="-1.55" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-2" x2="-3.7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-2" x2="-4.45" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-2.75" x2="-8.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-2.75" x2="-8.25" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-1.9" x2="-9.425" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="-1.9" x2="-9.425" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-9.425" y1="1.9" x2="-8.25" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="1.9" x2="-8.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="2.75" x2="-4.45" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="2.75" x2="-3.7" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="2" x2="-1.55" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="2" x2="-1.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-3.85" x2="-0.1" y2="3.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="-0.1" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-1.55" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="4.65" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.85" x2="-0.1" y2="-3.85" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="3.85" x2="4.65" y2="3.85" width="0.2032" layer="51"/>
<wire x1="4.65" y1="3.85" x2="4.65" y2="4.65" width="0.2032" layer="51"/>
<smd name="1" x="2.5654" y="0" dx="5.08" dy="1.905" layer="1"/>
<smd name="G@1" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@2" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="1"/>
<smd name="G@4" x="2.5654" y="-4.4196" dx="5.08" dy="2.286" layer="16"/>
<smd name="G@3" x="2.5654" y="4.4196" dx="5.08" dy="2.286" layer="16"/>
<text x="0.635" y="6.35" size="1.778" layer="25">&gt;NAME</text>
<text x="1.27" y="-8.89" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.025" y1="-0.5" x2="4.05" y2="0.5" layer="51"/>
</package>
<package name="SMA-142-0701-871">
<description>&lt;b&gt;SMA Connector&lt;/b&gt; HF-Steckverbinder PC END BLKHD JCK GLD .062" BOARDS&lt;p&gt;
Hersteller: Emerson / Johnson&lt;br&gt;
Source: &lt;a href="http://emersonconnectivity.com/OA_MEDIA/drawings/dr-1420701871.pdf"&gt; Data sheet&lt;a&gt;</description>
<wire x1="-4.65" y1="4.65" x2="4.65" y2="4.65" width="0.2032" layer="21"/>
<wire x1="4.65" y1="4.65" x2="4.65" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-4.65" x2="-4.65" y2="-4.65" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-4.65" x2="-4.65" y2="4.65" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="1.7" x2="-4.65" y2="4.65" width="0.2032" layer="21"/>
<wire x1="4.65" y1="4.65" x2="4.65" y2="1.7" width="0.2032" layer="21"/>
<wire x1="2.745" y1="-4.65" x2="-2.745" y2="-4.65" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.2378" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.4199" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.762" diameter="1.524"/>
<pad name="2@1" x="-4.2545" y="0" drill="1.524" diameter="2.54"/>
<pad name="2@2" x="4.2545" y="0" drill="1.524" diameter="2.54"/>
<pad name="2@3" x="-4.2545" y="-3.1496" drill="2.159" diameter="2.794"/>
<pad name="2@4" x="4.2545" y="-3.1496" drill="2.159" diameter="2.794"/>
<text x="-5.08" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.775" y1="-0.5" x2="-3.75" y2="0.5" layer="51"/>
<rectangle x1="-4.775" y1="-3.2432" x2="-3.75" y2="-2.2432" layer="51"/>
<rectangle x1="-4.775" y1="-4.0814" x2="-3.75" y2="-3.0814" layer="51"/>
<rectangle x1="3.75" y1="-0.5" x2="4.775" y2="0.5" layer="51" rot="R180"/>
<rectangle x1="3.75" y1="-3.2432" x2="4.775" y2="-2.2432" layer="51" rot="R180"/>
<rectangle x1="3.75" y1="-4.0814" x2="4.775" y2="-3.0814" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="BNC-FGND">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMA-" prefix="X">
<description>&lt;b&gt;SMA 50 Ohm&lt;/b&gt; End Launch Jack Receptacle - Round Contact&lt;p&gt;
Johnson Components&lt;br&gt;
Source: www.johnsoncomponents.com .. J502-ND.pdf</description>
<gates>
<gate name="G1" symbol="BNC-FGND" x="0" y="0"/>
</gates>
<devices>
<device name="J502-ND" package="J502-ND">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="2 3 4 5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0701-801/806" package="J502-ND-142-0701-801/806">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0701-801" constant="no"/>
<attribute name="OC_FARNELL" value="1608592" constant="no"/>
<attribute name="OC_NEWARK" value="90F2624" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0701-851/861" package="J502-ND-142-0701-851/861">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0701-851" constant="no"/>
<attribute name="OC_FARNELL" value="1019325" constant="no"/>
<attribute name="OC_NEWARK" value="99F6798" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0701-871/876" package="J502-ND-142-0701-871/876">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0701-871" constant="no"/>
<attribute name="OC_FARNELL" value="1339835" constant="no"/>
<attribute name="OC_NEWARK" value="91B6105" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0701-881/886" package="J502-ND-142-0701-881/886">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0701-881" constant="no"/>
<attribute name="OC_FARNELL" value="1608603" constant="no"/>
<attribute name="OC_NEWARK" value="08B906" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0711-821/826" package="J502-ND-142-0711-821/826">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0711-821" constant="no"/>
<attribute name="OC_FARNELL" value="1019328" constant="no"/>
<attribute name="OC_NEWARK" value="16F3627" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0711-871/876" package="J502-ND-142-0711-871/876">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0711-871" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="01C1639" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0711-881/886" package="J502-ND-142-0711-881/886">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="G@1 G@2 G@3 G@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0711-881" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="50B4464" constant="no"/>
</technology>
</technologies>
</device>
<device name="142-0701-871/880" package="SMA-142-0701-871">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="2@1 2@2 2@3 2@4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="JOHNSON COMPONENTS" constant="no"/>
<attribute name="MPN" value="142-0701-871" constant="no"/>
<attribute name="OC_FARNELL" value="1339835" constant="no"/>
<attribute name="OC_NEWARK" value="91B6105" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HMC574AMS8E">
<description>&lt;ANALOG DEVICES - HMC574AMS8E - RF SWITCH, SPDT, DC-3GHZ, MSOP-8&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOP65P488X110-8N">
<description>&lt;b&gt;HMC574AMS8E&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.2" y="0.975" dx="1.4" dy="0.45" layer="1"/>
<smd name="2" x="-2.2" y="0.325" dx="1.4" dy="0.45" layer="1"/>
<smd name="3" x="-2.2" y="-0.325" dx="1.4" dy="0.45" layer="1"/>
<smd name="4" x="-2.2" y="-0.975" dx="1.4" dy="0.45" layer="1"/>
<smd name="5" x="2.2" y="-0.975" dx="1.4" dy="0.45" layer="1"/>
<smd name="6" x="2.2" y="-0.325" dx="1.4" dy="0.45" layer="1"/>
<smd name="7" x="2.2" y="0.325" dx="1.4" dy="0.45" layer="1"/>
<smd name="8" x="2.2" y="0.975" dx="1.4" dy="0.45" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.15" y1="1.8" x2="3.15" y2="1.8" width="0.05" layer="51"/>
<wire x1="3.15" y1="1.8" x2="3.15" y2="-1.8" width="0.05" layer="51"/>
<wire x1="3.15" y1="-1.8" x2="-3.15" y2="-1.8" width="0.05" layer="51"/>
<wire x1="-3.15" y1="-1.8" x2="-3.15" y2="1.8" width="0.05" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0.85" x2="-0.85" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.15" y1="1.5" x2="1.15" y2="1.5" width="0.2" layer="21"/>
<wire x1="1.15" y1="1.5" x2="1.15" y2="-1.5" width="0.2" layer="21"/>
<wire x1="1.15" y1="-1.5" x2="-1.15" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-1.15" y1="-1.5" x2="-1.15" y2="1.5" width="0.2" layer="21"/>
<wire x1="-2.9" y1="1.55" x2="-1.5" y2="1.55" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="HMC574AMS8E">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="A" x="0" y="0" length="middle"/>
<pin name="B" x="0" y="-2.54" length="middle"/>
<pin name="RFC" x="0" y="-5.08" length="middle"/>
<pin name="VDD" x="0" y="-7.62" length="middle"/>
<pin name="RF2" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="GND_2" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="GND_1" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="RF1" x="27.94" y="-7.62" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HMC574AMS8E" prefix="IC">
<description>&lt;b&gt;ANALOG DEVICES - HMC574AMS8E - RF SWITCH, SPDT, DC-3GHZ, MSOP-8&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/HMC574AMS8E.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="HMC574AMS8E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P488X110-8N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="GND_1" pad="6"/>
<connect gate="G$1" pin="GND_2" pad="7"/>
<connect gate="G$1" pin="RF1" pad="5"/>
<connect gate="G$1" pin="RF2" pad="8"/>
<connect gate="G$1" pin="RFC" pad="3"/>
<connect gate="G$1" pin="VDD" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="ANALOG DEVICES - HMC574AMS8E - RF SWITCH, SPDT, DC-3GHZ, MSOP-8" constant="no"/>
<attribute name="HEIGHT" value="1.1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Analog Devices" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="HMC574AMS8E" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="584-HMC574AMS8E" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=584-HMC574AMS8E" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Robotech">
<description>&lt;Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT65P210X110-6N">
<description>&lt;b&gt;SOT - 363&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="-1.1" y="0.65" dx="0.95" dy="0.4" layer="1"/>
<smd name="2" x="-1.1" y="0" dx="0.95" dy="0.4" layer="1"/>
<smd name="3" x="-1.1" y="-0.65" dx="0.95" dy="0.4" layer="1"/>
<smd name="4" x="1.1" y="-0.65" dx="0.95" dy="0.4" layer="1"/>
<smd name="5" x="1.1" y="0" dx="0.95" dy="0.4" layer="1"/>
<smd name="6" x="1.1" y="0.65" dx="0.95" dy="0.4" layer="1"/>
<text x="0" y="3.81" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="27" rot="R180" align="center">&gt;VALUE</text>
<wire x1="-0.7" y1="-1.075" x2="0.7" y2="-1.075" width="0.001" layer="51"/>
<wire x1="0.7" y1="-1.075" x2="0.7" y2="1.075" width="0.001" layer="51"/>
<wire x1="0.7" y1="1.075" x2="-0.7" y2="1.075" width="0.001" layer="51"/>
<wire x1="-0.7" y1="1.075" x2="-0.7" y2="-1.075" width="0.001" layer="51"/>
<wire x1="-1.85" y1="-1.35" x2="1.85" y2="-1.35" width="0.05" layer="51"/>
<wire x1="1.85" y1="-1.35" x2="1.85" y2="1.35" width="0.05" layer="51"/>
<wire x1="1.85" y1="1.35" x2="-1.85" y2="1.35" width="0.05" layer="51"/>
<wire x1="-1.85" y1="1.35" x2="-1.85" y2="-1.35" width="0.05" layer="51"/>
<circle x="0" y="0" radius="0.35" width="0.05" layer="21"/>
<wire x1="0" y1="-0.5" x2="0" y2="0.5" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.05" layer="51"/>
<wire x1="-0.525" y1="1" x2="-0.625" y2="0.9" width="0.1" layer="51"/>
<wire x1="-0.425" y1="1" x2="-0.625" y2="0.8" width="0.1" layer="51"/>
<wire x1="-0.325" y1="1" x2="-0.625" y2="0.7" width="0.1" layer="51"/>
<wire x1="-0.225" y1="1" x2="-0.625" y2="0.6" width="0.1" layer="51"/>
<wire x1="-0.125" y1="1" x2="-0.625" y2="0.5" width="0.1" layer="51"/>
<wire x1="-0.025" y1="1" x2="-0.625" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.075" y1="1" x2="-0.625" y2="0.3" width="0.1" layer="51"/>
<wire x1="0.175" y1="1" x2="-0.625" y2="0.2" width="0.1" layer="51"/>
<wire x1="0.275" y1="1" x2="-0.625" y2="0.1" width="0.1" layer="51"/>
<wire x1="0.375" y1="1" x2="-0.625" y2="0" width="0.1" layer="51"/>
<wire x1="-0.625" y1="-1" x2="0.625" y2="-1" width="0.1" layer="51"/>
<wire x1="0.625" y1="-1" x2="0.625" y2="1" width="0.1" layer="51"/>
<wire x1="0.625" y1="1" x2="-0.625" y2="1" width="0.1" layer="51"/>
<wire x1="-0.625" y1="1" x2="-0.625" y2="-1" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-1" x2="0.3" y2="-1" width="0.2" layer="21"/>
<wire x1="0.3" y1="-1" x2="0.3" y2="1" width="0.2" layer="21"/>
<wire x1="0.3" y1="1" x2="-0.3" y2="1" width="0.2" layer="21"/>
<wire x1="-0.3" y1="1" x2="-0.3" y2="-1" width="0.2" layer="21"/>
<circle x="-1.3" y="1.35" radius="0.125" width="0.25" layer="25"/>
</package>
<package name="SOT343R" urn="urn:adsk.eagle:footprint:29697/1">
<description>&lt;b&gt;Small Outline Transistor; reverse pinning; 4 leads&lt;/b&gt;&lt;p&gt;
Philips Semiconductors, SOT343R.pdf</description>
<wire x1="-1" y1="0.55" x2="1" y2="0.55" width="0.2032" layer="51"/>
<wire x1="1" y1="0.55" x2="1" y2="-0.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-0.55" x2="-1" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-1" y1="-0.55" x2="-1" y2="0.55" width="0.2032" layer="21"/>
<smd name="1" x="0.5" y="-0.9" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="-0.65" y="-0.9" dx="0.6" dy="0.8" layer="1"/>
<smd name="3" x="-0.65" y="0.9" dx="0.6" dy="0.8" layer="1"/>
<smd name="4" x="0.65" y="0.9" dx="0.6" dy="0.8" layer="1"/>
<text x="-1.1" y="1.45" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.1" y="-2.7" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.15" y1="-1.1" x2="0.85" y2="-0.6" layer="51"/>
<rectangle x1="-0.85" y1="-1.1" x2="-0.45" y2="-0.6" layer="51"/>
<rectangle x1="-0.85" y1="0.6" x2="-0.45" y2="1.1" layer="51"/>
<rectangle x1="0.45" y1="0.6" x2="0.85" y2="1.1" layer="51"/>
</package>
<package name="SOT363_PHILIPS" urn="urn:adsk.eagle:footprint:29648/1">
<description>&lt;b&gt;Small Outline Transistor; 6 leads&lt;/b&gt;&lt;p&gt;
Philips Semiconductors, SOT363.pdf</description>
<wire x1="-1" y1="0.55" x2="1" y2="0.55" width="0.2032" layer="51"/>
<wire x1="1" y1="0.55" x2="1" y2="-0.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-0.55" x2="-1" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-1" y1="-0.55" x2="-1" y2="0.55" width="0.2032" layer="21"/>
<circle x="-0.7" y="-0.25" radius="0.15" width="0" layer="21"/>
<smd name="1" x="-0.65" y="-0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="2" x="0" y="-0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="3" x="0.65" y="-0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="4" x="0.65" y="0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="5" x="0" y="0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="6" x="-0.65" y="0.8" dx="0.4" dy="0.8" layer="1"/>
<text x="-1.1" y="1.45" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.1" y="-2.7" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.1" x2="-0.5" y2="-0.6" layer="51"/>
<rectangle x1="-0.15" y1="-1.1" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="0.5" y1="-1.1" x2="0.8" y2="-0.6" layer="51"/>
<rectangle x1="0.5" y1="0.6" x2="0.8" y2="1.1" layer="51"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="1.1" layer="51"/>
<rectangle x1="-0.8" y1="0.6" x2="-0.5" y2="1.1" layer="51"/>
</package>
<package name="M67715">
<smd name="1" x="8.5" y="-0.5" dx="9" dy="3" layer="1" rot="R90"/>
<smd name="2" x="13.5" y="-0.5" dx="9" dy="3" layer="1" rot="R90"/>
<smd name="3" x="22.5" y="-0.5" dx="9" dy="3" layer="1" rot="R90"/>
<smd name="4" x="31.5" y="-0.5" dx="9" dy="3" layer="1" rot="R90"/>
<smd name="5" x="36.5" y="-0.5" dx="9" dy="3" layer="1" rot="R90"/>
<pad name="6" x="1.5" y="11" drill="3" diameter="4" shape="square"/>
<pad name="7" x="43.5" y="11" drill="3" diameter="4" shape="square"/>
<wire x1="4" y1="5" x2="4" y2="17" width="0.127" layer="20"/>
<wire x1="4" y1="17" x2="41" y2="17" width="0.127" layer="20"/>
<wire x1="41" y1="17" x2="41" y2="5" width="0.127" layer="20"/>
<wire x1="41" y1="5" x2="4" y2="5" width="0.127" layer="20"/>
<text x="-1.27" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOT223">
<description>&lt;b&gt;Small Outline Transistor 223&lt;/b&gt;&lt;p&gt;
PLASTIC PACKAGE CASE 318E-04&lt;br&gt;
Source: http://www.onsemi.co.jp .. LM137M-D.PDF</description>
<smd name="GND" x="-2.3" y="-3.15" dx="1.5" dy="2" layer="1"/>
<smd name="OUT" x="0" y="-3.15" dx="1.5" dy="2" layer="1"/>
<smd name="IN" x="2.3" y="-3.15" dx="1.5" dy="2" layer="1"/>
<rectangle x1="-0.9271" y1="1.1303" x2="0.9271" y2="4.3307" layer="51" rot="R270"/>
<rectangle x1="-0.9271" y1="-3.1623" x2="0.9271" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="-3.2385" y1="-3.1623" x2="-1.3843" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="1.3843" y1="-3.1623" x2="3.2385" y2="-2.2987" layer="51" rot="R270"/>
<wire x1="3.277" y1="1.778" x2="3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-1.778" x2="-3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-1.778" x2="-3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="1.778" x2="3.277" y2="1.778" width="0.2032" layer="21"/>
<rectangle x1="-0.9271" y1="1.1303" x2="0.9271" y2="4.3307" layer="51" rot="R270"/>
<rectangle x1="-0.9271" y1="-3.1623" x2="0.9271" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="-3.2385" y1="-3.1623" x2="-1.3843" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="1.3843" y1="-3.1623" x2="3.2385" y2="-2.2987" layer="51" rot="R270"/>
<text x="-2.54" y="0.0508" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-1.3208" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<smd name="VOUT" x="0" y="3.15" dx="3.8" dy="2" layer="1"/>
<text x="0.4" y="0.4" size="0.254" layer="48" font="vector">direction of pcb</text>
<text x="0.4" y="-0.05" size="0.254" layer="48" font="vector">transportation for</text>
<text x="0.4" y="-0.5" size="0.254" layer="48" font="vector">wavesoldering</text>
<wire x1="0" y1="-0.7" x2="0" y2="0.6" width="0.127" layer="48"/>
<wire x1="0" y1="0.6" x2="-0.2" y2="0.2" width="0.127" layer="48"/>
<wire x1="-0.2" y1="0.2" x2="0.2" y2="0.2" width="0.127" layer="48"/>
<wire x1="0.2" y1="0.2" x2="0" y2="0.6" width="0.127" layer="48"/>
<wire x1="0" y1="-0.7" x2="0.2" y2="-0.3" width="0.127" layer="48"/>
<wire x1="0.2" y1="-0.3" x2="-0.2" y2="-0.3" width="0.127" layer="48"/>
<wire x1="-0.2" y1="-0.3" x2="0" y2="-0.7" width="0.127" layer="48"/>
</package>
<package name="DIL14">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<pad name="1" x="-7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-9.771" y="-3.048" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<pad name="3" x="-2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="0" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.731" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<pad name="9" x="5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="0" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="-2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<wire x1="9.39" y1="2.921" x2="-9.39" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-9.39" y1="-2.921" x2="9.39" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="9.39" y1="2.921" x2="9.39" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-9.39" y1="2.921" x2="-9.39" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-9.39" y1="-2.921" x2="-9.39" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-9.39" y1="1.016" x2="-9.39" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
</package>
<package name="SO14">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<smd name="1" x="-3.81" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-3.81" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-2.54" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.27" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-2.54" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="-1.27" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="0" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="0" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="1.27" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="2.54" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="1.27" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="2.54" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.81" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="3.81" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.556" y="-0.508" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.334" y="-2.413" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<wire x1="5.08" y1="-2.5908" x2="5.08" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-2.2352" x2="5.08" y2="2.5908" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.5908" x2="-5.08" y2="2.5908" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.5908" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.2352" x2="-5.08" y2="-2.5908" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.5908" x2="5.08" y2="-2.5908" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<rectangle x1="-0.254" y1="2.5908" x2="0.254" y2="3.8608" layer="51"/>
<rectangle x1="-4.064" y1="-3.8608" x2="-3.556" y2="-2.5908" layer="51"/>
<rectangle x1="-2.794" y1="-3.8608" x2="-2.286" y2="-2.5908" layer="51"/>
<rectangle x1="-1.524" y1="-3.8608" x2="-1.016" y2="-2.5908" layer="51"/>
<rectangle x1="-0.254" y1="-3.8608" x2="0.254" y2="-2.5908" layer="51"/>
<rectangle x1="-1.524" y1="2.5908" x2="-1.016" y2="3.8608" layer="51"/>
<rectangle x1="-2.794" y1="2.5908" x2="-2.286" y2="3.8608" layer="51"/>
<rectangle x1="-4.064" y1="2.5908" x2="-3.556" y2="3.8608" layer="21"/>
<wire x1="-5.08" y1="-2.2352" x2="5.08" y2="-2.2352" width="0.0508" layer="21"/>
<rectangle x1="1.016" y1="2.5908" x2="1.524" y2="3.8608" layer="51"/>
<rectangle x1="2.286" y1="2.5908" x2="2.794" y2="3.8608" layer="51"/>
<rectangle x1="3.556" y1="2.5908" x2="4.064" y2="3.8608" layer="51"/>
<rectangle x1="1.016" y1="-3.8608" x2="1.524" y2="-2.5908" layer="51"/>
<rectangle x1="2.286" y1="-3.8608" x2="2.794" y2="-2.5908" layer="51"/>
<rectangle x1="3.556" y1="-3.8608" x2="4.064" y2="-2.5908" layer="51"/>
</package>
<package name="LCC20">
<description>&lt;b&gt;Leadless Chip Carrier&lt;/b&gt;&lt;p&gt; Ceramic Package</description>
<text x="-4.0051" y="6.065" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.9751" y="-7.5601" size="1.778" layer="27">&gt;VALUE</text>
<smd name="2" x="-1.27" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="1" x="0" y="3.8001" dx="0.8" dy="3.4" layer="1"/>
<wire x1="-0.4001" y1="4.4" x2="-0.87" y2="4.4" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="4.4" x2="-4.4" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-0.4001" y1="4.3985" x2="0.4001" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-1.6701" y1="4.3985" x2="-0.8699" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="2.14" x2="-4.3985" y2="2.94" width="0.2032" layer="51" curve="180"/>
<wire x1="-2.9401" y1="4.4" x2="-3.3" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.87" y1="4.4" x2="0.4001" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.87" y1="4.3985" x2="1.67" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="3.3" x2="-4.4" y2="2.9401" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="2.14" x2="-4.4" y2="1.6701" width="0.2032" layer="51"/>
<wire x1="-4.3985" y1="0.87" x2="-4.3985" y2="1.67" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="-0.4001" x2="-4.3985" y2="0.4001" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="-1.6701" x2="-4.3985" y2="-0.8699" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="0.87" x2="-4.4" y2="0.4001" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-0.4001" x2="-4.4" y2="-0.87" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-2.9401" x2="-4.4" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-4.4" x2="-4.4" y2="-4.4099" width="0.2032" layer="51"/>
<wire x1="2.14" y1="4.3985" x2="2.94" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="2.14" y1="4.4" x2="1.6701" y2="4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="4.4" x2="2.9401" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.4001" y1="-4.4" x2="0.87" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-0.4001" y1="-4.3985" x2="0.4001" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="0.87" y1="-4.3985" x2="1.67" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="2.9401" y1="-4.4" x2="4.4" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-0.87" y1="-4.4" x2="-0.4001" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-1.6701" y1="-4.3985" x2="-0.8699" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="-2.9401" y1="-4.3985" x2="-2.1399" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="-2.14" y1="-4.4" x2="-1.6701" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-4.4" x2="-2.9401" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="0.4001" x2="4.4" y2="0.87" width="0.2032" layer="51"/>
<wire x1="4.3985" y1="0.4001" x2="4.3985" y2="-0.4001" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="1.6701" x2="4.3985" y2="0.8699" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="2.9401" x2="4.4" y2="4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="-0.87" x2="4.4" y2="-0.4001" width="0.2032" layer="51"/>
<wire x1="4.3985" y1="-0.87" x2="4.3985" y2="-1.67" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="-2.14" x2="4.3985" y2="-2.94" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="-2.14" x2="4.4" y2="-1.6701" width="0.2032" layer="51"/>
<wire x1="4.4" y1="-4.4" x2="4.4" y2="-2.9401" width="0.2032" layer="51"/>
<wire x1="-2.9401" y1="4.3985" x2="-2.1399" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-1.6701" y1="4.4" x2="-2.14" y2="4.4" width="0.2032" layer="51"/>
<wire x1="-4.3985" y1="-2.9401" x2="-4.3985" y2="-2.1399" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="-1.6701" x2="-4.4" y2="-2.14" width="0.2032" layer="51"/>
<wire x1="1.6701" y1="-4.4" x2="2.14" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="2.14" y1="-4.3985" x2="2.94" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="4.3985" y1="2.9401" x2="4.3985" y2="2.1399" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="1.6701" x2="4.4" y2="2.14" width="0.2032" layer="51"/>
<smd name="3" x="-2.54" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="4" x="-4.5001" y="2.54" dx="2" dy="0.8" layer="1"/>
<smd name="5" x="-4.5001" y="1.27" dx="2" dy="0.8" layer="1"/>
<smd name="6" x="-4.5001" y="0" dx="2" dy="0.8" layer="1"/>
<smd name="7" x="-4.5001" y="-1.27" dx="2" dy="0.8" layer="1"/>
<smd name="8" x="-4.5001" y="-2.54" dx="2" dy="0.8" layer="1"/>
<smd name="9" x="-2.54" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="10" x="-1.27" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="11" x="0" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="12" x="1.27" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="13" x="2.54" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="14" x="4.5001" y="-2.54" dx="2" dy="0.8" layer="1"/>
<smd name="15" x="4.5001" y="-1.27" dx="2" dy="0.8" layer="1"/>
<smd name="16" x="4.5001" y="0" dx="2" dy="0.8" layer="1"/>
<smd name="17" x="4.5001" y="1.27" dx="2" dy="0.8" layer="1"/>
<smd name="18" x="4.5001" y="2.54" dx="2" dy="0.8" layer="1"/>
<smd name="19" x="2.54" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="20" x="1.27" y="4.5001" dx="0.8" dy="2" layer="1"/>
<wire x1="-3.3" y1="4.4" x2="-4.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-3.1941" x2="-4.4" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-4.4" x2="-3.1941" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="3.1941" y1="-4.4" x2="4.4" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="4.4" y1="-4.4" x2="4.4" y2="-3.1941" width="0.2032" layer="21"/>
<wire x1="4.4" y1="3.1941" x2="4.4" y2="4.4" width="0.2032" layer="21"/>
<wire x1="4.4" y1="4.4" x2="3.1941" y2="4.4" width="0.2032" layer="21"/>
</package>
<package name="S8">
<smd name="1" x="0" y="0" dx="0.762" dy="1.143" layer="1"/>
<smd name="2" x="1.27" y="0" dx="0.762" dy="1.143" layer="1"/>
<smd name="3" x="2.54" y="0" dx="0.762" dy="1.143" layer="1"/>
<smd name="4" x="3.81" y="0" dx="0.762" dy="1.143" layer="1"/>
<smd name="5" x="3.81" y="5.08" dx="0.762" dy="1.143" layer="1"/>
<smd name="6" x="2.54" y="5.08" dx="0.762" dy="1.143" layer="1"/>
<smd name="7" x="1.27" y="5.08" dx="0.762" dy="1.143" layer="1"/>
<smd name="8" x="0" y="5.08" dx="0.762" dy="1.143" layer="1"/>
<wire x1="-0.4953" y1="0.635" x2="4.3053" y2="0.635" width="0.127" layer="21"/>
<wire x1="4.3053" y1="0.635" x2="4.3053" y2="1.016" width="0.127" layer="21"/>
<wire x1="4.3053" y1="1.016" x2="4.3053" y2="4.445" width="0.127" layer="21"/>
<wire x1="4.3053" y1="4.445" x2="-0.4953" y2="4.445" width="0.127" layer="21"/>
<wire x1="-0.4953" y1="4.445" x2="-0.4953" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.4953" y1="1.016" x2="-0.4953" y2="0.635" width="0.127" layer="21"/>
<wire x1="4.3053" y1="1.016" x2="-0.4953" y2="1.016" width="0.127" layer="21"/>
<text x="-0.9906" y="-0.9906" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-2.9718" y="-0.9906" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="MASWSS0115TR-3000">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="RF1" x="0" y="0" length="middle"/>
<pin name="GND" x="0" y="-2.54" length="middle" direction="pwr"/>
<pin name="RF2" x="0" y="-5.08" length="middle"/>
<pin name="V1" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="RFC" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="V2" x="25.4" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="MMIC_GAINBLOCK">
<pin name="+IN" x="-7.62" y="0" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="MMIC_GAINBLOCK_VCC">
<pin name="+IN" x="-7.62" y="0" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="0" y2="2.54" width="0.4064" layer="94"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" length="point" rot="R270"/>
<wire x1="0" y1="2.54" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="M67715">
<pin name="RFIN" x="-22.86" y="0" length="middle"/>
<pin name="RFOUT" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="GND" x="-7.62" y="-10.16" length="middle" rot="R90"/>
<pin name="VCC1" x="-2.54" y="-10.16" length="middle" rot="R90"/>
<pin name="VBB" x="2.54" y="-10.16" length="middle" rot="R90"/>
<pin name="VCC2" x="7.62" y="-10.16" length="middle" rot="R90"/>
<wire x1="-17.78" y1="-5.08" x2="-17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="-17.78" y1="5.08" x2="20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="20.32" y2="-5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="-5.08" x2="-17.78" y2="-5.08" width="0.254" layer="94"/>
<text x="-17.78" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="5.08" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="LINEAR-REGULATOR-OUT2">
<pin name="IN" x="-12.7" y="2.54" length="middle" direction="pwr"/>
<pin name="OUT@1" x="12.7" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="GND" x="0" y="-7.62" length="middle" direction="pwr" rot="R90"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="OUT@2" x="12.7" y="0" length="middle" direction="sup" rot="R180"/>
</symbol>
<symbol name="7402">
<pin name="O" x="12.7" y="0" visible="pad" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="I0" x="-12.7" y="2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="I1" x="-12.7" y="-2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<wire x1="-1.27" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.096" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.2446" y1="-5.0678" x2="7.5439" y2="0.0507" width="0.4064" layer="94" curve="60.1471" cap="flat"/>
<wire x1="-1.2446" y1="5.0678" x2="7.5442" y2="-0.0505" width="0.4064" layer="94" curve="-60.1488" cap="flat"/>
<text x="-7.62" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94" curve="-90"/>
</symbol>
<symbol name="PWRN-1">
<pin name="GND" x="0" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="10.16" visible="pad" direction="pwr" rot="R270"/>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-7.62" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="5.08" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
<symbol name="LTC1624CS8">
<pin name="SENSE-" x="-7.62" y="5.08" length="point" direction="hiz" rot="R180"/>
<pin name="ITH/RUN" x="-7.62" y="2.54" length="point" rot="R180"/>
<pin name="VFB" x="-7.62" y="0" length="point" direction="in" rot="R180"/>
<pin name="GND" x="-7.62" y="-2.54" length="point" direction="pwr" rot="R180"/>
<pin name="VIN" x="7.62" y="5.08" length="point" direction="pwr"/>
<pin name="BOOST" x="7.62" y="2.54" length="point" direction="pwr"/>
<pin name="TG" x="7.62" y="0" length="point" direction="out"/>
<pin name="SW" x="7.62" y="-2.54" length="point" direction="pas"/>
<text x="-5.08" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MASWSS0115TR-3000" prefix="S">
<description>&lt;b&gt;RF Switch ICs DC-3.0GHz ISO 22dB IL &lt;.3dB @ 2.4GHz&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/MASWSS0115TR-3000.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MASWSS0115TR-3000" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT65P210X110-6N">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="RF1" pad="1"/>
<connect gate="G$1" pin="RF2" pad="3"/>
<connect gate="G$1" pin="RFC" pad="5"/>
<connect gate="G$1" pin="V1" pad="6"/>
<connect gate="G$1" pin="V2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="RF Switch ICs DC-3.0GHz ISO 22dB IL &lt;.3dB @ 2.4GHz" constant="no"/>
<attribute name="HEIGHT" value="1.1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="MACOM" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MASWSS0115TR-3000" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="937-MASWSS0115-T" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BGA616" prefix="IC">
<gates>
<gate name="G$1" symbol="MMIC_GAINBLOCK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT343R">
<connects>
<connect gate="G$1" pin="+IN" pad="1"/>
<connect gate="G$1" pin="GND" pad="2 4"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BGA2800" prefix="IC">
<gates>
<gate name="G$1" symbol="MMIC_GAINBLOCK_VCC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT363_PHILIPS">
<connects>
<connect gate="G$1" pin="+IN" pad="6"/>
<connect gate="G$1" pin="GND" pad="2 4 5"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M67715" prefix="IC">
<gates>
<gate name="G$1" symbol="M67715" x="0" y="0"/>
</gates>
<devices>
<device name="" package="M67715">
<connects>
<connect gate="G$1" pin="GND" pad="6 7"/>
<connect gate="G$1" pin="RFIN" pad="1"/>
<connect gate="G$1" pin="RFOUT" pad="5"/>
<connect gate="G$1" pin="VBB" pad="3"/>
<connect gate="G$1" pin="VCC1" pad="2"/>
<connect gate="G$1" pin="VCC2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AZ1086?-*" prefix="IC">
<gates>
<gate name="G$1" symbol="LINEAR-REGULATOR-OUT2" x="0" y="0"/>
</gates>
<devices>
<device name="H" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT@1" pad="OUT"/>
<connect gate="G$1" pin="OUT@2" pad="VOUT"/>
</connects>
<technologies>
<technology name="3.3"/>
<technology name="5.0"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74*02" prefix="IC">
<description>Quad 2-input &lt;b&gt;NOR&lt;/b&gt; gate</description>
<gates>
<gate name="A" symbol="7402" x="12.7" y="5.08" swaplevel="1"/>
<gate name="B" symbol="7402" x="12.7" y="-10.16" swaplevel="1"/>
<gate name="C" symbol="7402" x="43.18" y="5.08" swaplevel="1"/>
<gate name="D" symbol="7402" x="43.18" y="-10.16" swaplevel="1"/>
<gate name="P" symbol="PWRN-1" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="N" package="DIL14">
<connects>
<connect gate="A" pin="I0" pad="2"/>
<connect gate="A" pin="I1" pad="3"/>
<connect gate="A" pin="O" pad="1"/>
<connect gate="B" pin="I0" pad="5"/>
<connect gate="B" pin="I1" pad="6"/>
<connect gate="B" pin="O" pad="4"/>
<connect gate="C" pin="I0" pad="8"/>
<connect gate="C" pin="I1" pad="9"/>
<connect gate="C" pin="O" pad="10"/>
<connect gate="D" pin="I0" pad="11"/>
<connect gate="D" pin="I1" pad="12"/>
<connect gate="D" pin="O" pad="13"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name=""/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
<device name="D" package="SO14">
<connects>
<connect gate="A" pin="I0" pad="2"/>
<connect gate="A" pin="I1" pad="3"/>
<connect gate="A" pin="O" pad="1"/>
<connect gate="B" pin="I0" pad="5"/>
<connect gate="B" pin="I1" pad="6"/>
<connect gate="B" pin="O" pad="4"/>
<connect gate="C" pin="I0" pad="8"/>
<connect gate="C" pin="I1" pad="9"/>
<connect gate="C" pin="O" pad="10"/>
<connect gate="D" pin="I0" pad="11"/>
<connect gate="D" pin="I1" pad="12"/>
<connect gate="D" pin="O" pad="13"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name=""/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
<device name="FK" package="LCC20">
<connects>
<connect gate="A" pin="I0" pad="3"/>
<connect gate="A" pin="I1" pad="4"/>
<connect gate="A" pin="O" pad="2"/>
<connect gate="B" pin="I0" pad="8"/>
<connect gate="B" pin="I1" pad="9"/>
<connect gate="B" pin="O" pad="6"/>
<connect gate="C" pin="I0" pad="12"/>
<connect gate="C" pin="I1" pad="13"/>
<connect gate="C" pin="O" pad="14"/>
<connect gate="D" pin="I0" pad="16"/>
<connect gate="D" pin="I1" pad="18"/>
<connect gate="D" pin="O" pad="19"/>
<connect gate="P" pin="GND" pad="10"/>
<connect gate="P" pin="VCC" pad="20"/>
</connects>
<technologies>
<technology name=""/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC1624" prefix="IC">
<description>LTC1624 high-efficient Nch switching regulator controler</description>
<gates>
<gate name="G$1" symbol="LTC1624CS8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="S8">
<connects>
<connect gate="G$1" pin="BOOST" pad="7"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="ITH/RUN" pad="2"/>
<connect gate="G$1" pin="SENSE-" pad="1"/>
<connect gate="G$1" pin="SW" pad="5"/>
<connect gate="G$1" pin="TG" pad="6"/>
<connect gate="G$1" pin="VFB" pad="3"/>
<connect gate="G$1" pin="VIN" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Passive_Robotech">
<description>&lt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="1005">
<description>Metric Code Size 1005</description>
<smd name="1" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
</package>
<package name="1608">
<description>Metric Code Size 1608</description>
<smd name="1" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
</package>
<package name="3216">
<description>Metric Code Size 3216</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="3225">
<description>Metric Code Size 3225</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
</package>
<package name="4532">
<description>Metric Code Size 4532</description>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="5650">
<description>Metric Code Size 5650</description>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
</package>
<package name="2012-C">
<smd name="1" x="-0.9207" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="0.9206" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1.381" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.381" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.889" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-0.635" x2="0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="7H3">
<pad name="G6" x="-2.5" y="2.5" drill="0.8" shape="octagon"/>
<pad name="G5" x="-2.5" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="G3" x="2.5" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="G4" x="2.5" y="2.5" drill="0.8" shape="octagon"/>
<pad name="7" x="0" y="3.81" drill="1.5" shape="octagon"/>
<pad name="8" x="0" y="-3.81" drill="1.5" shape="octagon"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<text x="-2.54" y="-6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
<pad name="I" x="-10.12" y="2.5" drill="0.8" shape="octagon"/>
<pad name="G9" x="-10.12" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="G7" x="-5.12" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="G8" x="-5.12" y="2.5" drill="0.8" shape="octagon"/>
<pad name="15" x="-7.62" y="3.81" drill="1.5" shape="octagon"/>
<pad name="16" x="-7.62" y="-3.81" drill="1.5" shape="octagon"/>
<wire x1="-11.43" y1="3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-11.43" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-3.81" x2="-11.43" y2="3.81" width="0.127" layer="21"/>
<pad name="G2" x="5.12" y="2.5" drill="0.8" shape="octagon"/>
<pad name="G1" x="5.12" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="O" x="10.12" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="G" x="10.12" y="2.5" drill="0.8" shape="octagon"/>
<pad name="23" x="7.62" y="3.81" drill="1.5" shape="octagon"/>
<pad name="24" x="7.62" y="-3.81" drill="1.5" shape="octagon"/>
<wire x1="3.81" y1="3.81" x2="11.43" y2="3.81" width="0.127" layer="21"/>
<wire x1="11.43" y1="3.81" x2="11.43" y2="-3.81" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.81" x2="3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="2012-R">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="250-80">
<wire x1="12.5" y1="4" x2="-12.5" y2="4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="4" x2="-12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-4" x2="12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="12.5" y1="-4" x2="12.5" y2="4" width="0.127" layer="21"/>
<pad name="P$1" x="-15.24" y="0" drill="1.2" shape="octagon"/>
<pad name="P$2" x="15.24" y="0" drill="1.2" shape="octagon"/>
<rectangle x1="-15.24" y1="-0.508" x2="-12.573" y2="0.508" layer="51"/>
<rectangle x1="12.573" y1="-0.508" x2="15.24" y2="0.508" layer="51"/>
</package>
<package name="6032">
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.909" y1="1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="1.524" x2="2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.909" y1="1.524" x2="2.909" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="-1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="CEMENTR/48">
<description>Cement Resistor
Width=48mm, Depth=10mm, Height=10mm</description>
<pad name="1" x="-29.21" y="0" drill="1.5" shape="octagon"/>
<pad name="2" x="29.21" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="29.21" y1="0" x2="28.194" y2="0" width="0.6096" layer="51"/>
<wire x1="-29.21" y1="0" x2="-28.194" y2="0" width="0.6096" layer="51"/>
<rectangle x1="27.305" y1="-0.3048" x2="28.1686" y2="0.3048" layer="21"/>
<rectangle x1="-28.1686" y1="-0.3048" x2="-27.305" y2="0.3048" layer="21"/>
<wire x1="27.305" y1="0" x2="24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-27.305" y1="0" x2="-24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-24" y1="5" x2="24" y2="5" width="0.127" layer="21"/>
<wire x1="24" y1="5" x2="24" y2="-5" width="0.127" layer="21"/>
<wire x1="24" y1="-5" x2="-24" y2="-5" width="0.127" layer="21"/>
<wire x1="-24" y1="-5" x2="-24" y2="5" width="0.127" layer="21"/>
</package>
<package name="DEA162025LT-5003C3">
<smd name="1" x="-0.55" y="0.5" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0" y="0.5" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="0.55" y="0.5" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="0.55" y="-0.5" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<smd name="5" x="0" y="-0.5" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<smd name="6" x="-0.55" y="-0.5" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="21"/>
<circle x="-1.27" y="1.27" radius="0.3" width="0.127" layer="21"/>
<text x="1.27" y="0" size="1.27" layer="25">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="L5-7,5">
<pad name="P$1" x="2.5" y="0" drill="0.8"/>
<pad name="P$2" x="-2.5" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="0.5" y1="0" x2="1.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<text x="-3" y="-2.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.5" y="-4" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L22">
<pad name="P$1" x="11.43" y="0" drill="2.8" diameter="6.4516"/>
<pad name="P$2" x="-11.43" y="0" drill="2.8" diameter="6.4516"/>
<circle x="0" y="0" radius="11.359225" width="0.127" layer="21"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T50H">
<circle x="0" y="0" radius="2.83980625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="-6.35" drill="1"/>
<pad name="P$2" x="2.54" y="-6.35" drill="1"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T50V">
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T20V">
<pad name="P$1" x="-1.27" y="0" drill="0.8"/>
<pad name="P$2" x="1.27" y="0" drill="0.8"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<text x="-1.905" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-1.905" y="3.175" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="2125">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.8" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.8" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="SOLENOID-D5-L2.5">
<pad name="P$1" x="-1.27" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="27"/>
<wire x1="-0.635" y1="0" x2="0" y2="2.54" width="0.127" layer="27"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="0" width="0.127" layer="27"/>
</package>
<package name="SOLENOID-D5-L5">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
</package>
<package name="NR6028T">
<description>&lt;b&gt;NR6028&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2" layer="51"/>
<wire x1="-4.15" y1="4" x2="4.15" y2="4" width="0.1" layer="51"/>
<wire x1="4.15" y1="4" x2="4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="4.15" y1="-4" x2="-4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="-4.15" y1="-4" x2="-4.15" y2="4" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="21"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="TYS6045">
<smd name="1" x="-2.3" y="0" dx="6" dy="2.2" layer="1" rot="R90"/>
<smd name="2" x="2.3" y="0" dx="6" dy="2.2" layer="1" rot="R90"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="SRP1038">
<smd name="1" x="-4.4" y="0" dx="3.8" dy="3.5" layer="1"/>
<smd name="2" x="4.4" y="0" dx="3.8" dy="3.5" layer="1"/>
<wire x1="5" y1="5" x2="-5" y2="5" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="5" width="0.127" layer="21"/>
<text x="-2.54" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T37V">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<wire x1="-1.6256" y1="4.6736" x2="1.6256" y2="4.6736" width="0.127" layer="21"/>
<wire x1="1.6256" y1="4.6736" x2="1.6256" y2="-4.6736" width="0.127" layer="21"/>
<wire x1="1.6256" y1="-4.6736" x2="-1.6256" y2="-4.6736" width="0.127" layer="21"/>
<wire x1="-1.6256" y1="-4.6736" x2="-1.6256" y2="4.6736" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.127" layer="21"/>
<text x="-2.54" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3729" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="HELICALFILTR_3EL">
<text x="-2.54" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<pin name="GND@1" x="-2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="IN" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="-1.905" x2="-2.54" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="-1.27" x2="-1.905" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="-0.635" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="0.635" x2="-2.54" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="1.27" x2="-1.905" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="1.905" x2="-2.54" y2="2.54" width="0.254" layer="94" curve="90"/>
<pin name="GND@2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<text x="-2.54" y="6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND@3" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="OUT" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="2.54" y1="-2.54" x2="3.175" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="3.175" y1="-1.905" x2="2.54" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="-1.27" x2="3.175" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="3.175" y1="-0.635" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="3.175" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="3.175" y1="0.635" x2="2.54" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="1.27" x2="3.175" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="3.175" y1="1.905" x2="2.54" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
</symbol>
<symbol name="R">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="LPF">
<pin name="IN" x="-12.7" y="2.54" length="middle"/>
<pin name="OUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="L">
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7.5" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TT67426D" prefix="HF" uservalue="yes">
<gates>
<gate name="G$1" symbol="HELICALFILTR_3EL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="7H3">
<connects>
<connect gate="G$1" pin="GND@1" pad="G"/>
<connect gate="G$1" pin="GND@2" pad="G1"/>
<connect gate="G$1" pin="GND@3" pad="7 8 15 16 23 24 G2 G3 G4 G5 G6 G7 G8 G9"/>
<connect gate="G$1" pin="IN" pad="I"/>
<connect gate="G$1" pin="OUT" pad="O"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6032" package="6032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/48" package="CEMENTR/48">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DEA162025LT-5003C3" prefix="F">
<gates>
<gate name="G$1" symbol="LPF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DEA162025LT-5003C3">
<connects>
<connect gate="G$1" pin="GND" pad="2 4 5 6"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5(VERTICAL)" package="L5-7,5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22(VERTICAL)" package="L22">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50H" package="T50H">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50V" package="T50V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T20V" package="T20V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2125" package="2125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L2.5" package="SOLENOID-D5-L2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L5" package="SOLENOID-D5-L5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR6028" package="NR6028T">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR8040" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TYS6045" package="TYS6045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SRP1038" package="SRP1038">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T37V" package="T37V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Supply symbol library&lt;br&gt;
$Rev: 25542 $ 
&lt;p&gt;
since 2008&lt;br&gt;
by&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+5V">
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="VCC">
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V">
<description>5V</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Transistor_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Transitor Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SOT343">
<smd name="4" x="1" y="0.65" dx="1" dy="0.6" layer="1"/>
<smd name="3" x="1" y="-0.65" dx="1" dy="0.6" layer="1"/>
<smd name="1" x="-1" y="0.65" dx="1" dy="0.6" layer="1"/>
<smd name="2" x="-1" y="-0.5" dx="1" dy="0.9" layer="1"/>
<wire x1="0.6" y1="1" x2="0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="0.6" y1="-1" x2="-0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1" x2="-0.6" y2="1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="1" x2="0.6" y2="1" width="0.127" layer="21"/>
<text x="-1.27" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SC62">
<description>SC-62 (SOT89)</description>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<smd name="B" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="E" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="C" x="0" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<smd name="C@1" x="0" y="1.575" dx="2.032" dy="1.27" layer="1"/>
<rectangle x1="-0.89" y1="1.19" x2="0.89" y2="2.11" layer="51"/>
<wire x1="-0.7112" y1="-0.4064" x2="-0.7112" y2="0.9144" width="0.6096" layer="1"/>
<wire x1="-0.7112" y1="0.9144" x2="0.7112" y2="0.9144" width="0.6096" layer="1"/>
<wire x1="0.7112" y1="0.9144" x2="0.7112" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0.7112" y1="-0.4064" x2="0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0.2032" y1="-0.4064" x2="-0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="-0.2032" y1="-0.4064" x2="-0.7112" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="-0.2032" y1="-0.4064" x2="-0.2032" y2="0.508" width="0.6096" layer="1"/>
<wire x1="0.2032" y1="0.508" x2="0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0" y1="-1.4224" x2="0" y2="-0.7112" width="0.6096" layer="1"/>
</package>
<package name="SC70">
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="E" x="0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="B" x="-0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<rectangle x1="-0.15" y1="0.625" x2="0.15" y2="1.05" layer="51"/>
<rectangle x1="0.5" y1="-1.05" x2="0.8" y2="-0.625" layer="51"/>
<rectangle x1="-0.8" y1="-1.05" x2="-0.5" y2="-0.625" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
</package>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="NPN">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
</symbol>
<symbol name="PNP">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.086" y1="1.678" x2="1.578" y2="2.594" width="0.1524" layer="94"/>
<wire x1="1.578" y1="2.594" x2="0.516" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.516" y1="1.478" x2="2.086" y2="1.678" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.124" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
</symbol>
<symbol name="NFET">
<pin name="S" x="5.08" y="-2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<text x="-2.54" y="0.635" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="3.175" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="D" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="1.905" y1="0.635" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BFU730F" prefix="TR">
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT343">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="E" pad="1 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2SA1213" prefix="Q">
<gates>
<gate name="G$1" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC62">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DTC123E" prefix="TR">
<description>ROHM Digital Transistor</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="UA" package="SC70">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA" package="SC59">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IRLML6344" prefix="TR">
<gates>
<gate name="G$1" symbol="NFET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Connector Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
Hideo Tanida&lt;br&gt;
Makoto Shimazu&lt;br&gt;
Mayu Kojima&lt;br&gt;
Takefumi Hiraki&lt;br&gt;
Soichiro Iwataki&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="VIA_3.2">
<pad name="P$1" x="0" y="0" drill="3.2" shape="octagon"/>
<circle x="0" y="0" radius="4" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3" width="0.127" layer="52"/>
</package>
<package name="5415025">
<pad name="CENTER" x="0" y="0" drill="1.2"/>
<pad name="SHELL2" x="3.3" y="3.3" drill="2.2"/>
<pad name="SHELL3" x="3.3" y="-3.3" drill="2.2"/>
<pad name="SHELL1" x="-3.3" y="3.3" drill="2.2"/>
<pad name="SHELL4" x="-3.3" y="-3.3" drill="2.2"/>
<wire x1="-22.63" y1="3.81" x2="-11.96" y2="3.81" width="0.127" layer="21"/>
<wire x1="-11.96" y1="3.81" x2="-11.96" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-11.96" y1="-3.81" x2="-22.63" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-22.63" y1="-3.81" x2="-22.63" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.42" y1="7" x2="-6.42" y2="-7" width="0.127" layer="21"/>
<wire x1="-6.42" y1="-7" x2="-7.31" y2="-7" width="0.127" layer="21"/>
<wire x1="-7.31" y1="-7" x2="-7.31" y2="7" width="0.127" layer="21"/>
<wire x1="-7.31" y1="7" x2="-6.42" y2="7" width="0.127" layer="21"/>
<wire x1="-11.43" y1="3.81" x2="-10.16" y2="5.08" width="0.127" layer="21"/>
<wire x1="-10.16" y1="5.08" x2="-7.62" y2="5.08" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-3.81" x2="-10.16" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-5.08" x2="-7.62" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-6.35" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="-6.35" y2="-5.08" width="0.127" layer="21"/>
<circle x="-20.32" y="0" radius="1" width="0.127" layer="21"/>
<text x="-3.81" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="6.35" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="M-1">
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="BNC-F2">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="GND@1" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EARTH" prefix="VIA_">
<gates>
<gate name="_EARTH" symbol="M-1" x="5.08" y="0"/>
</gates>
<devices>
<device name="_3.2" package="VIA_3.2">
<connects>
<connect gate="_EARTH" pin="S" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BNC" prefix="CON">
<gates>
<gate name="G$1" symbol="BNC-F2" x="0" y="0"/>
</gates>
<devices>
<device name="-5415025" package="5415025">
<connects>
<connect gate="G$1" pin="1" pad="CENTER"/>
<connect gate="G$1" pin="GND@1" pad="SHELL1 SHELL2 SHELL3 SHELL4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="macom">
<packages>
<package name="MABA-010129-CT4A40">
<smd name="1" x="2.1082" y="1.524" dx="1.905" dy="0.9144" layer="1"/>
<smd name="2" x="2.1082" y="0" dx="1.905" dy="0.9144" layer="1"/>
<smd name="3" x="2.1082" y="-1.524" dx="1.905" dy="0.9144" layer="1"/>
<smd name="4" x="-2.1082" y="-1.524" dx="1.905" dy="0.9144" layer="1"/>
<smd name="5" x="-2.1082" y="1.524" dx="1.905" dy="0.9144" layer="1"/>
<wire x1="-1.3208" y1="2.1336" x2="1.3208" y2="2.1336" width="0.127" layer="21"/>
<wire x1="1.3208" y1="2.1336" x2="1.3208" y2="-2.1336" width="0.127" layer="21"/>
<wire x1="1.3208" y1="-2.1336" x2="-1.3208" y2="-2.1336" width="0.127" layer="21"/>
<wire x1="-1.3208" y1="-2.1336" x2="-1.3208" y2="2.1336" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TRANS_1_4">
<text x="5.08" y="11.43" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="5.08" y="8.89" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="PRI1" x="-7.62" y="5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="SEC1" x="7.62" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="-1.905" y2="4.445" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="4.445" x2="-1.27" y2="5.08" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="5.08" x2="-0.635" y2="4.445" width="0.254" layer="94" curve="90"/>
<wire x1="-0.635" y1="4.445" x2="0" y2="5.08" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="5.08" x2="0.635" y2="4.445" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="4.445" x2="1.27" y2="5.08" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="5.08" x2="1.905" y2="4.445" width="0.254" layer="94" curve="90"/>
<wire x1="1.905" y1="4.445" x2="2.54" y2="5.08" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="2.54" x2="1.905" y2="3.175" width="0.254" layer="94" curve="90"/>
<wire x1="1.905" y1="3.175" x2="1.27" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="2.54" x2="0.635" y2="3.175" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="3.175" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-0.635" y2="3.175" width="0.254" layer="94" curve="90"/>
<wire x1="-0.635" y1="3.175" x2="-1.27" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="2.54" x2="-1.905" y2="3.175" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="3.175" x2="-2.54" y2="2.54" width="0.254" layer="94" curve="90"/>
<pin name="SEC2" x="7.62" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="PRI2" x="-7.62" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="2.54" y1="-5.08" x2="1.905" y2="-4.445" width="0.254" layer="94" curve="90"/>
<wire x1="1.905" y1="-4.445" x2="1.27" y2="-5.08" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="-5.08" x2="0.635" y2="-4.445" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-4.445" x2="0" y2="-5.08" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-5.08" x2="-0.635" y2="-4.445" width="0.254" layer="94" curve="90"/>
<wire x1="-0.635" y1="-4.445" x2="-1.27" y2="-5.08" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-5.08" x2="-1.905" y2="-4.445" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="-4.445" x2="-2.54" y2="-5.08" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="-2.54" x2="-1.905" y2="-3.175" width="0.254" layer="94" curve="90"/>
<wire x1="-1.905" y1="-3.175" x2="-1.27" y2="-2.54" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="-3.175" width="0.254" layer="94" curve="90"/>
<wire x1="-0.635" y1="-3.175" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-3.175" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-3.175" x2="1.27" y2="-2.54" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="-3.175" width="0.254" layer="94" curve="90"/>
<wire x1="1.905" y1="-3.175" x2="2.54" y2="-2.54" width="0.254" layer="94" curve="90"/>
<pin name="SEC_CT" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MABA-010129-CT4A40" prefix="T">
<gates>
<gate name="A" symbol="TRANS_1_4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MABA-010129-CT4A40">
<connects>
<connect gate="A" pin="PRI1" pad="5"/>
<connect gate="A" pin="PRI2" pad="4"/>
<connect gate="A" pin="SEC1" pad="1"/>
<connect gate="A" pin="SEC2" pad="3"/>
<connect gate="A" pin="SEC_CT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Diode Library &lt;br&gt;
$Rev: 25542 $

&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SOT143R_INFINEON" urn="urn:adsk.eagle:footprint:29709/1">
<description>&lt;b&gt;Small Outline Transistor; 4 leads&lt;/b&gt; Reflow soldering&lt;p&gt;
INFINEON, www.infineon.com/cmc_upload/0/000/010/257/eh_db_5b.pdf</description>
<wire x1="-1.4478" y1="-0.635" x2="-1.4478" y2="0.635" width="0.1016" layer="51"/>
<wire x1="1.4478" y1="-0.635" x2="1.4478" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="0.635" x2="1.4478" y2="0.635" width="0.1016" layer="51"/>
<wire x1="1.4478" y1="-0.635" x2="-1.4478" y2="-0.635" width="0.1016" layer="51"/>
<smd name="4" x="-1" y="1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="1" y="1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="1" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="-0.8" y="-1" dx="1.2" dy="0.9" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.7366" y1="-1.3208" x2="1.1938" y2="-0.635" layer="51"/>
<rectangle x1="0.7112" y1="0.635" x2="1.1684" y2="1.3208" layer="51"/>
<rectangle x1="-1.143" y1="0.635" x2="-0.6858" y2="1.3208" layer="51"/>
<rectangle x1="-1.1938" y1="-1.3208" x2="-0.3048" y2="-0.635" layer="51"/>
</package>
<package name="SOD-123FL">
<description>SOD-123FL for manual soldering</description>
<smd name="K" x="-1.85" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="A" x="1.85" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.5" y1="1" x2="-1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="1.5" y1="1" x2="1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1" x2="1.5" y2="1" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1" x2="-1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="1.27" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
</package>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RING_DIODE">
<pin name="1" x="-12.7" y="5.08" visible="off" length="middle"/>
<pin name="2" x="-12.7" y="-5.08" visible="off" length="middle"/>
<pin name="3" x="12.7" y="5.08" visible="off" length="middle" rot="R180"/>
<pin name="4" x="12.7" y="-5.08" visible="off" length="middle" rot="R180"/>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.556" y1="1.778" x2="-1.524" y2="3.556" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.032" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.508" x2="-0.508" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-2.032" x2="-2.286" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-1.778" x2="-1.778" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="D">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BAT15-099R" prefix="D">
<gates>
<gate name="G$1" symbol="RING_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT143R_INFINEON">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SS2040FL" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-123FL">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1SS193" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC59">
<connects>
<connect gate="G$1" pin="A" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="CTRIM3008" urn="urn:adsk.eagle:footprint:23277/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<pad name="+" x="-1.875" y="0" drill="1"/>
<pad name="-" x="1.875" y="0" drill="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-2.5" y1="-0.9" x2="-2.25" y2="0.9" layer="51"/>
<rectangle x1="2.25" y1="-1" x2="2.45" y2="1" layer="51"/>
<rectangle x1="2.45" y1="-0.5" x2="2.65" y2="0.5" layer="51"/>
</package>
<package name="CTRIM3018_11" urn="urn:adsk.eagle:footprint:23278/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<smd name="+" x="-2.35" y="0" dx="2.3" dy="1.6" layer="1"/>
<smd name="-" x="2.35" y="0" dx="2.3" dy="1.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-2.6" y1="-0.6" x2="-2.25" y2="0.6" layer="51"/>
<rectangle x1="2.25" y1="-0.6" x2="2.6" y2="0.6" layer="51"/>
</package>
<package name="CTRIM3018_12" urn="urn:adsk.eagle:footprint:23279/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<smd name="+" x="-3" y="0" dx="2" dy="1.6" layer="1"/>
<smd name="-" x="3" y="0" dx="2" dy="1.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-3.5" y1="-0.6" x2="-2.25" y2="0.6" layer="51"/>
<rectangle x1="2.25" y1="-0.6" x2="3.5" y2="0.6" layer="51"/>
</package>
<package name="CTRIM3040.427" urn="urn:adsk.eagle:footprint:23280/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side, vertical to PCB</description>
<wire x1="0.3" y1="1.5" x2="-0.7" y2="-1.35" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.5" x2="0.7" y2="1.35" width="0.1524" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
</package>
<package name="CTRIM3040.428" urn="urn:adsk.eagle:footprint:23281/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from both sides, vertical to PCB</description>
<wire x1="0.3" y1="1.5" x2="-0.7" y2="-1.3" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.5" x2="0.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
<hole x="0" y="0" drill="4.5"/>
</package>
<package name="CTRIM3040.448" urn="urn:adsk.eagle:footprint:23282/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side, parallel to PCB</description>
<wire x1="-3.75" y1="2.91" x2="3.75" y2="2.91" width="0.254" layer="51"/>
<wire x1="3.75" y1="2.91" x2="3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.75" y1="-2.54" x2="-3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.75" y1="-2.54" x2="-3.75" y2="2.91" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-2.64" x2="-1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-3.64" x2="-0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.64" x2="1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="1.5" y1="-3.64" x2="1.5" y2="-2.64" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.64" x2="-0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.14" x2="0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.14" x2="0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.55" y1="2.91" x2="1.5" y2="2.91" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="3.81" drill="1.3"/>
<pad name="1B" x="2.5" y="3.81" drill="1.3"/>
<pad name="2" x="0" y="1.31" drill="1.3"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="3.01" x2="-2.2" y2="4.56" layer="51"/>
<rectangle x1="2.2" y1="3.01" x2="2.8" y2="4.56" layer="51"/>
</package>
<package name="CTRIM3040.450" urn="urn:adsk.eagle:footprint:23283/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from both sides, parallel to PCB</description>
<wire x1="-3.75" y1="2.91" x2="3.75" y2="2.91" width="0.254" layer="51"/>
<wire x1="3.75" y1="2.91" x2="3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.75" y1="-2.54" x2="-3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.75" y1="-2.54" x2="-3.75" y2="2.91" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-2.64" x2="-1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-3.64" x2="-0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.64" x2="1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="1.5" y1="-3.64" x2="1.5" y2="-2.64" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.64" x2="-0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.14" x2="0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.14" x2="0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.55" y1="2.91" x2="1.5" y2="2.91" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="3.81" drill="1.3"/>
<pad name="1B" x="2.5" y="3.81" drill="1.3"/>
<pad name="2" x="0" y="1.31" drill="1.3"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="3.01" x2="-2.2" y2="4.56" layer="51"/>
<rectangle x1="2.2" y1="3.01" x2="2.8" y2="4.56" layer="51"/>
</package>
<package name="CTRIM3040.452" urn="urn:adsk.eagle:footprint:23284/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side for automatic adjustment, vertical to PCB</description>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="-0.85" y1="1.5" x2="-1.7" y2="0" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="0" x2="-0.85" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="-1.5" x2="0.85" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="1.7" y2="0" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0" x2="0.85" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.35" y1="0.45" x2="1.05" y2="-1" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="0.95" x2="1.35" y2="-0.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.4508" width="0.1016" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
</package>
<package name="CTRIM3050.504" urn="urn:adsk.eagle:footprint:23285/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from one side, vertical to PCB</description>
<wire x1="-0.7" y1="0.4" x2="0.6" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="1.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1" x2="2.8" y2="1" width="0.254" layer="21" curve="-137.924978"/>
<wire x1="-2.4" y1="-1" x2="2.8" y2="-1" width="0.254" layer="21" curve="137.924978"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.254" layer="51" curve="42.075022"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.254" layer="51" curve="42.075022"/>
<circle x="0.2" y="0" radius="1" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.1"/>
<pad name="2" x="2.5" y="0" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CTRIM3050.505" urn="urn:adsk.eagle:footprint:23286/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from both sides, vertical to PCB</description>
<wire x1="-0.7" y1="0.4" x2="0.6" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="1.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1" x2="2.8" y2="1" width="0.254" layer="21" curve="-137.924978"/>
<wire x1="-2.4" y1="-1" x2="2.8" y2="-1" width="0.254" layer="21" curve="137.924978"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.254" layer="51" curve="42.075022"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.254" layer="51" curve="42.075022"/>
<circle x="0.2" y="0" radius="1" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.1"/>
<pad name="2" x="2.5" y="0" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0.2" y="0" drill="2.5"/>
</package>
<package name="CTRIM3050.506" urn="urn:adsk.eagle:footprint:23287/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from one side, parallel to PCB</description>
<wire x1="-2.4" y1="0.22" x2="-2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-2.63" x2="-1.2" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-1.63" x2="1.2" y2="-1.63" width="0.254" layer="21"/>
<wire x1="1.2" y1="-1.63" x2="1.2" y2="-2.63" width="0.254" layer="21"/>
<wire x1="2.4" y1="-1.63" x2="2.4" y2="0.22" width="0.254" layer="21"/>
<wire x1="2.4" y1="1.67" x2="-2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="-1.2" y1="-2.63" x2="-0.4" y2="-2.63" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-2.63" x2="-0.4" y2="-2.13" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-2.13" x2="0.4" y2="-2.13" width="0.254" layer="21"/>
<wire x1="0.4" y1="-2.13" x2="0.4" y2="-2.63" width="0.254" layer="21"/>
<wire x1="0.4" y1="-2.63" x2="1.2" y2="-2.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-1.63" x2="-2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="1.2" y1="-1.63" x2="2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-2.4" y1="0.22" x2="-2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="2.4" y1="0.22" x2="2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="-1.45" y1="1.67" x2="1.45" y2="1.67" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="1.27" drill="1.1"/>
<pad name="1B" x="2.5" y="1.27" drill="1.1"/>
<pad name="2" x="0" y="-0.23" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CTRIMCTZ2" urn="urn:adsk.eagle:footprint:23288/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; AVX</description>
<wire x1="-1.15" y1="-1.4" x2="-1.15" y2="0.45" width="0.254" layer="51"/>
<wire x1="-1.15" y1="0.45" x2="-0.45" y2="1.35" width="0.254" layer="51"/>
<wire x1="-0.45" y1="1.35" x2="0.45" y2="1.35" width="0.254" layer="51"/>
<wire x1="0.45" y1="1.35" x2="1.15" y2="0.4" width="0.254" layer="51"/>
<wire x1="1.15" y1="0.4" x2="1.15" y2="-1.4" width="0.254" layer="51"/>
<wire x1="1.15" y1="-1.4" x2="-1.15" y2="-1.4" width="0.254" layer="51"/>
<wire x1="-0.5" y1="-1.4" x2="-1.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="-1.15" y1="-1.4" x2="-1.15" y2="0.45" width="0.254" layer="21"/>
<wire x1="-1.15" y1="0.45" x2="-0.45" y2="1.35" width="0.254" layer="21"/>
<wire x1="0.5" y1="-1.4" x2="1.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="1.15" y1="-1.4" x2="1.15" y2="0.4" width="0.254" layer="21"/>
<wire x1="1.15" y1="0.4" x2="0.45" y2="1.35" width="0.254" layer="21"/>
<circle x="0" y="0" radius="0.75" width="0.1524" layer="21"/>
<smd name="1" x="0" y="1.25" dx="0.5" dy="0.45" layer="1"/>
<smd name="2" x="0" y="-1.25" dx="0.55" dy="0.5" layer="1"/>
<text x="-1.4" y="-1.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.7" y="-1.5" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.55" x2="0.15" y2="0.55" layer="21"/>
<rectangle x1="-0.55" y1="-0.15" x2="0.55" y2="0.15" layer="21"/>
</package>
<package name="CTRIMCTZ3" urn="urn:adsk.eagle:footprint:23289/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; AVX</description>
<wire x1="-1.45" y1="-2.15" x2="-1.45" y2="0.75" width="0.254" layer="51"/>
<wire x1="-1.45" y1="0.75" x2="-0.45" y2="2.1" width="0.254" layer="51"/>
<wire x1="-0.45" y1="2.1" x2="0.45" y2="2.1" width="0.254" layer="51"/>
<wire x1="0.45" y1="2.1" x2="1.45" y2="0.75" width="0.254" layer="51"/>
<wire x1="1.45" y1="0.75" x2="1.45" y2="-2.15" width="0.254" layer="51"/>
<wire x1="1.45" y1="-2.15" x2="-1.45" y2="-2.15" width="0.254" layer="51"/>
<wire x1="-0.6" y1="-2.15" x2="-1.45" y2="-2.15" width="0.254" layer="21"/>
<wire x1="-1.45" y1="-2.15" x2="-1.45" y2="0.75" width="0.254" layer="21"/>
<wire x1="-1.45" y1="0.75" x2="-0.45" y2="2.1" width="0.254" layer="21"/>
<wire x1="0.6" y1="-2.15" x2="1.45" y2="-2.15" width="0.254" layer="21"/>
<wire x1="1.45" y1="-2.15" x2="1.45" y2="0.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="0.75" x2="0.45" y2="2.1" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.1" width="0.1524" layer="21"/>
<smd name="1" x="0" y="1.95" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0" y="-1.95" dx="0.78" dy="0.6" layer="1"/>
<text x="-1.85" y="-2.3" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.1" y="-2.3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-0.85" x2="0.25" y2="0.85" layer="21"/>
<rectangle x1="-0.85" y1="-0.25" x2="0.85" y2="0.25" layer="21"/>
</package>
<package name="CTRIMTZBX4" urn="urn:adsk.eagle:footprint:23290/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; muRata</description>
<wire x1="-1.9" y1="2.15" x2="-1.9" y2="-2.15" width="0.254" layer="51"/>
<wire x1="-1.9" y1="-2.15" x2="1.9" y2="-2.15" width="0.254" layer="51"/>
<wire x1="1.9" y1="-2.15" x2="1.9" y2="2.15" width="0.254" layer="51"/>
<wire x1="1.9" y1="2.15" x2="-1.9" y2="2.15" width="0.254" layer="51"/>
<wire x1="-1.05" y1="-2.15" x2="-1.9" y2="-2.15" width="0.254" layer="21"/>
<wire x1="-1.9" y1="-2.15" x2="-1.9" y2="2.15" width="0.254" layer="21"/>
<wire x1="-1.9" y1="2.15" x2="-1.05" y2="2.15" width="0.254" layer="21"/>
<wire x1="1.05" y1="-2.15" x2="1.9" y2="-2.15" width="0.254" layer="21"/>
<wire x1="1.9" y1="-2.15" x2="1.9" y2="2.15" width="0.254" layer="21"/>
<wire x1="1.9" y1="2.15" x2="1.05" y2="2.15" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.1524" layer="51"/>
<smd name="1" x="0" y="1.8" dx="1.6" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.8" dx="1.6" dy="1.2" layer="1"/>
<text x="-2.3" y="-2.25" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.55" y="-2.25" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.2" x2="0.25" y2="1.2" layer="51"/>
<rectangle x1="-1.2" y1="-0.25" x2="1.2" y2="0.25" layer="51"/>
</package>
<package name="CTRIMCV05" urn="urn:adsk.eagle:footprint:23291/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; BC-Components</description>
<wire x1="-2.8" y1="0" x2="2.8" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="-2.8" y1="0" x2="-1.05" y2="2.6" width="0.254" layer="21" curve="-68.064256"/>
<wire x1="1.05" y1="2.6" x2="2.8" y2="0" width="0.254" layer="21" curve="-68.064256"/>
<wire x1="-2.8" y1="0" x2="-2.8" y2="-2.9" width="0.254" layer="21"/>
<wire x1="-2.8" y1="-2.9" x2="-1.1" y2="-2.9" width="0.254" layer="21"/>
<wire x1="-1.1" y1="-2.9" x2="1.15" y2="-2.9" width="0.254" layer="51"/>
<wire x1="1.15" y1="-2.9" x2="2.8" y2="-2.9" width="0.254" layer="21"/>
<wire x1="2.8" y1="-2.9" x2="2.8" y2="0" width="0.254" layer="21"/>
<wire x1="-1.95" y1="-1.15" x2="-1.95" y2="1.15" width="0.1524" layer="51"/>
<wire x1="-1.95" y1="1.15" x2="0" y2="2.25" width="0.1524" layer="51"/>
<wire x1="0" y1="2.25" x2="1.95" y2="1.15" width="0.1524" layer="51"/>
<wire x1="1.95" y1="1.15" x2="1.95" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="1.95" y1="-1.1" x2="0" y2="-2.25" width="0.1524" layer="51"/>
<wire x1="0" y1="-2.25" x2="-1.95" y2="-1.15" width="0.1524" layer="51"/>
<wire x1="-1.95" y1="-1.15" x2="-1.95" y2="1.15" width="0.1524" layer="21"/>
<wire x1="-1.95" y1="1.15" x2="-0.7" y2="1.85" width="0.1524" layer="21"/>
<wire x1="0.7" y1="1.85" x2="1.95" y2="1.15" width="0.1524" layer="21"/>
<wire x1="1.95" y1="1.15" x2="1.95" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-1.95" y1="-1.15" x2="-0.7" y2="-1.85" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-1.85" x2="1.95" y2="-1.1" width="0.1524" layer="21"/>
<pad name="1" x="0" y="2.5" drill="1"/>
<pad name="2" x="0" y="-2.5" drill="1"/>
<text x="-3.3" y="-3.05" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.5" y="-3.05" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.9" y1="-0.5" x2="1.95" y2="0.5" layer="21"/>
</package>
<package name="CTRIMTZ03" urn="urn:adsk.eagle:footprint:23292/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; muRata</description>
<wire x1="1.45" y1="-2.5" x2="-1.45" y2="-2.5" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-2.5" x2="-1.05" y2="2.7" width="0.254" layer="21" curve="-128.646369"/>
<wire x1="1.05" y1="2.7" x2="1.45" y2="-2.5" width="0.254" layer="21" curve="-128.646369"/>
<wire x1="-1.05" y1="2.7" x2="1.05" y2="2.7" width="0.254" layer="51" curve="-42.501011"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1" x="0" y="2.5" drill="1"/>
<pad name="2" x="0" y="-2.5" drill="1"/>
<text x="-3.3" y="-2.6" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.6" y="-2.6" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.35" y1="-1.2" x2="0.35" y2="1.2" layer="21"/>
<rectangle x1="-1.2" y1="-0.35" x2="1.2" y2="0.35" layer="21"/>
</package>
<package name="CTRIM808-BC" urn="urn:adsk.eagle:footprint:23276/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; BC-Components</description>
<wire x1="-3.4036" y1="1.016" x2="-1.016" y2="3.4036" width="0.254" layer="21" curve="-56.758486"/>
<wire x1="1.016" y1="3.4036" x2="3.4036" y2="1.016" width="0.254" layer="21" curve="-56.758486"/>
<wire x1="-3.4036" y1="-1.016" x2="3.4036" y2="-1.016" width="0.254" layer="21" curve="146.758486"/>
<circle x="0" y="0" radius="3.556" width="0.254" layer="51"/>
<circle x="0" y="0" radius="1.271" width="0.1524" layer="21"/>
<pad name="2" x="0" y="3.6068" drill="1.3"/>
<pad name="1" x="-3.6068" y="0" drill="1.3"/>
<pad name="3" x="3.6068" y="0" drill="1.3"/>
<text x="-4.4684" y="-3.4056" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.77" y="-3.4056" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="-0.3048" x2="1.2192" y2="0.3048" layer="21"/>
</package>
<package name="CTRIM808-1" urn="urn:adsk.eagle:footprint:23293/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; STELCO GmbH&lt;p&gt;
diameter 8.6 mm, grid 3.55 mm</description>
<wire x1="-3.4925" y1="1.5227" x2="3.4925" y2="1.5227" width="0.1524" layer="21" curve="-132.886424"/>
<wire x1="-1.5227" y1="-3.4925" x2="1.5227" y2="-3.4925" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="-3.4925" y1="1.5227" x2="-3.4925" y2="-1.5227" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="-3.4925" y1="-1.5227" x2="-1.5227" y2="-3.4925" width="0.1524" layer="21" curve="42.886424"/>
<wire x1="0.889" y1="-4.191" x2="-0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-3.705" x2="-0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-3.705" x2="0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-1.2443" y1="-0.254" x2="1.2443" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.2443" y1="0.254" x2="1.2443" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="0.889" x2="-4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="0.889" x2="-3.705" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-0.889" x2="-3.705" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.4925" y1="-1.5227" x2="3.4925" y2="1.5227" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="1.5227" y1="-3.4925" x2="3.4925" y2="-1.5227" width="0.1524" layer="21" curve="42.886424"/>
<wire x1="4.191" y1="0.889" x2="4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.705" y1="-0.889" x2="4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.705" y1="0.889" x2="4.191" y2="0.889" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.556" y="0" drill="1.3208"/>
<pad name="3" x="3.556" y="0" drill="1.3208"/>
<pad name="2" x="0" y="-3.556" drill="1.3208"/>
<text x="-4.445" y="-3.937" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.223" y="-4.318" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.254" x2="1.27" y2="0.254" layer="21"/>
<hole x="0" y="0" drill="3.302"/>
</package>
<package name="CTRIM808-BC7.5" urn="urn:adsk.eagle:footprint:23294/1" library_version="3">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; BC-Components</description>
<wire x1="1.3" y1="5.05" x2="3.35" y2="-1.6" width="0.254" layer="21" curve="-110.073805"/>
<wire x1="-3.35" y1="-1.6" x2="-1.3" y2="5.05" width="0.254" layer="21" curve="-110.073805"/>
<wire x1="-1.45" y1="-3" x2="1.45" y2="-3" width="0.254" layer="21" curve="39.851161"/>
<circle x="0" y="1" radius="4.25" width="0.254" layer="51"/>
<circle x="0" y="1" radius="1.271" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.3"/>
<pad name="2" x="0" y="5.08" drill="1.3"/>
<pad name="3" x="2.54" y="-2.54" drill="1.3"/>
<text x="-4.4684" y="-3.4056" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.77" y="-3.4056" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="0.6952" x2="1.2192" y2="1.3048" layer="21"/>
<hole x="0" y="1" drill="3"/>
</package>
</packages>
<packages3d>
<package3d name="CTRIM3008" urn="urn:adsk.eagle:package:23767/1" type="box" library_version="3">
<description>Trimm capacitor SMD STELCO GmbH</description>
<packageinstances>
<packageinstance name="CTRIM3008"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3018_11" urn="urn:adsk.eagle:package:23765/1" type="box" library_version="3">
<description>Trimm capacitor SMD STELCO GmbH</description>
<packageinstances>
<packageinstance name="CTRIM3018_11"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3018_12" urn="urn:adsk.eagle:package:23768/1" type="box" library_version="3">
<description>Trimm capacitor SMD STELCO GmbH</description>
<packageinstances>
<packageinstance name="CTRIM3018_12"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.427" urn="urn:adsk.eagle:package:23770/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from one side, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.427"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.428" urn="urn:adsk.eagle:package:23769/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from both sides, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.428"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.448" urn="urn:adsk.eagle:package:23777/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from one side, parallel to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.448"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.450" urn="urn:adsk.eagle:package:23771/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from both sides, parallel to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.450"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.452" urn="urn:adsk.eagle:package:23772/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from one side for automatic adjustment, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.452"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3050.504" urn="urn:adsk.eagle:package:23774/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 5 S-Triko 160 V DC for PCB mounting,
 Adjustable from one side, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3050.504"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3050.505" urn="urn:adsk.eagle:package:23773/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 5 S-Triko 160 V DC for PCB mounting,
 Adjustable from both sides, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3050.505"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3050.506" urn="urn:adsk.eagle:package:23775/1" type="box" library_version="3">
<description>Trimm capacitor STELCO GmbH
 5 S-Triko 160 V DC for PCB mounting,
 Adjustable from one side, parallel to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3050.506"/>
</packageinstances>
</package3d>
<package3d name="CTRIMCTZ2" urn="urn:adsk.eagle:package:23780/1" type="box" library_version="3">
<description>Trimm capacitor AVX</description>
<packageinstances>
<packageinstance name="CTRIMCTZ2"/>
</packageinstances>
</package3d>
<package3d name="CTRIMCTZ3" urn="urn:adsk.eagle:package:23776/1" type="box" library_version="3">
<description>Trimm capacitor AVX</description>
<packageinstances>
<packageinstance name="CTRIMCTZ3"/>
</packageinstances>
</package3d>
<package3d name="CTRIMTZBX4" urn="urn:adsk.eagle:package:23778/1" type="box" library_version="3">
<description>Trimm capacitor muRata</description>
<packageinstances>
<packageinstance name="CTRIMTZBX4"/>
</packageinstances>
</package3d>
<package3d name="CTRIMCV05" urn="urn:adsk.eagle:package:23784/1" type="box" library_version="3">
<description>Trimm capacitor BC-Components</description>
<packageinstances>
<packageinstance name="CTRIMCV05"/>
</packageinstances>
</package3d>
<package3d name="CTRIMTZ03" urn="urn:adsk.eagle:package:23779/1" type="box" library_version="3">
<description>Trimm capacitor muRata</description>
<packageinstances>
<packageinstance name="CTRIMTZ03"/>
</packageinstances>
</package3d>
<package3d name="CTRIM808-BC" urn="urn:adsk.eagle:package:23766/1" type="box" library_version="3">
<description>Trimm capacitor  BC-Components</description>
<packageinstances>
<packageinstance name="CTRIM808-BC"/>
</packageinstances>
</package3d>
<package3d name="CTRIM808-1" urn="urn:adsk.eagle:package:23781/1" type="box" library_version="3">
<description>Trimm capacitor  STELCO GmbH
diameter 8.6 mm, grid 3.55 mm</description>
<packageinstances>
<packageinstance name="CTRIM808-1"/>
</packageinstances>
</package3d>
<package3d name="CTRIM808-BC7.5" urn="urn:adsk.eagle:package:23783/1" type="box" library_version="3">
<description>Trimm capacitor  BC-Components</description>
<packageinstances>
<packageinstance name="CTRIM808-BC7.5"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C-TRIMM" urn="urn:adsk.eagle:symbol:23275/1" library_version="3">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-3.048" x2="-2.286" y2="0.762" width="0.3048" layer="94"/>
<wire x1="-3.048" y1="0" x2="-2.286" y2="0.762" width="0.3048" layer="94"/>
<wire x1="-2.286" y1="0.762" x2="-1.524" y2="1.524" width="0.3048" layer="94"/>
<wire x1="-3.048" y1="-3.302" x2="-3.048" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-3.302" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-2.794" y2="-1.778" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.286" y="-5.207" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="E" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-TRIMM" urn="urn:adsk.eagle:component:23799/2" prefix="C" uservalue="yes" library_version="3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="C-TRIMM" x="0" y="0"/>
</gates>
<devices>
<device name="3008" package="CTRIM3008">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23767/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3018_11" package="CTRIM3018_11">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23765/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3018_12" package="CTRIM3018_12">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23768/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.427" package="CTRIM3040.427">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23770/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.428" package="CTRIM3040.428">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23769/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.448" package="CTRIM3040.448">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23777/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.450" package="CTRIM3040.450">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23771/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.452" package="CTRIM3040.452">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23772/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3050.504" package="CTRIM3050.504">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23774/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3050.505" package="CTRIM3050.505">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23773/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3050.506" package="CTRIM3050.506">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23775/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTZ2" package="CTRIMCTZ2">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23780/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTZ3" package="CTRIMCTZ3">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23776/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="TZBX4" package="CTRIMTZBX4">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23778/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="CV05" package="CTRIMCV05">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23784/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="TZ03" package="CTRIMTZ03">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23779/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="808" package="CTRIM808-BC">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23766/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="808-1" package="CTRIM808-1">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23781/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="808-7.5" package="CTRIM808-BC7.5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23783/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="A" pinorder="1"/>
<pinmap gate="G$1" pin="E" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="IL-G-8P-S3T2-SA">
<pad name="4" x="0" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-9" y1="3.07" x2="11.54" y2="3.07" width="0.127" layer="21"/>
<wire x1="11.54" y1="3.07" x2="11.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="11.54" y1="2.54" x2="11.54" y2="-2.93" width="0.127" layer="21"/>
<wire x1="11.54" y1="-2.93" x2="-9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-9" y1="-2.93" x2="-9" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="-9" y2="3.07" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="11.54" y2="2.54" width="0.127" layer="21"/>
<text x="-8.89" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.89" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-10.16" y="-2.54" size="1.27" layer="21">1</text>
<pad name="8" x="10.16" y="0" drill="1.1" shape="long" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANSVERTERCTL" prefix="CON">
<gates>
<gate name="VBAT" symbol="M" x="2.54" y="7.62"/>
<gate name="VREG" symbol="M" x="2.54" y="5.08"/>
<gate name="B0" symbol="M" x="2.54" y="2.54"/>
<gate name="B1" symbol="M" x="2.54" y="0"/>
<gate name="RXEN" symbol="M" x="2.54" y="-2.54"/>
<gate name="TXEN" symbol="M" x="2.54" y="-5.08"/>
<gate name="GND" symbol="M" x="2.54" y="-7.62"/>
<gate name="VSTBY" symbol="M" x="2.54" y="-10.16"/>
</gates>
<devices>
<device name="" package="IL-G-8P-S3T2-SA">
<connects>
<connect gate="B0" pin="S" pad="1"/>
<connect gate="B1" pin="S" pad="2"/>
<connect gate="GND" pin="S" pad="3"/>
<connect gate="RXEN" pin="S" pad="5"/>
<connect gate="TXEN" pin="S" pad="6"/>
<connect gate="VBAT" pin="S" pad="7"/>
<connect gate="VREG" pin="S" pad="8"/>
<connect gate="VSTBY" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="coplaner" width="1.5" drill="0">
<clearance class="1" value="0.00635"/>
</class>
</classes>
<parts>
<part name="X1" library="con-coax" deviceset="SMA-" device="142-0711-821/826" value="SMA-142-0711-821/826"/>
<part name="IC2" library="HMC574AMS8E" deviceset="HMC574AMS8E" device=""/>
<part name="S2" library="IC_Robotech" deviceset="MASWSS0115TR-3000" device=""/>
<part name="C1" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="C10" library="Passive_Robotech" deviceset="C" device="-1608" value="33p"/>
<part name="C11" library="Passive_Robotech" deviceset="C" device="-2012" value="100p"/>
<part name="HF1" library="Passive_Robotech" deviceset="TT67426D" device=""/>
<part name="SUPPLY1" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY8" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY9" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY10" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY11" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY15" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY16" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C16" library="Passive_Robotech" deviceset="C" device="-1608" value="0.001u"/>
<part name="C17" library="Passive_Robotech" deviceset="C" device="-1608" value="100p"/>
<part name="C18" library="Passive_Robotech" deviceset="C" device="-1608" value="33p"/>
<part name="SUPPLY17" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY18" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C19" library="Passive_Robotech" deviceset="C" device="-1608" value="100p"/>
<part name="C20" library="Passive_Robotech" deviceset="C" device="-1608" value="100p"/>
<part name="C24" library="Passive_Robotech" deviceset="C" device="-3225" value="10u"/>
<part name="SUPPLY26" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$1" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="C26" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="C27" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="U$5" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="VIA_1" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_2" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_3" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_4" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="SUPPLY33" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="X2" library="con-coax" deviceset="SMA-" device="142-0711-821/826" value="SMA-142-0711-821/826"/>
<part name="SUPPLY6" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY7" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C34" library="Passive_Robotech" deviceset="C" device="-1608" value="47p"/>
<part name="SUPPLY43" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C42" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C43" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="IC1" library="IC_Robotech" deviceset="BGA616" device=""/>
<part name="IC3" library="IC_Robotech" deviceset="BGA2800" device=""/>
<part name="IC4" library="IC_Robotech" deviceset="M67715" device=""/>
<part name="C2" library="Passive_Robotech" deviceset="C" device="-2012" value="33p"/>
<part name="SUPPLY2" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY3" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="F1" library="Passive_Robotech" deviceset="DEA162025LT-5003C3" device=""/>
<part name="SUPPLY4" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R1" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R2" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R3" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY5" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY12" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="T1" library="macom" deviceset="MABA-010129-CT4A40" device=""/>
<part name="T2" library="macom" deviceset="MABA-010129-CT4A40" device=""/>
<part name="D1" library="Diode_Robotech" deviceset="BAT15-099R" device=""/>
<part name="SUPPLY13" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L1" library="Passive_Robotech" deviceset="L" device="-1608" value="22n"/>
<part name="C3" library="Passive_Robotech" deviceset="C" device="-2012" value="8.2p"/>
<part name="R4" library="Passive_Robotech" deviceset="R" device="-2012" value="51"/>
<part name="SUPPLY14" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="TR1" library="Transistor_Robotech" deviceset="BFU730F" device=""/>
<part name="R7" library="Passive_Robotech" deviceset="R" device="-2012" value="220"/>
<part name="R8" library="Passive_Robotech" deviceset="R" device="-2012" value="220"/>
<part name="R9" library="Passive_Robotech" deviceset="R" device="-2012" value="15k"/>
<part name="R10" library="Passive_Robotech" deviceset="R" device="-2012" value="22"/>
<part name="R11" library="Passive_Robotech" deviceset="R" device="-2012" value="7.5k"/>
<part name="SUPPLY19" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C4" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="L2" library="Passive_Robotech" deviceset="L" device="-1608" value="62n"/>
<part name="L3" library="Passive_Robotech" deviceset="L" device="-1608" value="8.2n"/>
<part name="C5" library="Passive_Robotech" deviceset="C" device="-2012" value="100p"/>
<part name="SUPPLY20" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY21" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY22" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C7" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-TRIMM" device="CTZ3" package3d_urn="urn:adsk.eagle:package:23776/1" value="2p(ave)"/>
<part name="SUPPLY24" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY25" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY23" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L4" library="Passive_Robotech" deviceset="L" device="-1608" value="68n"/>
<part name="C8" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="C9" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="SUPPLY27" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY28" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C12" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="C13" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="C14" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="C15" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="SUPPLY29" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY30" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY31" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY32" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R5" library="Passive_Robotech" deviceset="R" device="-2012" value="68"/>
<part name="C21" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="SUPPLY34" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L5" library="Passive_Robotech" deviceset="L" device="/7"/>
<part name="L6" library="Passive_Robotech" deviceset="L" device="/7"/>
<part name="L7" library="Passive_Robotech" deviceset="L" device="/7"/>
<part name="C22" library="Passive_Robotech" deviceset="C" device="-3225" value="22u"/>
<part name="C23" library="Passive_Robotech" deviceset="C" device="-3225" value="47u"/>
<part name="Q1" library="Transistor_Robotech" deviceset="2SA1213" device=""/>
<part name="Q2" library="Transistor_Robotech" deviceset="2SA1213" device=""/>
<part name="P+1" library="Supply_Robotech" deviceset="VCC" device=""/>
<part name="P+2" library="Supply_Robotech" deviceset="VCC" device=""/>
<part name="P+3" library="Supply_Robotech" deviceset="VCC" device=""/>
<part name="IC5" library="IC_Robotech" deviceset="AZ1086?-*" device="H" technology="3.3"/>
<part name="SUPPLY36" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY37" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY38" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C25" library="Passive_Robotech" deviceset="C" device="-3225" value="10u"/>
<part name="C28" library="Passive_Robotech" deviceset="C" device="-3225" value="10u"/>
<part name="SUPPLY39" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY40" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C6" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="CON1" library="connector" deviceset="TRANSVERTERCTL" device=""/>
<part name="IC6" library="IC_Robotech" deviceset="74*02" device="D" technology="HC"/>
<part name="TR2" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="TR3" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="SUPPLY35" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY42" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R6" library="Passive_Robotech" deviceset="R" device="-6032" value="0.1"/>
<part name="L8" library="Passive_Robotech" deviceset="L" device="NR6028" value="33u"/>
<part name="TR4" library="Transistor_Robotech" deviceset="IRLML6344" device=""/>
<part name="D2" library="Diode_Robotech" deviceset="SS2040FL" device=""/>
<part name="SUPPLY41" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="IC7" library="IC_Robotech" deviceset="LTC1624" device=""/>
<part name="C29" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C30" library="Passive_Robotech" deviceset="C" device="-2012" value="0.001u"/>
<part name="C31" library="Passive_Robotech" deviceset="C" device="-2012" value="100p"/>
<part name="C32" library="Passive_Robotech" deviceset="C" device="-2012" value="470p"/>
<part name="SUPPLY44" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R12" library="Passive_Robotech" deviceset="R" device="-2012" value="6.8k"/>
<part name="R13" library="Passive_Robotech" deviceset="R" device="-2012" value="3.9k"/>
<part name="R14" library="Passive_Robotech" deviceset="R" device="-2012" value="680"/>
<part name="SUPPLY45" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY46" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY47" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="D3" library="Diode_Robotech" deviceset="1SS193" device=""/>
<part name="SUPPLY48" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C33" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY49" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C35" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="SUPPLY50" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="TR5" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="TR6" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="R15" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R16" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="U$2" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="SUPPLY53" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C36" library="Passive_Robotech" deviceset="C" device="-3225" value="47u"/>
<part name="C37" library="Passive_Robotech" deviceset="C" device="-3225" value="22u"/>
<part name="SUPPLY54" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY55" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON2" library="Connector_Robotech" deviceset="BNC" device="-5415025"/>
<part name="SUPPLY52" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY51" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R17" library="Passive_Robotech" deviceset="R" device="-2012" value="220"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="X1" gate="G1" x="38.1" y="-5.08"/>
<instance part="IC2" gate="G$1" x="335.28" y="-10.16" rot="R180"/>
<instance part="S2" gate="G$1" x="167.64" y="7.62" rot="R180"/>
<instance part="C1" gate="G$1" x="48.26" y="-5.08" rot="R90"/>
<instance part="C10" gate="G$1" x="129.54" y="10.16" rot="R90"/>
<instance part="C11" gate="G$1" x="185.42" y="12.7" rot="R90"/>
<instance part="HF1" gate="G$1" x="121.92" y="10.16"/>
<instance part="SUPPLY1" gate="GND" x="40.64" y="-17.78"/>
<instance part="SUPPLY8" gate="GND" x="124.46" y="0"/>
<instance part="SUPPLY9" gate="GND" x="121.92" y="0"/>
<instance part="SUPPLY10" gate="GND" x="119.38" y="0"/>
<instance part="SUPPLY11" gate="GND" x="170.18" y="2.54"/>
<instance part="SUPPLY15" gate="GND" x="317.5" y="5.08"/>
<instance part="SUPPLY16" gate="GND" x="350.52" y="-12.7"/>
<instance part="C16" gate="G$1" x="317.5" y="12.7"/>
<instance part="C17" gate="G$1" x="342.9" y="-5.08" rot="R90"/>
<instance part="C18" gate="G$1" x="327.66" y="12.7"/>
<instance part="SUPPLY17" gate="GND" x="327.66" y="5.08"/>
<instance part="SUPPLY18" gate="GND" x="304.8" y="-17.78"/>
<instance part="C19" gate="G$1" x="297.18" y="5.08"/>
<instance part="C20" gate="G$1" x="187.96" y="-15.24" rot="R90"/>
<instance part="C24" gate="G$1" x="91.44" y="106.68"/>
<instance part="SUPPLY26" gate="GND" x="91.44" y="99.06"/>
<instance part="U$1" gate="1" x="91.44" y="121.92"/>
<instance part="C26" gate="G$1" x="213.36" y="-5.08" rot="R90"/>
<instance part="C27" gate="G$1" x="259.08" y="-33.02" rot="R90"/>
<instance part="U$5" gate="1" x="335.28" y="17.78"/>
<instance part="VIA_1" gate="_EARTH" x="99.06" y="83.82"/>
<instance part="VIA_2" gate="_EARTH" x="99.06" y="81.28"/>
<instance part="VIA_3" gate="_EARTH" x="99.06" y="78.74"/>
<instance part="VIA_4" gate="_EARTH" x="99.06" y="76.2"/>
<instance part="SUPPLY33" gate="GND" x="91.44" y="71.12"/>
<instance part="X2" gate="G1" x="30.48" y="43.18"/>
<instance part="SUPPLY6" gate="GND" x="55.88" y="-2.54"/>
<instance part="SUPPLY7" gate="GND" x="93.98" y="-5.08"/>
<instance part="C34" gate="G$1" x="40.64" y="43.18" rot="R90"/>
<instance part="SUPPLY43" gate="GND" x="35.56" y="38.1"/>
<instance part="C42" gate="G$1" x="241.3" y="-50.8"/>
<instance part="C43" gate="G$1" x="213.36" y="0" rot="R90"/>
<instance part="IC1" gate="G$1" x="198.12" y="35.56"/>
<instance part="IC3" gate="G$1" x="210.82" y="-15.24" rot="MR0"/>
<instance part="IC4" gate="G$1" x="241.3" y="35.56"/>
<instance part="C2" gate="G$1" x="213.36" y="35.56" rot="R90"/>
<instance part="SUPPLY2" gate="GND" x="233.68" y="22.86"/>
<instance part="SUPPLY3" gate="GND" x="198.12" y="25.4"/>
<instance part="F1" gate="G$1" x="281.94" y="33.02"/>
<instance part="SUPPLY4" gate="GND" x="281.94" y="20.32"/>
<instance part="R1" gate="G$1" x="50.8" y="38.1" rot="R90"/>
<instance part="R2" gate="G$1" x="58.42" y="43.18" rot="R180"/>
<instance part="R3" gate="G$1" x="68.58" y="38.1" rot="R270"/>
<instance part="SUPPLY5" gate="GND" x="50.8" y="30.48"/>
<instance part="SUPPLY12" gate="GND" x="68.58" y="30.48"/>
<instance part="T1" gate="A" x="63.5" y="5.08"/>
<instance part="T2" gate="A" x="104.14" y="5.08" rot="MR0"/>
<instance part="D1" gate="G$1" x="83.82" y="5.08"/>
<instance part="SUPPLY13" gate="GND" x="111.76" y="-2.54"/>
<instance part="L1" gate="G$1" x="63.5" y="-5.08" rot="R90"/>
<instance part="C3" gate="G$1" x="73.66" y="-5.08" rot="R90"/>
<instance part="R4" gate="G$1" x="83.82" y="-5.08"/>
<instance part="SUPPLY14" gate="GND" x="88.9" y="-10.16"/>
<instance part="TR1" gate="G$1" x="266.7" y="-20.32" rot="MR0"/>
<instance part="R7" gate="G$1" x="248.92" y="-22.86" rot="R90"/>
<instance part="R8" gate="G$1" x="246.38" y="-45.72"/>
<instance part="R9" gate="G$1" x="264.16" y="-45.72"/>
<instance part="R10" gate="G$1" x="274.32" y="-38.1" rot="R90"/>
<instance part="R11" gate="G$1" x="274.32" y="-50.8" rot="R90"/>
<instance part="SUPPLY19" gate="GND" x="264.16" y="-27.94"/>
<instance part="C4" gate="G$1" x="279.4" y="-48.26"/>
<instance part="L2" gate="G$1" x="254" y="-22.86" rot="R180"/>
<instance part="L3" gate="G$1" x="274.32" y="-25.4" rot="R180"/>
<instance part="C5" gate="G$1" x="269.24" y="-33.02" rot="R270"/>
<instance part="SUPPLY20" gate="GND" x="264.16" y="-35.56"/>
<instance part="SUPPLY21" gate="GND" x="279.4" y="-55.88"/>
<instance part="SUPPLY22" gate="GND" x="274.32" y="-58.42"/>
<instance part="C7" gate="G$1" x="281.94" y="-20.32" rot="R90"/>
<instance part="SUPPLY24" gate="GND" x="210.82" y="-25.4"/>
<instance part="SUPPLY25" gate="GND" x="218.44" y="-7.62"/>
<instance part="SUPPLY23" gate="GND" x="241.3" y="-58.42"/>
<instance part="L4" gate="G$1" x="208.28" y="30.48"/>
<instance part="C8" gate="G$1" x="205.74" y="22.86" rot="R270"/>
<instance part="C9" gate="G$1" x="205.74" y="17.78" rot="R270"/>
<instance part="SUPPLY27" gate="GND" x="198.12" y="22.86" rot="R270"/>
<instance part="SUPPLY28" gate="GND" x="198.12" y="17.78" rot="R270"/>
<instance part="C12" gate="G$1" x="248.92" y="20.32"/>
<instance part="C13" gate="G$1" x="243.84" y="5.08"/>
<instance part="C14" gate="G$1" x="231.14" y="17.78" rot="R270"/>
<instance part="C15" gate="G$1" x="231.14" y="12.7" rot="R270"/>
<instance part="SUPPLY29" gate="GND" x="223.52" y="17.78" rot="R270"/>
<instance part="SUPPLY30" gate="GND" x="223.52" y="12.7" rot="R270"/>
<instance part="SUPPLY31" gate="GND" x="243.84" y="-2.54"/>
<instance part="SUPPLY32" gate="GND" x="248.92" y="12.7"/>
<instance part="R5" gate="G$1" x="210.82" y="12.7" rot="R270"/>
<instance part="C21" gate="G$1" x="208.28" y="7.62" rot="R270"/>
<instance part="SUPPLY34" gate="GND" x="198.12" y="7.62" rot="R270"/>
<instance part="L5" gate="G$1" x="226.06" y="7.62" rot="R90"/>
<instance part="L6" gate="G$1" x="238.76" y="7.62" rot="R90"/>
<instance part="L7" gate="G$1" x="248.92" y="7.62" rot="R90"/>
<instance part="C22" gate="G$1" x="213.36" y="129.54"/>
<instance part="C23" gate="G$1" x="223.52" y="109.22"/>
<instance part="Q1" gate="G$1" x="254" y="104.14"/>
<instance part="Q2" gate="G$1" x="195.58" y="76.2"/>
<instance part="P+1" gate="VCC" x="223.52" y="116.84"/>
<instance part="P+2" gate="VCC" x="256.54" y="114.3"/>
<instance part="P+3" gate="VCC" x="198.12" y="86.36"/>
<instance part="IC5" gate="G$1" x="210.82" y="66.04"/>
<instance part="SUPPLY36" gate="GND" x="210.82" y="55.88"/>
<instance part="SUPPLY37" gate="GND" x="223.52" y="101.6"/>
<instance part="SUPPLY38" gate="GND" x="213.36" y="121.92"/>
<instance part="C25" gate="G$1" x="228.6" y="60.96"/>
<instance part="C28" gate="G$1" x="195.58" y="63.5"/>
<instance part="SUPPLY39" gate="GND" x="228.6" y="53.34"/>
<instance part="SUPPLY40" gate="GND" x="195.58" y="55.88"/>
<instance part="C6" gate="G$1" x="231.14" y="-15.24" rot="R90"/>
<instance part="CON1" gate="VBAT" x="167.64" y="132.08" rot="R180"/>
<instance part="CON1" gate="VREG" x="86.36" y="111.76" rot="R180"/>
<instance part="CON1" gate="B0" x="111.76" y="99.06" rot="R180"/>
<instance part="CON1" gate="B1" x="111.76" y="96.52" rot="R180"/>
<instance part="CON1" gate="RXEN" x="114.3" y="66.04" rot="R180"/>
<instance part="CON1" gate="TXEN" x="114.3" y="50.8" rot="R180"/>
<instance part="CON1" gate="GND" x="99.06" y="88.9"/>
<instance part="CON1" gate="VSTBY" x="106.68" y="119.38"/>
<instance part="IC6" gate="A" x="157.48" y="86.36"/>
<instance part="IC6" gate="B" x="129.54" y="83.82"/>
<instance part="TR2" gate="G$1" x="185.42" y="63.5"/>
<instance part="TR3" gate="G$1" x="246.38" y="96.52"/>
<instance part="SUPPLY35" gate="GND" x="187.96" y="55.88"/>
<instance part="SUPPLY42" gate="GND" x="248.92" y="88.9"/>
<instance part="R6" gate="G$1" x="205.74" y="127" rot="R270"/>
<instance part="L8" gate="G$1" x="213.36" y="111.76" rot="R90"/>
<instance part="TR4" gate="G$1" x="200.66" y="114.3"/>
<instance part="D2" gate="G$1" x="205.74" y="101.6" rot="R90"/>
<instance part="SUPPLY41" gate="GND" x="205.74" y="96.52"/>
<instance part="IC7" gate="G$1" x="175.26" y="109.22"/>
<instance part="C29" gate="G$1" x="190.5" y="109.22"/>
<instance part="C30" gate="G$1" x="177.8" y="127" rot="R180"/>
<instance part="C31" gate="G$1" x="213.36" y="88.9"/>
<instance part="C32" gate="G$1" x="157.48" y="111.76" rot="R270"/>
<instance part="SUPPLY44" gate="GND" x="167.64" y="104.14"/>
<instance part="R12" gate="G$1" x="144.78" y="111.76" rot="R180"/>
<instance part="R13" gate="G$1" x="218.44" y="101.6" rot="R90"/>
<instance part="R14" gate="G$1" x="218.44" y="88.9" rot="R90"/>
<instance part="SUPPLY45" gate="GND" x="213.36" y="81.28"/>
<instance part="SUPPLY46" gate="GND" x="218.44" y="81.28"/>
<instance part="SUPPLY47" gate="GND" x="139.7" y="109.22"/>
<instance part="D3" gate="G$1" x="177.8" y="86.36" rot="R180"/>
<instance part="IC6" gate="P" x="96.52" y="109.22"/>
<instance part="SUPPLY48" gate="GND" x="96.52" y="96.52"/>
<instance part="C33" gate="G$1" x="101.6" y="114.3"/>
<instance part="SUPPLY49" gate="GND" x="101.6" y="106.68"/>
<instance part="C35" gate="G$1" x="259.08" y="17.78"/>
<instance part="SUPPLY50" gate="GND" x="259.08" y="10.16"/>
<instance part="TR5" gate="G$1" x="271.78" y="5.08"/>
<instance part="TR6" gate="G$1" x="210.82" y="-55.88"/>
<instance part="R15" gate="G$1" x="340.36" y="-27.94" rot="R270"/>
<instance part="R16" gate="G$1" x="342.9" y="-27.94" rot="R270"/>
<instance part="U$2" gate="1" x="340.36" y="-20.32"/>
<instance part="IC6" gate="C" x="66.04" y="91.44"/>
<instance part="IC6" gate="D" x="66.04" y="76.2"/>
<instance part="SUPPLY53" gate="GND" x="53.34" y="66.04"/>
<instance part="C36" gate="G$1" x="228.6" y="109.22"/>
<instance part="C37" gate="G$1" x="218.44" y="129.54"/>
<instance part="SUPPLY54" gate="GND" x="218.44" y="121.92"/>
<instance part="SUPPLY55" gate="GND" x="228.6" y="101.6"/>
<instance part="CON2" gate="G$1" x="353.06" y="-5.08" rot="MR0"/>
<instance part="SUPPLY52" gate="GND" x="213.36" y="-63.5"/>
<instance part="SUPPLY51" gate="GND" x="274.32" y="-2.54"/>
<instance part="R17" gate="G$1" x="205.74" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<pinref part="HF1" gate="G$1" pin="GND@1"/>
<wire x1="119.38" y1="2.54" x2="119.38" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<pinref part="HF1" gate="G$1" pin="GND@2"/>
<wire x1="121.92" y1="2.54" x2="121.92" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<pinref part="HF1" gate="G$1" pin="GND@3"/>
<wire x1="124.46" y1="2.54" x2="124.46" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="350.52" y1="-10.16" x2="350.52" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND_1"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="307.34" y1="-5.08" x2="304.8" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="304.8" y1="-5.08" x2="304.8" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GND_2"/>
<wire x1="304.8" y1="-7.62" x2="304.8" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="307.34" y1="-7.62" x2="304.8" y2="-7.62" width="0.1524" layer="91"/>
<junction x="304.8" y="-7.62"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="VIA_1" gate="_EARTH" pin="S"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="93.98" y1="83.82" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<wire x1="91.44" y1="83.82" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
<pinref part="VIA_4" gate="_EARTH" pin="S"/>
<wire x1="91.44" y1="81.28" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="91.44" y1="78.74" x2="91.44" y2="76.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="76.2" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="93.98" y1="76.2" x2="91.44" y2="76.2" width="0.1524" layer="91"/>
<junction x="91.44" y="76.2"/>
<pinref part="VIA_3" gate="_EARTH" pin="S"/>
<wire x1="93.98" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<junction x="91.44" y="78.74"/>
<pinref part="VIA_2" gate="_EARTH" pin="S"/>
<wire x1="93.98" y1="81.28" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
<junction x="91.44" y="81.28"/>
<wire x1="91.44" y1="88.9" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<junction x="91.44" y="83.82"/>
<wire x1="91.44" y1="86.36" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<pinref part="CON1" gate="GND" pin="S"/>
<wire x1="96.52" y1="88.9" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="G1" pin="2"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="35.56" y1="40.64" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="GND"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="X1" gate="G1" pin="2"/>
<wire x1="40.64" y1="-15.24" x2="40.64" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T1" gate="A" pin="PRI2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="T2" gate="A" pin="PRI2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="88.9" y1="-7.62" x2="88.9" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="A" pin="SEC_CT"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="96.52" y1="5.08" x2="93.98" y2="5.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="5.08" x2="93.98" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TR1" gate="G$1" pin="E"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<pinref part="C27" gate="G$1" pin="2"/>
<junction x="264.16" y="-33.02"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="218.44" y1="0" x2="218.44" y2="-5.08" width="0.1524" layer="91"/>
<junction x="218.44" y="-5.08"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="203.2" y1="7.62" x2="200.66" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="GND"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR2" gate="G$1" pin="E"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR3" gate="G$1" pin="E"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="GND"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="P" pin="GND"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="C" pin="I0"/>
<pinref part="IC6" gate="C" pin="I1"/>
<wire x1="53.34" y1="93.98" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC6" gate="D" pin="I0"/>
<wire x1="53.34" y1="88.9" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<junction x="53.34" y="88.9"/>
<pinref part="IC6" gate="D" pin="I1"/>
<wire x1="53.34" y1="78.74" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<junction x="53.34" y="78.74"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<junction x="53.34" y="73.66"/>
</segment>
<segment>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<pinref part="S2" gate="G$1" pin="GND"/>
<wire x1="170.18" y1="5.08" x2="170.18" y2="10.16" width="0.1524" layer="91"/>
<wire x1="170.18" y1="10.16" x2="167.64" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TR6" gate="G$1" pin="E"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR5" gate="G$1" pin="E"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="HF1" gate="G$1" pin="OUT"/>
<pinref part="C10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="RFC"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="340.36" y1="-5.08" x2="335.28" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="347.98" y1="-5.08" x2="350.52" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="297.18" y1="0" x2="297.18" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="RF1"/>
<wire x1="297.18" y1="-2.54" x2="307.34" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="335.28" y1="-2.54" x2="335.28" y2="15.24" width="0.1524" layer="91"/>
<wire x1="335.28" y1="15.24" x2="327.66" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="317.5" y1="15.24" x2="327.66" y2="15.24" width="0.1524" layer="91"/>
<junction x="327.66" y="15.24"/>
<pinref part="U$5" gate="1" pin="+5V"/>
<wire x1="335.28" y1="17.78" x2="335.28" y2="15.24" width="0.1524" layer="91"/>
<junction x="335.28" y="15.24"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="91.44" y1="111.76" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U$1" gate="1" pin="+5V"/>
<wire x1="91.44" y1="121.92" x2="91.44" y2="119.38" width="0.1524" layer="91"/>
<pinref part="CON1" gate="VREG" pin="S"/>
<wire x1="91.44" y1="119.38" x2="91.44" y2="111.76" width="0.1524" layer="91"/>
<wire x1="88.9" y1="111.76" x2="91.44" y2="111.76" width="0.1524" layer="91"/>
<junction x="91.44" y="111.76"/>
</segment>
<segment>
<pinref part="U$2" gate="1" pin="+5V"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="340.36" y1="-20.32" x2="340.36" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="340.36" y1="-22.86" x2="342.9" y2="-22.86" width="0.1524" layer="91"/>
<junction x="340.36" y="-22.86"/>
</segment>
</net>
<net name="N$12" class="1">
<segment>
<pinref part="X2" gate="G1" pin="1"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="33.02" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$1" pin="+IN"/>
<wire x1="190.5" y1="35.56" x2="190.5" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="RFIN"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="IC3" gate="G$1" pin="OUT"/>
<wire x1="193.04" y1="-15.24" x2="203.2" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="185.42" y1="-15.24" x2="185.42" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="RF1"/>
<wire x1="167.64" y1="7.62" x2="185.42" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="F1" gate="G$1" pin="OUT"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="294.64" y1="35.56" x2="297.18" y2="35.56" width="0.1524" layer="91"/>
<wire x1="297.18" y1="35.56" x2="297.18" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="RFOUT"/>
<pinref part="F1" gate="G$1" pin="IN"/>
<wire x1="266.7" y1="35.56" x2="269.24" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="45.72" y1="43.18" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="50.8" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<junction x="50.8" y="43.18"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="63.5" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<pinref part="T1" gate="A" pin="PRI1"/>
<wire x1="55.88" y1="10.16" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<wire x1="55.88" y1="20.32" x2="73.66" y2="20.32" width="0.1524" layer="91"/>
<wire x1="73.66" y1="20.32" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<junction x="68.58" y="43.18"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="X1" gate="G1" pin="1"/>
<wire x1="45.72" y1="-5.08" x2="40.64" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="T1" gate="A" pin="SEC1"/>
<pinref part="D1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="T1" gate="A" pin="SEC2"/>
<pinref part="D1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="T2" gate="A" pin="SEC1"/>
<pinref part="D1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="T2" gate="A" pin="SEC2"/>
<pinref part="D1" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="T2" gate="A" pin="PRI1"/>
<pinref part="HF1" gate="G$1" pin="IN"/>
<wire x1="111.76" y1="10.16" x2="116.84" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="58.42" y1="-5.08" x2="53.34" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-5.08" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="T1" gate="A" pin="SEC_CT"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
<junction x="71.12" y="-5.08"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="269.24" y1="-45.72" x2="274.32" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="274.32" y1="-43.18" x2="274.32" y2="-45.72" width="0.1524" layer="91"/>
<junction x="274.32" y="-45.72"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="279.4" y1="-45.72" x2="274.32" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="TR1" gate="G$1" pin="B"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="269.24" y1="-20.32" x2="274.32" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="E"/>
<wire x1="279.4" y1="-20.32" x2="274.32" y2="-20.32" width="0.1524" layer="91"/>
<junction x="274.32" y="-20.32"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="254" y1="-15.24" x2="254" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="254" y1="-15.24" x2="248.92" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-15.24" x2="248.92" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-15.24" x2="248.92" y2="-15.24" width="0.1524" layer="91"/>
<junction x="248.92" y="-15.24"/>
<pinref part="TR1" gate="G$1" pin="C"/>
<wire x1="264.16" y1="-15.24" x2="254" y2="-15.24" width="0.1524" layer="91"/>
<junction x="254" y="-15.24"/>
<wire x1="246.38" y1="-15.24" x2="238.76" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="236.22" y1="-15.24" x2="238.76" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="274.32" y1="-33.02" x2="274.32" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="271.78" y1="-33.02" x2="274.32" y2="-33.02" width="0.1524" layer="91"/>
<junction x="274.32" y="-33.02"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="248.92" y1="-27.94" x2="254" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="251.46" y1="-45.72" x2="254" y2="-45.72" width="0.1524" layer="91"/>
<junction x="254" y="-45.72"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="259.08" y1="-45.72" x2="254" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="254" y1="-27.94" x2="254" y2="-33.02" width="0.1524" layer="91"/>
<junction x="254" y="-27.94"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="254" y1="-33.02" x2="254" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-33.02" x2="254" y2="-33.02" width="0.1524" layer="91"/>
<junction x="254" y="-33.02"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="A"/>
<pinref part="IC2" gate="G$1" pin="RF2"/>
<wire x1="287.02" y1="-20.32" x2="287.02" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="287.02" y1="-10.16" x2="307.34" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VBB"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="243.84" y1="25.4" x2="243.84" y2="7.62" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="2"/>
<junction x="243.84" y="7.62"/>
<pinref part="L7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="VCC2"/>
<wire x1="248.92" y1="22.86" x2="248.92" y2="25.4" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="248.92" y1="22.86" x2="254" y2="22.86" width="0.1524" layer="91"/>
<wire x1="254" y1="22.86" x2="254" y2="7.62" width="0.1524" layer="91"/>
<junction x="248.92" y="22.86"/>
<pinref part="Q1" gate="G$1" pin="C"/>
<wire x1="256.54" y1="99.06" x2="256.54" y2="22.86" width="0.1524" layer="91"/>
<wire x1="256.54" y1="22.86" x2="254" y2="22.86" width="0.1524" layer="91"/>
<junction x="254" y="22.86"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="259.08" y1="20.32" x2="259.08" y2="22.86" width="0.1524" layer="91"/>
<wire x1="259.08" y1="22.86" x2="256.54" y2="22.86" width="0.1524" layer="91"/>
<junction x="256.54" y="22.86"/>
<pinref part="TR5" gate="G$1" pin="B"/>
<wire x1="269.24" y1="5.08" x2="266.7" y2="5.08" width="0.1524" layer="91"/>
<wire x1="266.7" y1="5.08" x2="266.7" y2="22.86" width="0.1524" layer="91"/>
<wire x1="266.7" y1="22.86" x2="259.08" y2="22.86" width="0.1524" layer="91"/>
<junction x="259.08" y="22.86"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VCC1"/>
<wire x1="238.76" y1="25.4" x2="238.76" y2="17.78" width="0.1524" layer="91"/>
<wire x1="238.76" y1="17.78" x2="236.22" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="236.22" y1="17.78" x2="233.68" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="233.68" y1="12.7" x2="233.68" y2="17.78" width="0.1524" layer="91"/>
<junction x="233.68" y="17.78"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="233.68" y1="7.62" x2="233.68" y2="12.7" width="0.1524" layer="91"/>
<junction x="233.68" y="12.7"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="231.14" y1="7.62" x2="233.68" y2="7.62" width="0.1524" layer="91"/>
<junction x="233.68" y="7.62"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="210.82" y1="7.62" x2="220.98" y2="7.62" width="0.1524" layer="91"/>
<junction x="210.82" y="7.62"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="OUT"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="205.74" y1="35.56" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="210.82" y1="35.56" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<junction x="208.28" y="35.56"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="208.28" y1="25.4" x2="208.28" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="208.28" y1="17.78" x2="208.28" y2="22.86" width="0.1524" layer="91"/>
<junction x="208.28" y="22.86"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="210.82" y1="17.78" x2="208.28" y2="17.78" width="0.1524" layer="91"/>
<junction x="208.28" y="17.78"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="223.52" y1="114.3" x2="223.52" y2="111.76" width="0.1524" layer="91"/>
<junction x="223.52" y="111.76"/>
<pinref part="L8" gate="G$1" pin="2"/>
<wire x1="218.44" y1="111.76" x2="223.52" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="218.44" y1="106.68" x2="218.44" y2="111.76" width="0.1524" layer="91"/>
<junction x="218.44" y="111.76"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="228.6" y1="111.76" x2="223.52" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<pinref part="Q1" gate="G$1" pin="E"/>
<wire x1="256.54" y1="111.76" x2="256.54" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="E"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="198.12" y1="81.28" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="C"/>
<pinref part="IC5" gate="G$1" pin="IN"/>
<wire x1="198.12" y1="71.12" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="195.58" y1="66.04" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
<wire x1="195.58" y1="68.58" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<junction x="198.12" y="68.58"/>
</segment>
</net>
<net name="OUT@2" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="OUT@1"/>
<wire x1="223.52" y1="68.58" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
<junction x="223.52" y="68.58"/>
<pinref part="IC5" gate="G$1" pin="OUT@2"/>
<wire x1="223.52" y1="66.04" x2="223.52" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="228.6" y1="63.5" x2="228.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="228.6" y1="66.04" x2="223.52" y2="66.04" width="0.1524" layer="91"/>
<junction x="223.52" y="66.04"/>
<wire x1="200.66" y1="0" x2="200.66" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="241.3" y1="-45.72" x2="241.3" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-45.72" x2="241.3" y2="-45.72" width="0.1524" layer="91"/>
<junction x="241.3" y="-45.72"/>
<wire x1="200.66" y1="0" x2="177.8" y2="0" width="0.1524" layer="91"/>
<wire x1="177.8" y1="0" x2="177.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="177.8" y1="45.72" x2="177.8" y2="48.26" width="0.1524" layer="91"/>
<wire x1="177.8" y1="48.26" x2="233.68" y2="48.26" width="0.1524" layer="91"/>
<wire x1="233.68" y1="48.26" x2="233.68" y2="66.04" width="0.1524" layer="91"/>
<wire x1="233.68" y1="66.04" x2="228.6" y2="66.04" width="0.1524" layer="91"/>
<junction x="228.6" y="66.04"/>
<pinref part="TR6" gate="G$1" pin="B"/>
<wire x1="208.28" y1="-55.88" x2="200.66" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-55.88" x2="200.66" y2="-45.72" width="0.1524" layer="91"/>
<junction x="200.66" y="-45.72"/>
<pinref part="R17" gate="G$1" pin="1"/>
<junction x="200.66" y="0"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="+IN"/>
<wire x1="226.06" y1="-15.24" x2="220.98" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-15.24" x2="218.44" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-15.24" x2="226.06" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="A"/>
<wire x1="335.28" y1="-10.16" x2="335.28" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-30.48" x2="335.28" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-33.02" x2="335.28" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-63.5" x2="233.68" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-63.5" x2="228.6" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-63.5" x2="228.6" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-50.8" x2="228.6" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-30.48" x2="195.58" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-30.48" x2="175.26" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-30.48" x2="139.7" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="340.36" y1="-33.02" x2="335.28" y2="-33.02" width="0.1524" layer="91"/>
<junction x="335.28" y="-33.02"/>
<pinref part="S2" gate="G$1" pin="V2"/>
<wire x1="139.7" y1="-30.48" x2="139.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="139.7" y1="12.7" x2="142.24" y2="12.7" width="0.1524" layer="91"/>
<pinref part="TR5" gate="G$1" pin="C"/>
<wire x1="274.32" y1="10.16" x2="292.1" y2="10.16" width="0.1524" layer="91"/>
<wire x1="292.1" y1="10.16" x2="292.1" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-27.94" x2="292.1" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-30.48" x2="335.28" y2="-30.48" width="0.1524" layer="91"/>
<junction x="335.28" y="-30.48"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="B"/>
<wire x1="335.28" y1="-7.62" x2="337.82" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-7.62" x2="337.82" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-35.56" x2="337.82" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-66.04" x2="226.06" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-66.04" x2="226.06" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-33.02" x2="213.36" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-33.02" x2="172.72" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-33.02" x2="142.24" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="342.9" y1="-33.02" x2="342.9" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="342.9" y1="-35.56" x2="337.82" y2="-35.56" width="0.1524" layer="91"/>
<junction x="337.82" y="-35.56"/>
<pinref part="S2" gate="G$1" pin="V1"/>
<wire x1="142.24" y1="-33.02" x2="142.24" y2="7.62" width="0.1524" layer="91"/>
<pinref part="TR6" gate="G$1" pin="C"/>
<wire x1="213.36" y1="-50.8" x2="213.36" y2="-33.02" width="0.1524" layer="91"/>
<junction x="213.36" y="-33.02"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="IC6" gate="B" pin="O"/>
<pinref part="IC6" gate="A" pin="I1"/>
<wire x1="142.24" y1="83.82" x2="144.78" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BANDSEL1" class="0">
<segment>
<pinref part="IC6" gate="B" pin="I1"/>
<pinref part="IC6" gate="B" pin="I0"/>
<wire x1="116.84" y1="81.28" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CON1" gate="B1" pin="S"/>
<wire x1="114.3" y1="96.52" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<wire x1="116.84" y1="96.52" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
<junction x="116.84" y="86.36"/>
<label x="116.84" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="BANDSEL0" class="0">
<segment>
<pinref part="CON1" gate="B0" pin="S"/>
<pinref part="IC6" gate="A" pin="I0"/>
<wire x1="114.3" y1="99.06" x2="144.78" y2="99.06" width="0.1524" layer="91"/>
<wire x1="144.78" y1="99.06" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
<label x="124.46" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXEN" class="0">
<segment>
<wire x1="129.54" y1="50.8" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="177.8" y1="50.8" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
<wire x1="180.34" y1="50.8" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<wire x1="195.58" y1="50.8" x2="243.84" y2="50.8" width="0.1524" layer="91"/>
<pinref part="TR3" gate="G$1" pin="B"/>
<wire x1="243.84" y1="50.8" x2="243.84" y2="96.52" width="0.1524" layer="91"/>
<pinref part="CON1" gate="TXEN" pin="S"/>
<wire x1="116.84" y1="50.8" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<label x="121.92" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="B"/>
<pinref part="TR3" gate="G$1" pin="C"/>
<wire x1="251.46" y1="104.14" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="248.92" y1="104.14" x2="248.92" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="TR2" gate="G$1" pin="C"/>
<wire x1="187.96" y1="68.58" x2="187.96" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="B"/>
<wire x1="187.96" y1="76.2" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="TR4" gate="G$1" pin="D"/>
<wire x1="205.74" y1="121.92" x2="205.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="205.74" y1="121.92" x2="177.8" y2="121.92" width="0.1524" layer="91"/>
<junction x="205.74" y="121.92"/>
<pinref part="IC7" gate="G$1" pin="SENSE-"/>
<wire x1="177.8" y1="121.92" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="167.64" y1="121.92" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="177.8" y1="124.46" x2="177.8" y2="121.92" width="0.1524" layer="91"/>
<junction x="177.8" y="121.92"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="BOOST"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="182.88" y1="111.76" x2="190.5" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="SW"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="182.88" y1="106.68" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<wire x1="182.88" y1="104.14" x2="190.5" y2="104.14" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="TR4" gate="G$1" pin="S"/>
<wire x1="205.74" y1="104.14" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="208.28" y1="111.76" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<junction x="205.74" y="111.76"/>
<wire x1="190.5" y1="104.14" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<junction x="190.5" y="104.14"/>
<junction x="205.74" y="104.14"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="TG"/>
<wire x1="182.88" y1="109.22" x2="195.58" y2="109.22" width="0.1524" layer="91"/>
<pinref part="TR4" gate="G$1" pin="G"/>
<wire x1="195.58" y1="109.22" x2="195.58" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VMAIN" class="0">
<segment>
<pinref part="CON1" gate="VBAT" pin="S"/>
<wire x1="170.18" y1="132.08" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="205.74" y1="132.08" x2="182.88" y2="132.08" width="0.1524" layer="91"/>
<wire x1="182.88" y1="132.08" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<pinref part="IC7" gate="G$1" pin="VIN"/>
<wire x1="182.88" y1="114.3" x2="182.88" y2="121.92" width="0.1524" layer="91"/>
<junction x="182.88" y="132.08"/>
<wire x1="182.88" y1="121.92" x2="182.88" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<junction x="177.8" y="132.08"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="213.36" y1="132.08" x2="205.74" y2="132.08" width="0.1524" layer="91"/>
<junction x="205.74" y="132.08"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="218.44" y1="132.08" x2="213.36" y2="132.08" width="0.1524" layer="91"/>
<junction x="213.36" y="132.08"/>
<label x="172.72" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="218.44" y1="93.98" x2="218.44" y2="96.52" width="0.1524" layer="91"/>
<wire x1="218.44" y1="96.52" x2="213.36" y2="96.52" width="0.1524" layer="91"/>
<junction x="218.44" y="96.52"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="213.36" y1="96.52" x2="213.36" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC7" gate="G$1" pin="VFB"/>
<wire x1="213.36" y1="93.98" x2="213.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="167.64" y1="109.22" x2="165.1" y2="109.22" width="0.1524" layer="91"/>
<wire x1="165.1" y1="109.22" x2="165.1" y2="96.52" width="0.1524" layer="91"/>
<wire x1="165.1" y1="96.52" x2="165.1" y2="93.98" width="0.1524" layer="91"/>
<wire x1="165.1" y1="93.98" x2="213.36" y2="93.98" width="0.1524" layer="91"/>
<junction x="213.36" y="93.98"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="ITH/RUN"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="167.64" y1="111.76" x2="162.56" y2="111.76" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="162.56" y1="111.76" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="180.34" y1="86.36" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="180.34" y1="91.44" x2="162.56" y2="91.44" width="0.1524" layer="91"/>
<wire x1="162.56" y1="91.44" x2="162.56" y2="111.76" width="0.1524" layer="91"/>
<junction x="162.56" y="111.76"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="152.4" y1="111.76" x2="149.86" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="IC6" gate="A" pin="O"/>
<wire x1="175.26" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VSTBY" class="0">
<segment>
<pinref part="CON1" gate="VSTBY" pin="S"/>
<pinref part="IC6" gate="P" pin="VCC"/>
<wire x1="104.14" y1="119.38" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="101.6" y1="119.38" x2="96.52" y2="119.38" width="0.1524" layer="91"/>
<wire x1="101.6" y1="116.84" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<junction x="101.6" y="119.38"/>
<label x="99.06" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXEN" class="0">
<segment>
<pinref part="TR2" gate="G$1" pin="B"/>
<wire x1="182.88" y1="63.5" x2="152.4" y2="63.5" width="0.1524" layer="91"/>
<wire x1="152.4" y1="63.5" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="CON1" gate="RXEN" pin="S"/>
<wire x1="116.84" y1="66.04" x2="124.46" y2="66.04" width="0.1524" layer="91"/>
<wire x1="124.46" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="129.54" y1="63.5" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<label x="121.92" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="S2" gate="G$1" pin="RFC"/>
<wire x1="134.62" y1="10.16" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="RF2"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="167.64" y1="12.7" x2="182.88" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="C43" gate="G$1" pin="1"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="210.82" y1="-5.08" x2="210.82" y2="0" width="0.1524" layer="91"/>
<junction x="210.82" y="-5.08"/>
<pinref part="IC3" gate="G$1" pin="VCC"/>
<wire x1="210.82" y1="-7.62" x2="210.82" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<junction x="210.82" y="0"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
