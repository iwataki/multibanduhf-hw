<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Parts" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Accent_neu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="NXP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="NXP_2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Transistor_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Transitor Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SOT343N" urn="urn:adsk.eagle:footprint:29696/1">
<description>&lt;b&gt;Small Outline Transistor; 4 leads&lt;/b&gt;&lt;p&gt;
Philips Semiconductors, SOT343N.pdf</description>
<wire x1="-1" y1="0.55" x2="1" y2="0.55" width="0.2032" layer="51"/>
<wire x1="1" y1="0.55" x2="1" y2="-0.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-0.55" x2="-1" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-1" y1="-0.55" x2="-1" y2="0.55" width="0.2032" layer="21"/>
<smd name="1" x="-0.5" y="-0.9" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.65" y="-0.9" dx="0.6" dy="0.8" layer="1"/>
<smd name="3" x="0.65" y="0.9" dx="0.6" dy="0.8" layer="1"/>
<smd name="4" x="-0.65" y="0.9" dx="0.6" dy="0.8" layer="1"/>
<text x="-1.1" y="1.45" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.1" y="-2.7" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.85" y1="-1.1" x2="-0.15" y2="-0.6" layer="51"/>
<rectangle x1="0.45" y1="-1.1" x2="0.85" y2="-0.6" layer="51"/>
<rectangle x1="0.45" y1="0.6" x2="0.85" y2="1.1" layer="51"/>
<rectangle x1="-0.85" y1="0.6" x2="-0.45" y2="1.1" layer="51"/>
</package>
<package name="SC70">
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="E" x="0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="B" x="-0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<rectangle x1="-0.15" y1="0.625" x2="0.15" y2="1.05" layer="51"/>
<rectangle x1="0.5" y1="-1.05" x2="0.8" y2="-0.625" layer="51"/>
<rectangle x1="-0.8" y1="-1.05" x2="-0.5" y2="-0.625" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
</package>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="DUAL-GATE-N_MOSFET">
<wire x1="-5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.302" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.032" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="-1.016" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-5.588" y1="-1.524" x2="-4.572" y2="-1.524" width="0.1016" layer="94"/>
<wire x1="-4.572" y1="-1.524" x2="-4.318" y2="-1.27" width="0.1016" layer="94"/>
<wire x1="-5.588" y1="-1.524" x2="-5.842" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-3.81" y1="-1.524" x2="-2.794" y2="-1.524" width="0.1016" layer="94"/>
<wire x1="-2.794" y1="-1.524" x2="-2.54" y2="-1.27" width="0.1016" layer="94"/>
<wire x1="-3.81" y1="-1.524" x2="-4.064" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-2.794" y1="-2.032" x2="-3.81" y2="-2.032" width="0.1016" layer="94"/>
<wire x1="-3.81" y1="-2.032" x2="-4.064" y2="-2.286" width="0.1016" layer="94"/>
<wire x1="-2.794" y1="-2.032" x2="-2.54" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-4.572" y1="-2.032" x2="-5.588" y2="-2.032" width="0.1016" layer="94"/>
<wire x1="-5.588" y1="-2.032" x2="-5.842" y2="-2.286" width="0.1016" layer="94"/>
<wire x1="-4.572" y1="-2.032" x2="-4.318" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-3.556" x2="-3.302" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-3.556" x2="2.54" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="0" x2="-3.302" y2="-3.556" width="0.1524" layer="94"/>
<circle x="2.54" y="-1.27" radius="0.3592" width="0" layer="94"/>
<circle x="-5.08" y="2.54" radius="0.3592" width="0" layer="94"/>
<circle x="-3.302" y="0" radius="0.3592" width="0" layer="94"/>
<circle x="-3.302" y="-3.556" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="-3.556" radius="0.3592" width="0" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.112" y="2.794" size="1.016" layer="93">G1</text>
<text x="-7.112" y="0.254" size="1.016" layer="93">G2</text>
<pin name="G1" x="-7.62" y="0" visible="pin" length="point" direction="pas" rot="R180"/>
<pin name="G2" x="-7.62" y="2.54" visible="pin" length="point" direction="pas" rot="R180"/>
<pin name="DRAIN" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="SOURCE" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="-1.27" y="0.762"/>
<vertex x="0" y="1.27"/>
<vertex x="0" y="0.254"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-5.08" y="-1.524"/>
<vertex x="-5.588" y="-0.762"/>
<vertex x="-4.572" y="-0.762"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-3.302" y="-1.524"/>
<vertex x="-3.81" y="-0.762"/>
<vertex x="-2.794" y="-0.762"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-3.302" y="-2.032"/>
<vertex x="-2.794" y="-2.794"/>
<vertex x="-3.81" y="-2.794"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-5.08" y="-2.032"/>
<vertex x="-4.572" y="-2.794"/>
<vertex x="-5.588" y="-2.794"/>
</polygon>
<wire x1="-5.08" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="NPN">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
</symbol>
<symbol name="PNP">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.086" y1="1.678" x2="1.578" y2="2.594" width="0.1524" layer="94"/>
<wire x1="1.578" y1="2.594" x2="0.516" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.516" y1="1.478" x2="2.086" y2="1.678" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.124" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="3SK294" prefix="TR">
<gates>
<gate name="G$1" symbol="DUAL-GATE-N_MOSFET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT343N">
<connects>
<connect gate="G$1" pin="DRAIN" pad="1"/>
<connect gate="G$1" pin="G1" pad="3"/>
<connect gate="G$1" pin="G2" pad="4"/>
<connect gate="G$1" pin="SOURCE" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DTC123E" prefix="TR">
<description>ROHM Digital Transistor</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="UA" package="SC70">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA" package="SC59">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DTA123E" prefix="TR">
<description>ROHM PNP Digital Transistor</description>
<gates>
<gate name="G$1" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="UA" package="SC70">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA" package="SC59">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Passive_Robotech">
<description>&lt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="FCZ-7MM">
<pad name="1" x="-2.5" y="2.5" drill="0.8" shape="octagon"/>
<pad name="2" x="-2.5" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="-2.5" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="6" x="2.5" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="5" x="2.5" y="0" drill="0.8" shape="octagon"/>
<pad name="4" x="2.5" y="2.5" drill="0.8" shape="octagon"/>
<pad name="7" x="0" y="3.81" drill="1.5" shape="octagon"/>
<pad name="8" x="0" y="-3.81" drill="1.5" shape="octagon"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<text x="-2.54" y="-6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="FCZ-10MM">
<pad name="1S" x="-3.5" y="3.5" drill="0.8" shape="octagon"/>
<pad name="1C" x="-3.5" y="0" drill="0.8" shape="octagon"/>
<pad name="1E" x="-3.5" y="-3.5" drill="0.8" shape="octagon"/>
<pad name="2S" x="3.5" y="3.5" drill="0.8" shape="octagon"/>
<pad name="2E" x="3.5" y="-3.5" drill="0.8" shape="octagon"/>
<wire x1="-5" y1="5" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="5" x2="-5" y2="5" width="0.127" layer="21"/>
<pad name="P$6" x="0" y="5" drill="1.6" shape="octagon"/>
<pad name="P$7" x="0" y="-5" drill="1.6" shape="octagon"/>
<text x="-2.54" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="6.35" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="B3942">
<smd name="1" x="-1.27" y="-2.26" dx="2" dy="0.65" layer="1" rot="R90"/>
<smd name="2" x="0" y="-2.26" dx="2" dy="0.65" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="-2.26" dx="2" dy="0.65" layer="1" rot="R90"/>
<smd name="4" x="2.5" y="0" dx="2" dy="0.65" layer="1" rot="R180"/>
<smd name="5" x="1.27" y="2.26" dx="2" dy="0.65" layer="1" rot="R270"/>
<smd name="6" x="0" y="2.26" dx="2" dy="0.65" layer="1" rot="R270"/>
<smd name="7" x="-1.27" y="2.26" dx="2" dy="0.65" layer="1" rot="R270"/>
<smd name="8" x="-2.2" y="0" dx="2.9" dy="0.65" layer="1"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<circle x="-1.27" y="-1.27" radius="0.5" width="0.127" layer="21"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="1005">
<description>Metric Code Size 1005</description>
<smd name="1" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
</package>
<package name="1608">
<description>Metric Code Size 1608</description>
<smd name="1" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
</package>
<package name="2012-R">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="3216">
<description>Metric Code Size 3216</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="3225">
<description>Metric Code Size 3225</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
</package>
<package name="4532">
<description>Metric Code Size 4532</description>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="5650">
<description>Metric Code Size 5650</description>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
</package>
<package name="250-80">
<wire x1="12.5" y1="4" x2="-12.5" y2="4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="4" x2="-12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-4" x2="12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="12.5" y1="-4" x2="12.5" y2="4" width="0.127" layer="21"/>
<pad name="P$1" x="-15.24" y="0" drill="1.2" shape="octagon"/>
<pad name="P$2" x="15.24" y="0" drill="1.2" shape="octagon"/>
<rectangle x1="-15.24" y1="-0.508" x2="-12.573" y2="0.508" layer="51"/>
<rectangle x1="12.573" y1="-0.508" x2="15.24" y2="0.508" layer="51"/>
</package>
<package name="L5-7,5">
<pad name="P$1" x="2.5" y="0" drill="0.8"/>
<pad name="P$2" x="-2.5" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="0.5" y1="0" x2="1.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<text x="-3" y="-2.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.5" y="-4" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L22">
<pad name="P$1" x="11.43" y="0" drill="2.8" diameter="6.4516"/>
<pad name="P$2" x="-11.43" y="0" drill="2.8" diameter="6.4516"/>
<circle x="0" y="0" radius="11.359225" width="0.127" layer="21"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T50H">
<circle x="0" y="0" radius="2.83980625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="-6.35" drill="1"/>
<pad name="P$2" x="2.54" y="-6.35" drill="1"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T50V">
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T20V">
<pad name="P$1" x="-1.27" y="0" drill="0.8"/>
<pad name="P$2" x="1.27" y="0" drill="0.8"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<text x="-1.905" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-1.905" y="3.175" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="2125">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.8" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.8" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="SOLENOID-D5-L2.5">
<pad name="P$1" x="-1.27" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="27"/>
<wire x1="-0.635" y1="0" x2="0" y2="2.54" width="0.127" layer="27"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="0" width="0.127" layer="27"/>
</package>
<package name="SOLENOID-D5-L5">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
</package>
<package name="NR6028T">
<description>&lt;b&gt;NR6028&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2" layer="51"/>
<wire x1="-4.15" y1="4" x2="4.15" y2="4" width="0.1" layer="51"/>
<wire x1="4.15" y1="4" x2="4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="4.15" y1="-4" x2="-4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="-4.15" y1="-4" x2="-4.15" y2="4" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="21"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="TYS6045">
<smd name="1" x="-2.3" y="0" dx="6" dy="2.2" layer="1" rot="R90"/>
<smd name="2" x="2.3" y="0" dx="6" dy="2.2" layer="1" rot="R90"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="SRP1038">
<smd name="1" x="-4.4" y="0" dx="3.8" dy="3.5" layer="1"/>
<smd name="2" x="4.4" y="0" dx="3.8" dy="3.5" layer="1"/>
<wire x1="5" y1="5" x2="-5" y2="5" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="5" width="0.127" layer="21"/>
<text x="-2.54" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T37V">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<wire x1="-1.6256" y1="4.6736" x2="1.6256" y2="4.6736" width="0.127" layer="21"/>
<wire x1="1.6256" y1="4.6736" x2="1.6256" y2="-4.6736" width="0.127" layer="21"/>
<wire x1="1.6256" y1="-4.6736" x2="-1.6256" y2="-4.6736" width="0.127" layer="21"/>
<wire x1="-1.6256" y1="-4.6736" x2="-1.6256" y2="4.6736" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.127" layer="21"/>
<text x="-2.54" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
</package>
<package name="2012-C">
<smd name="1" x="-0.9207" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="0.9206" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1.381" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.381" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.889" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-0.635" x2="0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="6032">
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.909" y1="1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="1.524" x2="2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.909" y1="1.524" x2="2.909" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="-1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="CEMENTR/48">
<description>Cement Resistor
Width=48mm, Depth=10mm, Height=10mm</description>
<pad name="1" x="-29.21" y="0" drill="1.5" shape="octagon"/>
<pad name="2" x="29.21" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="29.21" y1="0" x2="28.194" y2="0" width="0.6096" layer="51"/>
<wire x1="-29.21" y1="0" x2="-28.194" y2="0" width="0.6096" layer="51"/>
<rectangle x1="27.305" y1="-0.3048" x2="28.1686" y2="0.3048" layer="21"/>
<rectangle x1="-28.1686" y1="-0.3048" x2="-27.305" y2="0.3048" layer="21"/>
<wire x1="27.305" y1="0" x2="24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-27.305" y1="0" x2="-24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-24" y1="5" x2="24" y2="5" width="0.127" layer="21"/>
<wire x1="24" y1="5" x2="24" y2="-5" width="0.127" layer="21"/>
<wire x1="24" y1="-5" x2="-24" y2="-5" width="0.127" layer="21"/>
<wire x1="-24" y1="-5" x2="-24" y2="5" width="0.127" layer="21"/>
</package>
<package name="FB-801-5PIN">
<pad name="P$1" x="-5.08" y="2.54" drill="0.8" shape="octagon"/>
<pad name="P$2" x="-5.08" y="0" drill="0.8" shape="octagon"/>
<pad name="P$3" x="-5.08" y="-2.54" drill="0.8" shape="octagon"/>
<pad name="P$4" x="5.08" y="-2.54" drill="0.8" shape="octagon"/>
<pad name="P$5" x="5.08" y="2.54" drill="0.8" shape="octagon"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<text x="-2.54" y="5.08" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="21">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CENTERTAP-COIL">
<pin name="4" x="5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="5" x="5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="1" x="-5.08" y="5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="3" x="-5.08" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SHELL">
<pin name="GND@1" x="0" y="2.54" visible="pin" length="short" direction="pas"/>
<pin name="GND@2" x="0" y="-2.54" visible="pin" length="short" direction="pas"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="FILTER">
<pin name="IN" x="-5.08" y="5.08" length="point" rot="R90"/>
<pin name="OUT" x="5.08" y="5.08" length="point" rot="R90"/>
<pin name="COM" x="0" y="-2.54" length="point" rot="R180"/>
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="3.175" y1="0.635" x2="-3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="L">
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3729" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="R">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FCZ-COIL" prefix="L">
<gates>
<gate name="G$1" symbol="CENTERTAP-COIL" x="0" y="0"/>
<gate name="G$2" symbol="SHELL" x="0" y="-12.7" addlevel="request"/>
</gates>
<devices>
<device name="7MM" package="FCZ-7MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="6"/>
<connect gate="G$2" pin="GND@1" pad="7"/>
<connect gate="G$2" pin="GND@2" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="FCZ-10MM">
<connects>
<connect gate="G$1" pin="1" pad="1S"/>
<connect gate="G$1" pin="2" pad="1C"/>
<connect gate="G$1" pin="3" pad="1E"/>
<connect gate="G$1" pin="4" pad="2S"/>
<connect gate="G$1" pin="5" pad="2E"/>
<connect gate="G$2" pin="GND@1" pad="P$6"/>
<connect gate="G$2" pin="GND@2" pad="P$7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="B3942" prefix="FIL">
<gates>
<gate name="G$1" symbol="FILTER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B3942">
<connects>
<connect gate="G$1" pin="COM" pad="2 3 4 6 7 8"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5(VERTICAL)" package="L5-7,5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22(VERTICAL)" package="L22">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50H" package="T50H">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50V" package="T50V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T20V" package="T20V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2125" package="2125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L2.5" package="SOLENOID-D5-L2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L5" package="SOLENOID-D5-L5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR6028" package="NR6028T">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR8040" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TYS6045" package="TYS6045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SRP1038" package="SRP1038">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T37V" package="T37V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7.5" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6032" package="6032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/48" package="CEMENTR/48">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CENTER-TAP-BALUN" prefix="T">
<gates>
<gate name="G$1" symbol="CENTERTAP-COIL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FB-801-5PIN">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Diode Library &lt;br&gt;
$Rev: 25542 $

&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
<package name="D-10">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<pad name="A" x="5.08" y="0" drill="1.5" shape="octagon"/>
<pad name="C" x="-5.08" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="3.175" y1="1.524" x2="-3.175" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.524" x2="3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.937" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.524" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="3.937" y1="0" x2="3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.524" x2="3.175" y2="0" width="0.1524" layer="21"/>
<rectangle x1="-2.286" y1="-1.524" x2="-1.524" y2="1.524" layer="21"/>
<text x="-3.175" y="-3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.524" width="0.1524" layer="21"/>
</package>
<package name="D1005">
<description>Metric Code Size 1005</description>
<smd name="K" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="A" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.127" y2="0" width="0.127" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.381" y2="0" width="0.127" layer="51"/>
<wire x1="-0.127" y1="0.127" x2="-0.127" y2="0" width="0.127" layer="51"/>
<wire x1="-0.127" y1="0" x2="-0.127" y2="-0.127" width="0.127" layer="51"/>
<wire x1="0.127" y1="0.127" x2="0.127" y2="-0.127" width="0.127" layer="51"/>
<wire x1="0.127" y1="-0.127" x2="-0.127" y2="0" width="0.127" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0.127" width="0.127" layer="51"/>
</package>
<package name="D-12.5">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<pad name="A" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<rectangle x1="-2.286" y1="-1.524" x2="-1.524" y2="1.524" layer="21"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="3.175" y1="1.524" x2="-3.175" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.524" x2="3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-5.207" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.524" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="5.207" y1="0" x2="3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.524" x2="3.175" y2="0" width="0.1524" layer="21"/>
<rectangle x1="-2.286" y1="-1.524" x2="-1.524" y2="1.524" layer="21"/>
<text x="-3.175" y="-3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.524" width="0.1524" layer="21"/>
</package>
<package name="D-2.5">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<wire x1="0.508" y1="0.762" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.508" y2="-0.762" width="0.1524" layer="21"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<text x="-1.651" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-0.508" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
</package>
<package name="D-5">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<pad name="A" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="1.397" y1="0.889" x2="-1.397" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.889" x2="1.397" y2="-0.889" width="0.1524" layer="21"/>
<rectangle x1="-1.016" y1="-0.889" x2="-0.508" y2="0.889" layer="21"/>
<wire x1="-1.397" y1="0" x2="-1.651" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0.889" x2="-1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<text x="-1.905" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="1.397" y1="-0.889" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0" x2="-1.397" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.397" y1="0" x2="1.397" y2="0.889" width="0.1524" layer="21"/>
</package>
<package name="D-7.5">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<pad name="A" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="1.905" y1="0.889" x2="-1.905" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="1.905" y2="-0.889" width="0.1524" layer="21"/>
<rectangle x1="-1.27" y1="-0.889" x2="-0.762" y2="0.889" layer="21"/>
<wire x1="-1.905" y1="0" x2="-2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0.889" x2="-1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="1.905" y2="0" width="0.1524" layer="21"/>
<text x="-1.905" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="1.905" y1="-0.889" x2="1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.905" y1="0" x2="1.905" y2="0.889" width="0.1524" layer="21"/>
</package>
<package name="D1608">
<description>Metric Code Size 1608</description>
<smd name="K" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="A" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="0.635" y1="0" x2="-0.254" y2="0" width="0.127" layer="51"/>
<wire x1="-0.254" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.254" y1="0.254" x2="-0.254" y2="0" width="0.127" layer="51"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.254" y1="0.254" x2="-0.254" y2="0" width="0.127" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.254" y1="-0.254" x2="0.254" y2="0.254" width="0.127" layer="51"/>
</package>
<package name="D2012">
<description>Metric Code Size 2012</description>
<smd name="K" x="-1.127" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="A" x="1.127" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="0" x2="-0.381" y2="0" width="0.127" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="0" width="0.127" layer="51"/>
<wire x1="-0.381" y1="0.508" x2="-0.381" y2="0" width="0.127" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="-0.508" width="0.127" layer="51"/>
<wire x1="-0.381" y1="0" x2="0.381" y2="0.508" width="0.127" layer="51"/>
<wire x1="0.381" y1="0.508" x2="0.381" y2="-0.508" width="0.127" layer="51"/>
<wire x1="0.381" y1="-0.508" x2="-0.381" y2="0" width="0.127" layer="51"/>
</package>
<package name="D3216">
<description>Metric Code Size 3216</description>
<smd name="K" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="A" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0.508" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.508" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="0.508" y2="0.508" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.508" x2="0.508" y2="-0.508" width="0.127" layer="51"/>
<wire x1="0.508" y1="-0.508" x2="-0.635" y2="0" width="0.127" layer="51"/>
</package>
<package name="D3225">
<description>Metric Code Size 3225</description>
<smd name="K" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="A" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="0.381" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.381" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
</package>
<package name="D4532">
<description>Metric Code Size 4532</description>
<smd name="K" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="A" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="1.27" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
</package>
<package name="D5650">
<description>Metric Code Size 5650</description>
<smd name="K" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="A" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="1.27" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="DO-214AC(SMA)">
<smd name="1" x="-1.88" y="0" dx="1.72" dy="1.88" layer="1"/>
<smd name="2" x="1.88" y="0" dx="1.72" dy="1.88" layer="1"/>
<wire x1="-2.25" y1="-1.395" x2="2.25" y2="-1.395" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.395" x2="2.25" y2="-0.825" width="0.127" layer="51"/>
<wire x1="2.25" y1="-0.825" x2="2.25" y2="0.825" width="0.127" layer="51"/>
<wire x1="2.25" y1="0.825" x2="2.25" y2="1.395" width="0.127" layer="51"/>
<wire x1="2.25" y1="1.395" x2="-2.25" y2="1.395" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.395" x2="-2.25" y2="0.825" width="0.127" layer="51"/>
<wire x1="-2.25" y1="0.825" x2="-2.25" y2="-0.825" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-0.825" x2="-2.25" y2="-1.395" width="0.127" layer="51"/>
<wire x1="-2.25" y1="0.825" x2="-2.64" y2="0.825" width="0.127" layer="51"/>
<wire x1="-2.64" y1="0.825" x2="-2.64" y2="-0.825" width="0.127" layer="51"/>
<wire x1="-2.64" y1="-0.825" x2="-2.25" y2="-0.825" width="0.127" layer="51"/>
<wire x1="2.25" y1="0.825" x2="2.64" y2="0.825" width="0.127" layer="51"/>
<wire x1="2.64" y1="0.825" x2="2.64" y2="-0.825" width="0.127" layer="51"/>
<wire x1="2.64" y1="-0.825" x2="2.25" y2="-0.825" width="0.127" layer="51"/>
<text x="-2.286" y="1.778" size="1.27" layer="25">&gt;NAME</text>
<text x="0.254" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
</package>
<package name="D5066">
<smd name="A" x="3.302" y="0" dx="3.048" dy="3.81" layer="1"/>
<smd name="C" x="-3.302" y="0" dx="3.048" dy="3.81" layer="1"/>
<wire x1="3.556" y1="2.54" x2="3.556" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.556" y1="-2.54" x2="-3.556" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.556" y1="-2.54" x2="-3.556" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.556" y1="2.54" x2="3.556" y2="2.54" width="0.127" layer="21"/>
<text x="-3.556" y="3.048" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-0.508" y1="1.016" x2="-0.508" y2="0" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="-0.508" y2="0" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.127" layer="21"/>
</package>
<package name="D-7.5-THICKLEAD">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<pad name="A" x="3.81" y="0" drill="1" shape="octagon"/>
<pad name="C" x="-3.81" y="0" drill="1" shape="octagon"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="1.905" y1="0.889" x2="-1.905" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="1.905" y2="-0.889" width="0.1524" layer="21"/>
<rectangle x1="-1.27" y1="-0.889" x2="-0.762" y2="0.889" layer="21"/>
<wire x1="-1.905" y1="0" x2="-2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0.889" x2="-1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="1.905" y2="0" width="0.1524" layer="21"/>
<text x="-1.905" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="1.905" y1="-0.889" x2="1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.905" y1="0" x2="1.905" y2="0.889" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PIND">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="D">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HSMP-3820" prefix="D">
<gates>
<gate name="G$1" symbol="PIND" x="5.08" y="2.54"/>
</gates>
<devices>
<device name="" package="SC59">
<connects>
<connect gate="G$1" pin="A" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="-10" package="D-10">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD1015" package="D1005">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-12.5" package="D-12.5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5" package="D-2.5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5" package="D-5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7.5" package="D-7.5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD1608" package="D1608">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD2012" package="D2012">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD3216" package="D3216">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD3225" package="D3225">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD4532" package="D4532">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD5650" package="D5650">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="DO-214AC(SMA)">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD5066" package="D5066">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D-7.5-1" package="D-7.5-THICKLEAD">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Supply symbol library&lt;br&gt;
$Rev: 25542 $ 
&lt;p&gt;
since 2008&lt;br&gt;
by&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Connector Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
Hideo Tanida&lt;br&gt;
Makoto Shimazu&lt;br&gt;
Mayu Kojima&lt;br&gt;
Takefumi Hiraki&lt;br&gt;
Soichiro Iwataki&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="5251-02-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="2.1321" y="-3.6561" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<text x="-2.8941" y="-2.8941" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-1.27" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<wire x1="1.27" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="-1.27" y1="9.906" x2="-1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-1.016" y1="10.414" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="8.382" layer="51"/>
<rectangle x1="1.016" y1="0.254" x2="1.524" y2="8.382" layer="51"/>
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="48"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
<text x="2.286" y="-2.794" size="1.27" layer="51" rot="R90">1</text>
<text x="-1.016" y="-2.794" size="1.27" layer="51" rot="R90">2</text>
</package>
<package name="5251-02-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-2.5131" y="3.2751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="2.032" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-0.762" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">2</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<text x="2.5131" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="-2.54" y1="2.921" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="-1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.27" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.905" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.397" x2="1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.397" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
</package>
<package name="IL-G-2P-S3T2-SA">
<pad name="1" x="-1.27" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-2.25" y1="3.07" x2="2.25" y2="3.07" width="0.127" layer="21"/>
<wire x1="2.25" y1="3.07" x2="2.25" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.25" y1="2.54" x2="2.25" y2="-2.93" width="0.127" layer="21"/>
<wire x1="2.25" y1="-2.93" x2="-2.25" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.93" x2="-2.25" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.54" x2="-2.25" y2="3.07" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.54" x2="2.25" y2="2.54" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="-2.54" size="1.27" layer="21">1</text>
</package>
<package name="VIA_3.2">
<pad name="P$1" x="0" y="0" drill="3.2" shape="octagon"/>
<circle x="0" y="0" radius="4" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3" width="0.127" layer="52"/>
</package>
</packages>
<symbols>
<symbol name="M-1">
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RT-2PIN" prefix="CON">
<gates>
<gate name="_SIGNAL" symbol="M-1" x="5.08" y="2.54"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-2.54"/>
</gates>
<devices>
<device name="-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IL-G-S" package="IL-G-2P-S3T2-SA">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EARTH" prefix="VIA_">
<gates>
<gate name="_EARTH" symbol="M-1" x="5.08" y="0"/>
</gates>
<devices>
<device name="_3.2" package="VIA_3.2">
<connects>
<connect gate="_EARTH" pin="S" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Robotech">
<description>&lt;Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QSOP16">
<smd name="1" x="-2.24" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="-1.6" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="-0.96" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="-0.32" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="5" x="0.32" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="6" x="0.96" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="7" x="1.6" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="8" x="2.24" y="-3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="9" x="2.24" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="10" x="1.6" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="11" x="0.96" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="12" x="0.32" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="13" x="-0.32" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="14" x="-0.96" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="15" x="-1.6" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<smd name="16" x="-2.24" y="3" dx="1.6" dy="0.4" layer="1" rot="R90"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<circle x="-3.2" y="-2.54" radius="0.4" width="0.127" layer="21"/>
<text x="3.81" y="-3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PWRN">
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
<symbol name="HMC245AQS16E">
<pin name="RFC" x="-12.7" y="-2.54" length="middle"/>
<pin name="RF3" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="B" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="A" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="RF2" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="RF1" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HMC245AQS16E" prefix="IC">
<gates>
<gate name="G$1" symbol="PWRN" x="-10.16" y="0"/>
<gate name="G$2" symbol="HMC245AQS16E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QSOP16">
<connects>
<connect gate="G$1" pin="GND" pad="2 3 4 5 7 11 13 15 16"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$2" pin="A" pad="10"/>
<connect gate="G$2" pin="B" pad="9"/>
<connect gate="G$2" pin="RF1" pad="14"/>
<connect gate="G$2" pin="RF2" pad="12"/>
<connect gate="G$2" pin="RF3" pad="6"/>
<connect gate="G$2" pin="RFC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="IL-G-8P-S3T2-SA">
<pad name="4" x="0" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-9" y1="3.07" x2="11.54" y2="3.07" width="0.127" layer="21"/>
<wire x1="11.54" y1="3.07" x2="11.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="11.54" y1="2.54" x2="11.54" y2="-2.93" width="0.127" layer="21"/>
<wire x1="11.54" y1="-2.93" x2="-9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-9" y1="-2.93" x2="-9" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="-9" y2="3.07" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="11.54" y2="2.54" width="0.127" layer="21"/>
<text x="-8.89" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.89" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-10.16" y="-2.54" size="1.27" layer="21">1</text>
<pad name="8" x="10.16" y="0" drill="1.1" shape="long" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANSVERTERCTL" prefix="CON">
<gates>
<gate name="VBAT" symbol="M" x="2.54" y="7.62"/>
<gate name="VREG" symbol="M" x="2.54" y="5.08"/>
<gate name="B0" symbol="M" x="2.54" y="2.54"/>
<gate name="B1" symbol="M" x="2.54" y="0"/>
<gate name="RXEN" symbol="M" x="2.54" y="-2.54"/>
<gate name="TXEN" symbol="M" x="2.54" y="-5.08"/>
<gate name="GND" symbol="M" x="2.54" y="-7.62"/>
<gate name="VSTBY" symbol="M" x="2.54" y="-10.16"/>
</gates>
<devices>
<device name="" package="IL-G-8P-S3T2-SA">
<connects>
<connect gate="B0" pin="S" pad="1"/>
<connect gate="B1" pin="S" pad="2"/>
<connect gate="GND" pin="S" pad="3"/>
<connect gate="RXEN" pin="S" pad="5"/>
<connect gate="TXEN" pin="S" pad="6"/>
<connect gate="VBAT" pin="S" pad="7"/>
<connect gate="VREG" pin="S" pad="8"/>
<connect gate="VSTBY" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="TR1" library="Transistor_Robotech" deviceset="3SK294" device=""/>
<part name="L1" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM" value="FCZ-COIL7MM-144"/>
<part name="D1" library="Diode_Robotech" deviceset="HSMP-3820" device=""/>
<part name="D2" library="Diode_Robotech" deviceset="HSMP-3820" device=""/>
<part name="D3" library="Diode_Robotech" deviceset="HSMP-3820" device=""/>
<part name="D4" library="Diode_Robotech" deviceset="HSMP-3820" device=""/>
<part name="FIL1" library="Passive_Robotech" deviceset="B3942" device=""/>
<part name="L2" library="Passive_Robotech" deviceset="L" device="-1608" value="180n"/>
<part name="L3" library="Passive_Robotech" deviceset="L" device="-1608" value="180n"/>
<part name="C1" library="Passive_Robotech" deviceset="C" device="-1608" value="2.2p"/>
<part name="C2" library="Passive_Robotech" deviceset="C" device="-1608" value="2.2p"/>
<part name="C3" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="C4" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="C5" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="C6" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="C7" library="Passive_Robotech" deviceset="C" device="-1608" value="4p"/>
<part name="C8" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="C9" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="R1" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R2" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R3" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R4" library="Passive_Robotech" deviceset="R" device="-2012" value="3.3k"/>
<part name="R5" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R6" library="Passive_Robotech" deviceset="R" device="-2012" value="5.1k"/>
<part name="R7" library="Passive_Robotech" deviceset="R" device="-2012" value="330"/>
<part name="R8" library="Passive_Robotech" deviceset="R" device="/7" value="10"/>
<part name="R9" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R10" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R11" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="SUPPLY1" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY2" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY3" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY4" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY5" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY6" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY7" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY8" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY9" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY10" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY11" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L4" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM" value="FCZ-COIL7MM-9"/>
<part name="L5" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM" value="FCZ-COIL7MM-9"/>
<part name="C10" library="Passive_Robotech" deviceset="C" device="/5" value="82p"/>
<part name="C11" library="Passive_Robotech" deviceset="C" device="/5" value="82p"/>
<part name="C12" library="Passive_Robotech" deviceset="C" device="/2.5" value="3p"/>
<part name="SUPPLY12" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY13" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C13" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="C14" library="Passive_Robotech" deviceset="C" device="/2.5" value="0.01u"/>
<part name="T1" library="Passive_Robotech" deviceset="CENTER-TAP-BALUN" device=""/>
<part name="T2" library="Passive_Robotech" deviceset="CENTER-TAP-BALUN" device=""/>
<part name="D5" library="Diode_Robotech" deviceset="DIODE" device="-5"/>
<part name="D6" library="Diode_Robotech" deviceset="DIODE" device="-5"/>
<part name="D7" library="Diode_Robotech" deviceset="DIODE" device="-5"/>
<part name="D8" library="Diode_Robotech" deviceset="DIODE" device="-5"/>
<part name="SUPPLY14" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY15" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY16" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R12" library="Passive_Robotech" deviceset="R" device="/7" value="300"/>
<part name="R13" library="Passive_Robotech" deviceset="R" device="/7" value="18"/>
<part name="R14" library="Passive_Robotech" deviceset="R" device="/7" value="300"/>
<part name="SUPPLY17" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY18" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON1" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="CON2" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="SUPPLY19" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY20" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C15" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="C16" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="C17" library="Passive_Robotech" deviceset="C" device="/2.5" value="0.01u"/>
<part name="C18" library="Passive_Robotech" deviceset="C" device="/2.5" value="0.01u"/>
<part name="SUPPLY22" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY23" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY24" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L6" library="Passive_Robotech" deviceset="L" device="-1608" value="1u"/>
<part name="R15" library="Passive_Robotech" deviceset="R" device="-2012" value="51"/>
<part name="SUPPLY25" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="IC2" library="IC_Robotech" deviceset="HMC245AQS16E" device=""/>
<part name="C21" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="C22" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="C23" library="Passive_Robotech" deviceset="C" device="-2012" value="0.01u"/>
<part name="SUPPLY30" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C24" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="SUPPLY31" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON3" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="CON4" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="CON5" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="SUPPLY32" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY33" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="VIA_1" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_2" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_3" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_4" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="SUPPLY34" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R16" library="Passive_Robotech" deviceset="R" device="/7" value="100"/>
<part name="R17" library="Passive_Robotech" deviceset="R" device="/7" value="100"/>
<part name="C25" library="Passive_Robotech" deviceset="C" device="/2.5" value="0.01u"/>
<part name="C26" library="Passive_Robotech" deviceset="C" device="/2.5" value="0.01u"/>
<part name="SUPPLY35" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY36" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY37" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY38" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY39" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON6" library="connector" deviceset="TRANSVERTERCTL" device=""/>
<part name="TR2" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="TR3" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="TR4" library="Transistor_Robotech" deviceset="DTA123E" device="KA"/>
<part name="TR5" library="Transistor_Robotech" deviceset="DTA123E" device="KA"/>
<part name="SUPPLY21" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY26" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="D9" library="Diode_Robotech" deviceset="DIODE" device="-5"/>
<part name="D10" library="Diode_Robotech" deviceset="DIODE" device="-5"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="TR1" gate="G$1" x="60.96" y="63.5"/>
<instance part="L1" gate="G$1" x="78.74" y="68.58"/>
<instance part="D1" gate="G$1" x="96.52" y="76.2" rot="R180"/>
<instance part="D2" gate="G$1" x="96.52" y="71.12" rot="R180"/>
<instance part="D3" gate="G$1" x="-17.78" y="68.58"/>
<instance part="D4" gate="G$1" x="-17.78" y="60.96"/>
<instance part="FIL1" gate="G$1" x="20.32" y="58.42"/>
<instance part="L2" gate="G$1" x="7.62" y="63.5" rot="R90"/>
<instance part="L3" gate="G$1" x="33.02" y="63.5" rot="R90"/>
<instance part="C1" gate="G$1" x="2.54" y="58.42"/>
<instance part="C2" gate="G$1" x="38.1" y="58.42"/>
<instance part="C3" gate="G$1" x="-5.08" y="63.5" rot="R90"/>
<instance part="C4" gate="G$1" x="43.18" y="63.5" rot="R90"/>
<instance part="C5" gate="G$1" x="63.5" y="53.34"/>
<instance part="C6" gate="G$1" x="45.72" y="76.2" rot="R270"/>
<instance part="C7" gate="G$1" x="68.58" y="68.58"/>
<instance part="C8" gate="G$1" x="63.5" y="83.82" rot="R270"/>
<instance part="C9" gate="G$1" x="91.44" y="58.42"/>
<instance part="R1" gate="G$1" x="-27.94" y="76.2" rot="R90"/>
<instance part="R2" gate="G$1" x="-22.86" y="76.2" rot="R90"/>
<instance part="R3" gate="G$1" x="-10.16" y="55.88" rot="R90"/>
<instance part="R4" gate="G$1" x="60.96" y="76.2" rot="R180"/>
<instance part="R5" gate="G$1" x="48.26" y="68.58" rot="R90"/>
<instance part="R6" gate="G$1" x="48.26" y="50.8" rot="R90"/>
<instance part="R7" gate="G$1" x="58.42" y="50.8" rot="R90"/>
<instance part="R8" gate="G$1" x="68.58" y="88.9" rot="R270"/>
<instance part="R9" gate="G$1" x="86.36" y="58.42" rot="R90"/>
<instance part="R10" gate="G$1" x="101.6" y="83.82" rot="R90"/>
<instance part="R11" gate="G$1" x="106.68" y="83.82" rot="R90"/>
<instance part="SUPPLY1" gate="GND" x="58.42" y="43.18"/>
<instance part="SUPPLY2" gate="GND" x="63.5" y="45.72"/>
<instance part="SUPPLY3" gate="GND" x="48.26" y="43.18"/>
<instance part="SUPPLY4" gate="GND" x="38.1" y="71.12"/>
<instance part="SUPPLY5" gate="GND" x="55.88" y="81.28"/>
<instance part="SUPPLY6" gate="GND" x="38.1" y="50.8"/>
<instance part="SUPPLY7" gate="GND" x="20.32" y="53.34"/>
<instance part="SUPPLY8" gate="GND" x="2.54" y="50.8"/>
<instance part="SUPPLY9" gate="GND" x="86.36" y="48.26"/>
<instance part="SUPPLY10" gate="GND" x="91.44" y="48.26"/>
<instance part="SUPPLY11" gate="GND" x="-10.16" y="45.72"/>
<instance part="L4" gate="G$1" x="38.1" y="17.78" rot="MR180"/>
<instance part="L5" gate="G$1" x="12.7" y="17.78" rot="MR0"/>
<instance part="C10" gate="G$1" x="20.32" y="17.78"/>
<instance part="C11" gate="G$1" x="30.48" y="17.78"/>
<instance part="C12" gate="G$1" x="22.86" y="22.86" rot="R90"/>
<instance part="SUPPLY12" gate="GND" x="20.32" y="7.62"/>
<instance part="SUPPLY13" gate="GND" x="30.48" y="7.62"/>
<instance part="C13" gate="G$1" x="-22.86" y="48.26" rot="R180"/>
<instance part="C14" gate="G$1" x="101.6" y="43.18" rot="R180"/>
<instance part="T1" gate="G$1" x="53.34" y="17.78" rot="R180"/>
<instance part="T2" gate="G$1" x="88.9" y="17.78"/>
<instance part="D5" gate="G$1" x="71.12" y="22.86"/>
<instance part="D6" gate="G$1" x="78.74" y="17.78" rot="R270"/>
<instance part="D7" gate="G$1" x="73.66" y="12.7" rot="R180"/>
<instance part="D8" gate="G$1" x="66.04" y="17.78" rot="R90"/>
<instance part="SUPPLY14" gate="GND" x="43.18" y="7.62"/>
<instance part="SUPPLY15" gate="GND" x="96.52" y="10.16"/>
<instance part="SUPPLY16" gate="GND" x="48.26" y="10.16"/>
<instance part="R12" gate="G$1" x="60.96" y="-2.54" rot="R90"/>
<instance part="R13" gate="G$1" x="68.58" y="2.54" rot="R180"/>
<instance part="R14" gate="G$1" x="76.2" y="-2.54" rot="R270"/>
<instance part="SUPPLY17" gate="GND" x="60.96" y="-10.16"/>
<instance part="SUPPLY18" gate="GND" x="76.2" y="-10.16"/>
<instance part="CON1" gate="_SIGNAL" x="-2.54" y="20.32" rot="R180"/>
<instance part="CON1" gate="_GND" x="-2.54" y="15.24" rot="R180"/>
<instance part="CON2" gate="_SIGNAL" x="91.44" y="2.54"/>
<instance part="CON2" gate="_GND" x="91.44" y="0"/>
<instance part="SUPPLY19" gate="GND" x="86.36" y="-2.54"/>
<instance part="SUPPLY20" gate="GND" x="7.62" y="12.7"/>
<instance part="C15" gate="G$1" x="-27.94" y="48.26" rot="R180"/>
<instance part="C16" gate="G$1" x="114.3" y="71.12" rot="R270"/>
<instance part="C17" gate="G$1" x="40.64" y="93.98"/>
<instance part="C18" gate="G$1" x="48.26" y="93.98"/>
<instance part="SUPPLY22" gate="GND" x="48.26" y="86.36"/>
<instance part="SUPPLY23" gate="GND" x="40.64" y="86.36"/>
<instance part="SUPPLY24" gate="GND" x="81.28" y="15.24"/>
<instance part="L6" gate="G$1" x="106.68" y="27.94" rot="R90"/>
<instance part="R15" gate="G$1" x="116.84" y="22.86" rot="R90"/>
<instance part="SUPPLY25" gate="GND" x="116.84" y="12.7"/>
<instance part="IC2" gate="G$1" x="157.48" y="101.6"/>
<instance part="IC2" gate="G$2" x="139.7" y="73.66"/>
<instance part="C21" gate="G$1" x="170.18" y="81.28" rot="R270"/>
<instance part="C22" gate="G$1" x="170.18" y="73.66" rot="R270"/>
<instance part="C23" gate="G$1" x="170.18" y="66.04" rot="R270"/>
<instance part="SUPPLY30" gate="GND" x="157.48" y="91.44"/>
<instance part="C24" gate="G$1" x="162.56" y="106.68"/>
<instance part="SUPPLY31" gate="GND" x="162.56" y="99.06"/>
<instance part="CON3" gate="_SIGNAL" x="182.88" y="81.28"/>
<instance part="CON3" gate="_GND" x="182.88" y="78.74"/>
<instance part="CON4" gate="_SIGNAL" x="182.88" y="73.66"/>
<instance part="CON4" gate="_GND" x="182.88" y="71.12"/>
<instance part="CON5" gate="_SIGNAL" x="182.88" y="66.04"/>
<instance part="CON5" gate="_GND" x="182.88" y="63.5"/>
<instance part="SUPPLY32" gate="GND" x="175.26" y="58.42"/>
<instance part="SUPPLY33" gate="GND" x="175.26" y="40.64"/>
<instance part="VIA_1" gate="_EARTH" x="177.8" y="106.68"/>
<instance part="VIA_2" gate="_EARTH" x="177.8" y="104.14"/>
<instance part="VIA_3" gate="_EARTH" x="177.8" y="101.6"/>
<instance part="VIA_4" gate="_EARTH" x="177.8" y="99.06"/>
<instance part="SUPPLY34" gate="GND" x="172.72" y="93.98"/>
<instance part="R16" gate="G$1" x="17.78" y="101.6" rot="R180"/>
<instance part="R17" gate="G$1" x="17.78" y="99.06" rot="R180"/>
<instance part="C25" gate="G$1" x="-7.62" y="93.98"/>
<instance part="C26" gate="G$1" x="-2.54" y="93.98"/>
<instance part="SUPPLY35" gate="GND" x="-7.62" y="86.36"/>
<instance part="SUPPLY36" gate="GND" x="-2.54" y="86.36"/>
<instance part="L5" gate="G$2" x="12.7" y="-2.54"/>
<instance part="L4" gate="G$2" x="35.56" y="-2.54"/>
<instance part="L1" gate="G$2" x="78.74" y="50.8"/>
<instance part="SUPPLY37" gate="GND" x="78.74" y="43.18"/>
<instance part="SUPPLY38" gate="GND" x="35.56" y="-10.16"/>
<instance part="SUPPLY39" gate="GND" x="10.16" y="-10.16"/>
<instance part="CON6" gate="VBAT" x="175.26" y="144.78"/>
<instance part="CON6" gate="VREG" x="175.26" y="111.76"/>
<instance part="CON6" gate="B0" x="180.34" y="48.26"/>
<instance part="CON6" gate="B1" x="180.34" y="50.8"/>
<instance part="CON6" gate="RXEN" x="175.26" y="134.62"/>
<instance part="CON6" gate="TXEN" x="175.26" y="116.84"/>
<instance part="CON6" gate="GND" x="180.34" y="45.72"/>
<instance part="CON6" gate="VSTBY" x="175.26" y="114.3"/>
<instance part="TR2" gate="G$1" x="137.16" y="134.62" rot="MR0"/>
<instance part="TR3" gate="G$1" x="137.16" y="116.84" rot="MR0"/>
<instance part="TR4" gate="G$1" x="93.98" y="132.08" rot="MR90"/>
<instance part="TR5" gate="G$1" x="93.98" y="114.3" rot="MR90"/>
<instance part="SUPPLY21" gate="GND" x="134.62" y="109.22"/>
<instance part="SUPPLY26" gate="GND" x="134.62" y="127"/>
<instance part="D9" gate="G$1" x="76.2" y="121.92" rot="R180"/>
<instance part="D10" gate="G$1" x="76.2" y="116.84" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="FIL1" gate="G$1" pin="IN"/>
<wire x1="12.7" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="FIL1" gate="G$1" pin="OUT"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="25.4" y1="63.5" x2="27.94" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="38.1" y1="63.5" x2="40.64" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="38.1" y1="60.96" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
<junction x="38.1" y="63.5"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="0" y1="63.5" x2="2.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="2.54" y1="60.96" x2="2.54" y2="63.5" width="0.1524" layer="91"/>
<junction x="2.54" y="63.5"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="TR1" gate="G$1" pin="G1"/>
<wire x1="48.26" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<junction x="48.26" y="63.5"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="48.26" y1="55.88" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="58.42" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<pinref part="TR1" gate="G$1" pin="SOURCE"/>
<wire x1="63.5" y1="58.42" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<junction x="63.5" y="55.88"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="TR1" gate="G$1" pin="DRAIN"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="63.5" y1="68.58" x2="73.66" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="3"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="73.66" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="73.66" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="68.58" y1="73.66" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="66.04" y1="76.2" x2="68.58" y2="76.2" width="0.1524" layer="91"/>
<wire x1="68.58" y1="76.2" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="68.58" y2="76.2" width="0.1524" layer="91"/>
<junction x="68.58" y="73.66"/>
<junction x="68.58" y="76.2"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="66.04" y1="83.82" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<junction x="68.58" y="83.82"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="48.26" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="53.34" y1="76.2" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="48.26" y1="73.66" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<junction x="48.26" y="76.2"/>
<pinref part="TR1" gate="G$1" pin="G2"/>
<wire x1="53.34" y1="66.04" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<junction x="53.34" y="76.2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="FIL1" gate="G$1" pin="COM"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="86.36" y1="50.8" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="91.44" y1="50.8" x2="91.44" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="58.42" y1="83.82" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="40.64" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="38.1" y1="76.2" x2="38.1" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="48.26" x2="-10.16" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L5" gate="G$1" pin="3"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="17.78" y1="12.7" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="20.32" y1="10.16" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<junction x="20.32" y="12.7"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="30.48" y1="10.16" x2="30.48" y2="12.7" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="33.02" y1="12.7" x2="30.48" y2="12.7" width="0.1524" layer="91"/>
<junction x="30.48" y="12.7"/>
</segment>
<segment>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<pinref part="T1" gate="G$1" pin="4"/>
<wire x1="48.26" y1="12.7" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="5"/>
<wire x1="93.98" y1="15.24" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="96.52" y1="15.24" x2="96.52" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON2" gate="_GND" pin="S"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON1" gate="_GND" pin="S"/>
<pinref part="L5" gate="G$1" pin="5"/>
<wire x1="2.54" y1="15.24" x2="7.62" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<junction x="7.62" y="15.24"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="83.82" y1="17.78" x2="81.28" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="116.84" y1="17.78" x2="116.84" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON3" gate="_GND" pin="S"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<wire x1="177.8" y1="78.74" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="175.26" y1="78.74" x2="175.26" y2="71.12" width="0.1524" layer="91"/>
<pinref part="CON4" gate="_GND" pin="S"/>
<wire x1="175.26" y1="71.12" x2="175.26" y2="63.5" width="0.1524" layer="91"/>
<wire x1="175.26" y1="63.5" x2="175.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="177.8" y1="71.12" x2="175.26" y2="71.12" width="0.1524" layer="91"/>
<junction x="175.26" y="71.12"/>
<pinref part="CON5" gate="_GND" pin="S"/>
<wire x1="177.8" y1="63.5" x2="175.26" y2="63.5" width="0.1524" layer="91"/>
<junction x="175.26" y="63.5"/>
</segment>
<segment>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="177.8" y1="45.72" x2="175.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="175.26" y1="45.72" x2="175.26" y2="43.18" width="0.1524" layer="91"/>
<pinref part="CON6" gate="GND" pin="S"/>
</segment>
<segment>
<pinref part="VIA_1" gate="_EARTH" pin="S"/>
<pinref part="VIA_2" gate="_EARTH" pin="S"/>
<wire x1="172.72" y1="106.68" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<pinref part="VIA_3" gate="_EARTH" pin="S"/>
<wire x1="172.72" y1="104.14" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<junction x="172.72" y="104.14"/>
<pinref part="VIA_4" gate="_EARTH" pin="S"/>
<wire x1="172.72" y1="101.6" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<junction x="172.72" y="101.6"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="172.72" y1="99.06" x2="172.72" y2="96.52" width="0.1524" layer="91"/>
<junction x="172.72" y="99.06"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<pinref part="L4" gate="G$1" pin="5"/>
<wire x1="43.18" y1="10.16" x2="43.18" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L5" gate="G$2" pin="GND@1"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<wire x1="12.7" y1="0" x2="10.16" y2="0" width="0.1524" layer="91"/>
<wire x1="10.16" y1="0" x2="10.16" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$2" pin="GND@2"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-5.08" x2="10.16" y2="-5.08" width="0.1524" layer="91"/>
<junction x="10.16" y="-5.08"/>
</segment>
<segment>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<pinref part="L4" gate="G$2" pin="GND@2"/>
<wire x1="35.56" y1="-7.62" x2="35.56" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$2" pin="GND@1"/>
<wire x1="35.56" y1="0" x2="35.56" y2="-5.08" width="0.1524" layer="91"/>
<junction x="35.56" y="-5.08"/>
</segment>
<segment>
<pinref part="L1" gate="G$2" pin="GND@2"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$2" pin="GND@1"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<junction x="78.74" y="48.26"/>
</segment>
<segment>
<pinref part="TR3" gate="G$1" pin="E"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR2" gate="G$1" pin="E"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="5"/>
<wire x1="83.82" y1="66.04" x2="86.36" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="86.36" y1="66.04" x2="86.36" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="91.44" y1="60.96" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
<wire x1="91.44" y1="66.04" x2="86.36" y2="66.04" width="0.1524" layer="91"/>
<junction x="86.36" y="66.04"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="4"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="83.82" y1="71.12" x2="93.98" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="93.98" y1="76.2" x2="93.98" y2="71.12" width="0.1524" layer="91"/>
<junction x="93.98" y="71.12"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="-15.24" y1="60.96" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="63.5" x2="-15.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="63.5" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="-15.24" y="63.5"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="63.5" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="60.96" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="-10.16" y="63.5"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="71.12" x2="-22.86" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="-22.86" y1="68.58" x2="-20.32" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="53.34" x2="-22.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="-22.86" y="68.58"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="71.12" x2="-27.94" y2="60.96" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="-27.94" y1="60.96" x2="-20.32" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="53.34" x2="-27.94" y2="60.96" width="0.1524" layer="91"/>
<junction x="-27.94" y="60.96"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="99.06" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="101.6" y1="76.2" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="101.6" y1="48.26" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="101.6" y="76.2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="99.06" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="106.68" y1="71.12" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="109.22" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<junction x="106.68" y="71.12"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="1"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="17.78" y1="22.86" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<junction x="20.32" y="22.86"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="27.94" y1="22.86" x2="30.48" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="30.48" y1="20.32" x2="30.48" y2="22.86" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="3"/>
<wire x1="33.02" y1="22.86" x2="30.48" y2="22.86" width="0.1524" layer="91"/>
<junction x="30.48" y="22.86"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="45.72" x2="-22.86" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="33.02" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="33.02" x2="101.6" y2="40.64" width="0.1524" layer="91"/>
<wire x1="101.6" y1="27.94" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<junction x="101.6" y="33.02"/>
<pinref part="L6" gate="G$1" pin="1"/>
<pinref part="T2" gate="G$1" pin="4"/>
<wire x1="93.98" y1="20.32" x2="101.6" y2="20.32" width="0.1524" layer="91"/>
<wire x1="101.6" y1="20.32" x2="101.6" y2="27.94" width="0.1524" layer="91"/>
<junction x="101.6" y="27.94"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="40.64" y1="101.6" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="101.6" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="86.36" y1="101.6" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
<wire x1="101.6" y1="101.6" x2="101.6" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="40.64" y1="96.52" x2="40.64" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="22.86" y1="101.6" x2="40.64" y2="101.6" width="0.1524" layer="91"/>
<junction x="40.64" y="101.6"/>
<pinref part="TR4" gate="G$1" pin="C"/>
<wire x1="88.9" y1="134.62" x2="86.36" y2="134.62" width="0.1524" layer="91"/>
<wire x1="86.36" y1="134.62" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<junction x="86.36" y="101.6"/>
<wire x1="86.36" y1="121.92" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="121.92" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<junction x="86.36" y="121.92"/>
<pinref part="D9" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="48.26" y1="99.06" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<wire x1="78.74" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="99.06" x2="106.68" y2="99.06" width="0.1524" layer="91"/>
<wire x1="106.68" y1="99.06" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="48.26" y1="96.52" x2="48.26" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="22.86" y1="99.06" x2="48.26" y2="99.06" width="0.1524" layer="91"/>
<junction x="48.26" y="99.06"/>
<pinref part="TR5" gate="G$1" pin="C"/>
<wire x1="88.9" y1="116.84" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<junction x="88.9" y="99.06"/>
<wire x1="78.74" y1="116.84" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="88.9" y="116.84"/>
<pinref part="D10" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="66.04" y1="20.32" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="66.04" y1="22.86" x2="68.58" y2="22.86" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="3"/>
<wire x1="58.42" y1="22.86" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<junction x="66.04" y="22.86"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="73.66" y1="22.86" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="78.74" y1="22.86" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="1"/>
<wire x1="83.82" y1="22.86" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<junction x="78.74" y="22.86"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="78.74" y1="12.7" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="1"/>
<wire x1="58.42" y1="12.7" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="10.16" x2="78.74" y2="10.16" width="0.1524" layer="91"/>
<wire x1="78.74" y1="10.16" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="78.74" y="12.7"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="71.12" y1="12.7" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="66.04" y1="12.7" x2="66.04" y2="15.24" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="3"/>
<wire x1="83.82" y1="12.7" x2="83.82" y2="7.62" width="0.1524" layer="91"/>
<wire x1="83.82" y1="7.62" x2="66.04" y2="7.62" width="0.1524" layer="91"/>
<wire x1="66.04" y1="7.62" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<junction x="66.04" y="12.7"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="63.5" y1="2.54" x2="60.96" y2="2.54" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="2"/>
<wire x1="58.42" y1="17.78" x2="60.96" y2="17.78" width="0.1524" layer="91"/>
<wire x1="60.96" y1="17.78" x2="60.96" y2="2.54" width="0.1524" layer="91"/>
<junction x="60.96" y="2.54"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="73.66" y1="2.54" x2="76.2" y2="2.54" width="0.1524" layer="91"/>
<pinref part="CON2" gate="_SIGNAL" pin="S"/>
<wire x1="86.36" y1="2.54" x2="76.2" y2="2.54" width="0.1524" layer="91"/>
<junction x="76.2" y="2.54"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="CON1" gate="_SIGNAL" pin="S"/>
<pinref part="L5" gate="G$1" pin="4"/>
<wire x1="2.54" y1="20.32" x2="7.62" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="45.72" x2="-27.94" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="35.56" x2="114.3" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="114.3" y1="35.56" x2="116.84" y2="35.56" width="0.1524" layer="91"/>
<wire x1="116.84" y1="35.56" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$2" pin="RFC"/>
<wire x1="127" y1="71.12" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<junction x="116.84" y="71.12"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="68.58" y1="111.76" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="68.58" y1="114.3" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<wire x1="73.66" y1="116.84" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<wire x1="68.58" y1="116.84" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<wire x1="73.66" y1="121.92" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="68.58" y1="121.92" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<junction x="68.58" y="116.84"/>
<pinref part="D9" gate="G$1" pin="C"/>
<pinref part="D10" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="111.76" y1="27.94" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="162.56" y1="109.22" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<pinref part="CON6" gate="VREG" pin="S"/>
<wire x1="172.72" y1="111.76" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<wire x1="172.72" y1="109.22" x2="162.56" y2="109.22" width="0.1524" layer="91"/>
<junction x="162.56" y="109.22"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="IC2" gate="G$2" pin="RF1"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="152.4" y1="76.2" x2="152.4" y2="81.28" width="0.1524" layer="91"/>
<wire x1="152.4" y1="81.28" x2="165.1" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC2" gate="G$2" pin="RF2"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="152.4" y1="73.66" x2="165.1" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="IC2" gate="G$2" pin="RF3"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="152.4" y1="71.12" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="CON3" gate="_SIGNAL" pin="S"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="177.8" y1="81.28" x2="172.72" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="CON4" gate="_SIGNAL" pin="S"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="177.8" y1="73.66" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="CON5" gate="_SIGNAL" pin="S"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="177.8" y1="66.04" x2="172.72" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="177.8" y1="48.26" x2="121.92" y2="48.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="48.26" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$2" pin="A"/>
<wire x1="121.92" y1="76.2" x2="127" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CON6" gate="B0" pin="S"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="IC2" gate="G$2" pin="B"/>
<wire x1="127" y1="73.66" x2="124.46" y2="73.66" width="0.1524" layer="91"/>
<wire x1="124.46" y1="73.66" x2="124.46" y2="50.8" width="0.1524" layer="91"/>
<wire x1="124.46" y1="50.8" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<pinref part="CON6" gate="B1" pin="S"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="81.28" x2="-27.94" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="12.7" y1="101.6" x2="-7.62" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="101.6" x2="-27.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="96.52" x2="-7.62" y2="101.6" width="0.1524" layer="91"/>
<junction x="-7.62" y="101.6"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="-22.86" y1="96.52" x2="-22.86" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="81.28" x2="-22.86" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="12.7" y1="99.06" x2="-2.54" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="99.06" x2="-22.86" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="96.52" x2="-2.54" y2="99.06" width="0.1524" layer="91"/>
<junction x="-2.54" y="99.06"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="4"/>
<wire x1="43.18" y1="15.24" x2="45.72" y2="15.24" width="0.1524" layer="91"/>
<wire x1="45.72" y1="15.24" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="5"/>
<wire x1="45.72" y1="20.32" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="TR2" gate="G$1" pin="B"/>
<pinref part="CON6" gate="RXEN" pin="S"/>
<wire x1="172.72" y1="134.62" x2="139.7" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="TR3" gate="G$1" pin="B"/>
<pinref part="CON6" gate="TXEN" pin="S"/>
<wire x1="172.72" y1="116.84" x2="139.7" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="TR5" gate="G$1" pin="B"/>
<wire x1="93.98" y1="111.76" x2="127" y2="111.76" width="0.1524" layer="91"/>
<wire x1="127" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<wire x1="129.54" y1="111.76" x2="129.54" y2="121.92" width="0.1524" layer="91"/>
<pinref part="TR3" gate="G$1" pin="C"/>
<wire x1="129.54" y1="121.92" x2="134.62" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="TR4" gate="G$1" pin="B"/>
<wire x1="93.98" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<wire x1="129.54" y1="129.54" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<pinref part="TR2" gate="G$1" pin="C"/>
<wire x1="129.54" y1="139.7" x2="134.62" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="CON6" gate="VBAT" pin="S"/>
<wire x1="172.72" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<wire x1="101.6" y1="144.78" x2="101.6" y2="134.62" width="0.1524" layer="91"/>
<pinref part="TR5" gate="G$1" pin="E"/>
<wire x1="101.6" y1="134.62" x2="101.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="101.6" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<pinref part="TR4" gate="G$1" pin="E"/>
<wire x1="99.06" y1="134.62" x2="101.6" y2="134.62" width="0.1524" layer="91"/>
<junction x="101.6" y="134.62"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
