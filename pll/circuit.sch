<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Parts" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Accent_neu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="NXP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="NXP_2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AnalogDevicesPlls">
<packages>
<package name="UFQFPN32-H">
<description>&lt;b&gt;UFQFPN32&lt;/b&gt;&lt;p&gt;
5 x 5 mm, 32-pin ultra thin fine pitch quad flat no-lead package</description>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<smd name="1" x="-2.65" y="1.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="-2.65" y="1.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="-2.65" y="0.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-2.65" y="0.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-2.65" y="-0.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-2.65" y="-0.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="7" x="-2.65" y="-1.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="8" x="-2.65" y="-1.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="-1.75" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="10" x="-1.25" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="11" x="-0.75" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="12" x="-0.25" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="13" x="0.25" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="14" x="0.75" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="15" x="1.25" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="16" x="1.75" y="-2.65" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="17" x="2.65" y="-1.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="18" x="2.65" y="-1.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="19" x="2.65" y="-0.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="20" x="2.65" y="-0.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="21" x="2.65" y="0.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="22" x="2.65" y="0.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="23" x="2.65" y="1.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="24" x="2.65" y="1.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="25" x="1.75" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="26" x="1.25" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="27" x="0.75" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="28" x="0.25" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="29" x="-0.25" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="30" x="-0.75" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="31" x="-1.25" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<smd name="32" x="-1.75" y="2.65" dx="0.3" dy="1.2" layer="1"/>
<rectangle x1="-2.57" y1="1.135" x2="-2.17" y2="1.365" layer="51"/>
<rectangle x1="-2.57" y1="0.635" x2="-2.17" y2="0.865" layer="51"/>
<rectangle x1="-2.57" y1="0.135" x2="-2.17" y2="0.365" layer="51"/>
<rectangle x1="-2.57" y1="-0.365" x2="-2.17" y2="-0.135" layer="51"/>
<rectangle x1="-2.57" y1="-0.865" x2="-2.17" y2="-0.635" layer="51"/>
<rectangle x1="-2.57" y1="-1.365" x2="-2.17" y2="-1.135" layer="51"/>
<rectangle x1="-2.57" y1="-1.865" x2="-2.17" y2="-1.635" layer="51"/>
<rectangle x1="-1.95" y1="-2.485" x2="-1.55" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="-1.45" y1="-2.485" x2="-1.05" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="-0.95" y1="-2.485" x2="-0.55" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="-0.45" y1="-2.485" x2="-0.05" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="0.05" y1="-2.485" x2="0.45" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="0.55" y1="-2.485" x2="0.95" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="1.05" y1="-2.485" x2="1.45" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="1.55" y1="-2.485" x2="1.95" y2="-2.255" layer="51" rot="R90"/>
<rectangle x1="2.17" y1="-1.865" x2="2.57" y2="-1.635" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="-1.365" x2="2.57" y2="-1.135" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="-0.865" x2="2.57" y2="-0.635" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="-0.365" x2="2.57" y2="-0.135" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="0.135" x2="2.57" y2="0.365" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="0.635" x2="2.57" y2="0.865" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="1.135" x2="2.57" y2="1.365" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="1.635" x2="2.57" y2="1.865" layer="51" rot="R180"/>
<rectangle x1="1.55" y1="2.255" x2="1.95" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="1.05" y1="2.255" x2="1.45" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="0.55" y1="2.255" x2="0.95" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="0.05" y1="2.255" x2="0.45" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="-0.45" y1="2.255" x2="-0.05" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="-0.95" y1="2.255" x2="-0.55" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="-1.45" y1="2.255" x2="-1.05" y2="2.485" layer="51" rot="R270"/>
<rectangle x1="-1.95" y1="2.255" x2="-1.55" y2="2.485" layer="51" rot="R270"/>
<circle x="-1.87" y="1.83" radius="0.420384375" width="0.127" layer="21"/>
<text x="-2.905" y="2.995" size="1.27" layer="25">&gt;Name</text>
<text x="-2.665" y="-4.205" size="1.27" layer="27">&gt;Value</text>
<pad name="EP" x="0" y="0" drill="2.5" diameter="3.45" shape="square"/>
<text x="-3.81" y="2.54" size="1.27" layer="21">1</text>
</package>
</packages>
<symbols>
<symbol name="ADF4351">
<pin name="CLK" x="-22.86" y="12.7" length="middle"/>
<pin name="DATA" x="-22.86" y="10.16" length="middle"/>
<pin name="LE" x="-22.86" y="7.62" length="middle"/>
<pin name="CE" x="-22.86" y="5.08" length="middle"/>
<pin name="SW" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="VP" x="0" y="22.86" length="middle" rot="R270"/>
<pin name="CPOUT" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="CPGND" x="-5.08" y="-22.86" length="middle" rot="R90"/>
<pin name="AGND" x="-2.54" y="-22.86" length="middle" rot="R90"/>
<pin name="AVDD" x="2.54" y="22.86" length="middle" rot="R270"/>
<pin name="VCOGND" x="2.54" y="-22.86" length="middle" rot="R90"/>
<pin name="RFOUTB-" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="RFOUTB+" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="RFOUTA-" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="RFOUTA+" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="VVCO" x="5.08" y="22.86" length="middle" rot="R270"/>
<pin name="RSET" x="-22.86" y="-7.62" length="middle"/>
<pin name="VTUNE" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="VCOM" x="-22.86" y="-2.54" length="middle"/>
<pin name="VREF" x="-22.86" y="-5.08" length="middle"/>
<pin name="LD" x="-22.86" y="2.54" length="middle"/>
<pin name="PDB" x="-22.86" y="0" length="middle"/>
<pin name="DGND" x="-10.16" y="-22.86" length="middle" rot="R90"/>
<pin name="DVDD" x="-5.08" y="22.86" length="middle" rot="R270"/>
<pin name="REFIN" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="MUXOUT" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="SDGND" x="-7.62" y="-22.86" length="middle" rot="R90"/>
<pin name="SDVDD" x="-2.54" y="22.86" length="middle" rot="R270"/>
<pin name="EPAD" x="0" y="-22.86" length="middle" rot="R90"/>
<wire x1="-17.78" y1="-17.78" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="17.78" x2="17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="-17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="-17.78" x2="-17.78" y2="-17.78" width="0.254" layer="94"/>
<text x="10.16" y="-20.32" size="1.778" layer="95">&gt;NAME</text>
<text x="10.16" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="TEMP" x="5.08" y="-22.86" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADF4351" prefix="IC">
<gates>
<gate name="G$1" symbol="ADF4351" x="0" y="0"/>
</gates>
<devices>
<device name="H" package="UFQFPN32-H">
<connects>
<connect gate="G$1" pin="AGND" pad="9"/>
<connect gate="G$1" pin="AVDD" pad="10"/>
<connect gate="G$1" pin="CE" pad="4"/>
<connect gate="G$1" pin="CLK" pad="1"/>
<connect gate="G$1" pin="CPGND" pad="8"/>
<connect gate="G$1" pin="CPOUT" pad="7"/>
<connect gate="G$1" pin="DATA" pad="2"/>
<connect gate="G$1" pin="DGND" pad="27"/>
<connect gate="G$1" pin="DVDD" pad="28"/>
<connect gate="G$1" pin="EPAD" pad="EP"/>
<connect gate="G$1" pin="LD" pad="25"/>
<connect gate="G$1" pin="LE" pad="3"/>
<connect gate="G$1" pin="MUXOUT" pad="30"/>
<connect gate="G$1" pin="PDB" pad="26"/>
<connect gate="G$1" pin="REFIN" pad="29"/>
<connect gate="G$1" pin="RFOUTA+" pad="12"/>
<connect gate="G$1" pin="RFOUTA-" pad="13"/>
<connect gate="G$1" pin="RFOUTB+" pad="14"/>
<connect gate="G$1" pin="RFOUTB-" pad="15"/>
<connect gate="G$1" pin="RSET" pad="22"/>
<connect gate="G$1" pin="SDGND" pad="31"/>
<connect gate="G$1" pin="SDVDD" pad="32"/>
<connect gate="G$1" pin="SW" pad="5"/>
<connect gate="G$1" pin="TEMP" pad="19"/>
<connect gate="G$1" pin="VCOGND" pad="11 18 21"/>
<connect gate="G$1" pin="VCOM" pad="23"/>
<connect gate="G$1" pin="VP" pad="6"/>
<connect gate="G$1" pin="VREF" pad="24"/>
<connect gate="G$1" pin="VTUNE" pad="20"/>
<connect gate="G$1" pin="VVCO" pad="16 17"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IQD-Frequency-Products" urn="urn:adsk.eagle:library:90">
<description>&lt;b&gt;Crystals and Oscillators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by IQD Frequency Products&lt;/author&gt;</description>
<packages>
<package name="2.6X2.1-4-PAD" urn="urn:adsk.eagle:footprint:3327/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.8" y="-0.6" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="-0.6" dx="0.9" dy="0.8" layer="1"/>
<smd name="3" x="0.8" y="0.6" dx="0.9" dy="0.8" layer="1" rot="R180"/>
<smd name="4" x="-0.8" y="0.6" dx="0.9" dy="0.8" layer="1" rot="R180"/>
<text x="-3.24" y="2.04" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.54" y="-3.41" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.6" y1="1.4" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.4" x2="-1.6" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="21"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<circle x="-1.8" y="-1.7" radius="0.2" width="0" layer="21"/>
</package>
<package name="7.2X5.2-4-PAD" urn="urn:adsk.eagle:footprint:3328/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-2.54" y="-2.1" dx="1.8" dy="1.6" layer="1"/>
<smd name="2" x="2.54" y="-2.1" dx="1.8" dy="1.6" layer="1"/>
<smd name="3" x="2.54" y="2.1" dx="1.8" dy="1.6" layer="1" rot="R180"/>
<smd name="4" x="-2.54" y="2.1" dx="1.8" dy="1.6" layer="1" rot="R180"/>
<text x="-3.8192" y="4.215" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.1192" y="-5.493" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.5" y1="3.5" x2="4.63" y2="3.5" width="0.127" layer="21"/>
<wire x1="4.63" y1="3.5" x2="4.73" y2="-3.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="-3.5" x2="-4.74" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-4.74" y1="-3.5" x2="-4.74" y2="3.46" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3.5" x2="-5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3.5" x2="-4.74" y2="-3.5" width="0.127" layer="21"/>
<circle x="-5.1" y="-3.8" radius="0.2" width="0" layer="21"/>
</package>
<package name="3.4X2.7-4-PAD" urn="urn:adsk.eagle:footprint:3329/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-3.1" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.9" x2="-3.1" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.91" width="0.2" layer="21"/>
<wire x1="-2.42" y1="1.9" x2="-2.42" y2="-1.91" width="0.2" layer="21"/>
<wire x1="-3.1" y1="1.9" x2="-3.1" y2="-1.9" width="0.2032" layer="21"/>
<smd name="1" x="-1.1" y="-0.8" dx="1.4" dy="1.15" layer="1"/>
<smd name="2" x="1.1" y="-0.8" dx="1.4" dy="1.15" layer="1"/>
<smd name="3" x="1.1" y="0.8" dx="1.4" dy="1.15" layer="1" rot="R180"/>
<smd name="4" x="-1.1" y="0.8" dx="1.4" dy="1.15" layer="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-2.7" y="-2.3" radius="0.2" width="0" layer="21"/>
</package>
<package name="5.0X3.2-4-PAD" urn="urn:adsk.eagle:footprint:3330/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-3.3" y1="-2.5" x2="2.368" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2.476" x2="-2.5" y2="-2.476" width="0.2032" layer="21"/>
<wire x1="2.368" y1="2.5" x2="-3.3" y2="2.5" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-2.476" x2="2.4" y2="2.476" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="2.5" x2="-3.3" y2="-2.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.25" y="-1.25" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="1.25" y="-1.25" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="1.25" y="1.25" dx="1.4" dy="1.4" layer="1" rot="R270"/>
<smd name="4" x="-1.25" y="1.25" dx="1.4" dy="1.4" layer="1" rot="R270"/>
<text x="3.04" y="4.39" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="-3.686" y="-4.608" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-2.9" y="-2.9" radius="0.2" width="0" layer="21"/>
</package>
<package name="5.2X3.4-4-PAD" urn="urn:adsk.eagle:footprint:3331/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-4.2" y1="-2.3" x2="3.368" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="2.276" x2="-3.3" y2="-2.176" width="0.2032" layer="21"/>
<wire x1="3.368" y1="2.3" x2="-4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-2.276" x2="3.4" y2="2.276" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="2.3" x2="-4.2" y2="-2.3" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="-1.1" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.1" y="-1.1" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="3" x="2.1" y="1.1" dx="1.4" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.1" y="1.1" dx="1.4" dy="1.6" layer="1" rot="R270"/>
<text x="3.04" y="4.39" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="-3.686" y="-4.608" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-3.7" y="-2.8" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.0X1.6-4-PAD" urn="urn:adsk.eagle:footprint:3332/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.637" y="-0.487" dx="0.875" dy="0.775" layer="1"/>
<smd name="2" x="0.637" y="-0.487" dx="0.875" dy="0.775" layer="1"/>
<smd name="3" x="0.637" y="0.487" dx="0.875" dy="0.775" layer="1" rot="R180"/>
<smd name="4" x="-0.637" y="0.487" dx="0.875" dy="0.775" layer="1" rot="R180"/>
<text x="-2.27" y="1.77" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.67" y="-3.34" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.4" y1="1.3" x2="1.5" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.3" x2="1.5" y2="-1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.3" x2="-1.4" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1.3" x2="-1.4" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.3" x2="-1.9" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.3" x2="-1.9" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.3" x2="-1.4" y2="-1.3" width="0.127" layer="21"/>
<circle x="-1.6" y="-1.6" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.5X2-4-PAD" urn="urn:adsk.eagle:footprint:3333/1" library_version="1">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.85" y="-0.65" dx="1.2" dy="1" layer="1"/>
<smd name="2" x="0.85" y="-0.65" dx="1.2" dy="1" layer="1"/>
<smd name="3" x="0.85" y="0.65" dx="1.2" dy="1" layer="1" rot="R180"/>
<smd name="4" x="-0.85" y="0.65" dx="1.2" dy="1" layer="1" rot="R180"/>
<text x="-3.27" y="2.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.47" y="-3.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.1" y1="-1.5" x2="2" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="1.5" width="0.127" layer="21"/>
<wire x1="2" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.5" x2="-2.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1.5" x2="-2.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.5" x2="-2.1" y2="-1.5" width="0.127" layer="21"/>
<circle x="-2.3" y="-1.8" radius="0.2" width="0" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="2.6X2.1-4-PAD" urn="urn:adsk.eagle:package:3366/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="2.6X2.1-4-PAD"/>
</packageinstances>
</package3d>
<package3d name="7.2X5.2-4-PAD" urn="urn:adsk.eagle:package:3365/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="7.2X5.2-4-PAD"/>
</packageinstances>
</package3d>
<package3d name="3.4X2.7-4-PAD" urn="urn:adsk.eagle:package:3367/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="3.4X2.7-4-PAD"/>
</packageinstances>
</package3d>
<package3d name="5.0X3.2-4-PAD" urn="urn:adsk.eagle:package:3368/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="5.0X3.2-4-PAD"/>
</packageinstances>
</package3d>
<package3d name="5.2X3.4-4-PAD" urn="urn:adsk.eagle:package:3369/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="5.2X3.4-4-PAD"/>
</packageinstances>
</package3d>
<package3d name="2.0X1.6-4-PAD" urn="urn:adsk.eagle:package:3370/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="2.0X1.6-4-PAD"/>
</packageinstances>
</package3d>
<package3d name="2.5X2-4-PAD" urn="urn:adsk.eagle:package:3371/1" type="box" library_version="1">
<description>IQD Frequency Products SMD Package</description>
<packageinstances>
<packageinstance name="2.5X2-4-PAD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="QG2" urn="urn:adsk.eagle:symbol:3326/1" library_version="1">
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="0" width="0.4064" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="2.54" y2="2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="6.35" y2="0" width="0.4064" layer="94"/>
<wire x1="6.35" y1="0" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="-3.175" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="3.175" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-1.27" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.175" x2="-1.27" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.826" y1="-0.381" x2="-4.826" y2="0.381" width="0.254" layer="94"/>
<wire x1="-4.826" y1="0.381" x2="-2.794" y2="0.381" width="0.254" layer="94"/>
<wire x1="-2.794" y1="0.381" x2="-2.794" y2="-0.381" width="0.254" layer="94"/>
<wire x1="-4.826" y1="-0.381" x2="-2.794" y2="-0.381" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.016" x2="-3.81" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.016" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.016" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<text x="-7.62" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-6.985" y="4.318" size="1.524" layer="95">VDD</text>
<text x="-6.985" y="-5.842" size="1.524" layer="95">VSS</text>
<text x="2.54" y="-5.842" size="1.524" layer="95">OUT</text>
<text x="2.54" y="5.588" size="1.524" layer="95">CON</text>
<pin name="VSS" x="-12.7" y="-5.08" visible="pad" length="middle" direction="pwr"/>
<pin name="VDD" x="-12.7" y="5.08" visible="pad" length="middle" direction="pwr"/>
<pin name="OUT" x="12.7" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="CON" x="12.7" y="5.08" visible="pad" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSC-OE" urn="urn:adsk.eagle:component:3380/1" prefix="OS" library_version="1">
<description>&lt;b&gt;OSCILLATOR WITH OE&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="QG2" x="0" y="0"/>
</gates>
<devices>
<device name="-CFPS-107" package="2.6X2.1-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3366/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-108" package="2.6X2.1-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3366/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-109" package="2.6X2.1-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3366/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-32" package="7.2X5.2-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3365/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-39" package="3.4X2.7-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3367/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-69" package="5.0X3.2-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3368/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-72" package="7.2X5.2-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3365/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-73" package="7.2X5.2-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3365/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPS-9" package="5.2X3.4-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3369/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXO-540" package="2.0X1.6-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3370/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXO-541" package="2.0X1.6-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3370/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXO-542" package="2.0X1.6-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3370/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXO-793" package="2.5X2-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3371/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXO-794" package="2.5X2-4-PAD">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3371/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Robotech">
<description>&lt;Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT89">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;</description>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.727" dx="0.8" dy="1.9" layer="1" stop="no" cream="no"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
<smd name="2.1" x="0" y="0.889" dx="2" dy="3.5" layer="1" roundness="75" cream="no"/>
<rectangle x1="-0.508" y1="-2.794" x2="0.508" y2="-1.27" layer="29"/>
<rectangle x1="-0.381" y1="-2.667" x2="0.381" y2="-1.3335" layer="31"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.1524" layer="21"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.1524" layer="21"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.1524" layer="21"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.1524" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.2032" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.2032" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.2032" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.2032" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.2032" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="-0.762" width="0.254" layer="43"/>
<wire x1="-0.254" y1="-0.762" x2="-0.381" y2="-0.762" width="0.254" layer="43"/>
<wire x1="-0.381" y1="-0.762" x2="-0.889" y2="-0.254" width="0.254" layer="43"/>
<wire x1="-0.889" y1="-0.254" x2="-0.889" y2="2.159" width="0.254" layer="43"/>
<wire x1="-0.889" y1="2.159" x2="-0.381" y2="2.667" width="0.254" layer="43"/>
<wire x1="-0.381" y1="2.667" x2="0.381" y2="2.667" width="0.254" layer="43"/>
<wire x1="0.381" y1="2.667" x2="0.889" y2="2.159" width="0.254" layer="43"/>
<wire x1="0.889" y1="2.159" x2="0.889" y2="-0.254" width="0.254" layer="43"/>
<wire x1="0.889" y1="-0.254" x2="0.381" y2="-0.762" width="0.254" layer="43"/>
<wire x1="0.381" y1="-0.762" x2="0.254" y2="-0.762" width="0.254" layer="43"/>
<wire x1="0.254" y1="-0.762" x2="0.254" y2="-1.143" width="0.254" layer="43"/>
<wire x1="0" y1="-1.143" x2="0" y2="-0.762" width="0.254" layer="43"/>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.762" layer="43"/>
<wire x1="0" y1="0" x2="0" y2="1.778" width="1.6764" layer="43"/>
<rectangle x1="-0.508" y1="-2.794" x2="0.508" y2="-1.27" layer="29"/>
<rectangle x1="-0.381" y1="-2.667" x2="0.381" y2="-1.3335" layer="31"/>
<wire x1="0" y1="0.1905" x2="0" y2="1.651" width="1.8" layer="31"/>
</package>
<package name="OKL-T">
<smd name="9" x="0.5" y="1.52" dx="3" dy="1.4" layer="1"/>
<smd name="10" x="0.5" y="3.81" dx="3" dy="1.4" layer="1"/>
<smd name="1" x="0.5" y="6.1" dx="3" dy="1.4" layer="1"/>
<smd name="8" x="3.81" y="0.5" dx="1.4" dy="3" layer="1"/>
<smd name="7" x="6.1" y="0.5" dx="1.4" dy="3" layer="1"/>
<smd name="12" x="8.39" y="0.5" dx="1.4" dy="3" layer="1"/>
<smd name="6" x="11.7" y="1.52" dx="3" dy="1.4" layer="1"/>
<smd name="5" x="11.7" y="6.1" dx="3" dy="1.4" layer="1"/>
<smd name="4" x="11.7" y="10.68" dx="3" dy="1.4" layer="1"/>
<smd name="2" x="3.81" y="11.7" dx="1.4" dy="3" layer="1"/>
<smd name="3" x="8.39" y="11.7" dx="1.4" dy="3" layer="1"/>
<polygon width="0.127" layer="1">
<vertex x="11.57" y="7.5"/>
<vertex x="11.57" y="11.57"/>
<vertex x="9.8" y="11.57"/>
<vertex x="9.8" y="7.5"/>
</polygon>
<rectangle x1="9.8" y1="7.5" x2="11.57" y2="11.57" layer="31"/>
<rectangle x1="9.7" y1="7.4" x2="11.67" y2="11.67" layer="29"/>
<polygon width="0.1016" layer="1">
<vertex x="5.21" y="9.8"/>
<vertex x="9.28" y="9.8"/>
<vertex x="9.28" y="11.57"/>
<vertex x="6.5" y="11.5"/>
<vertex x="6.5" y="13.2"/>
<vertex x="5.21" y="13.2"/>
</polygon>
<rectangle x1="5.21" y1="9.8" x2="9.28" y2="11.57" layer="31"/>
<rectangle x1="5.11" y1="9.7" x2="9.38" y2="11.67" layer="29"/>
<polygon width="0.127" layer="1">
<vertex x="-1" y="9.8"/>
<vertex x="4.7" y="9.8"/>
<vertex x="4.7" y="11.57"/>
<vertex x="-1" y="11.57"/>
</polygon>
<rectangle x1="-1" y1="9.8" x2="4.7" y2="11.57" layer="31"/>
<rectangle x1="-1.1" y1="9.7" x2="4.8" y2="11.67" layer="29"/>
<smd name="11" x="12.45" y="3.81" dx="1.5" dy="1.4" layer="1"/>
<wire x1="1" y1="12.2" x2="12.2" y2="12.2" width="0.127" layer="21"/>
<wire x1="12.2" y1="12.2" x2="12.2" y2="0" width="0.127" layer="21"/>
<wire x1="12.2" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="11.2" width="0.127" layer="21"/>
<wire x1="0" y1="11.2" x2="1" y2="12.2" width="0.127" layer="21"/>
<circle x="6.1" y="6.1" radius="5.6" width="0.127" layer="21"/>
<rectangle x1="5.21" y1="11.57" x2="6.5" y2="13.2" layer="31"/>
<rectangle x1="5.11" y1="11.67" x2="6.6" y2="13.3" layer="29"/>
<polygon width="0.127" layer="1">
<vertex x="10.2" y="4.51"/>
<vertex x="10.2" y="3.11"/>
<vertex x="11.5" y="3.11"/>
<vertex x="11.5" y="4.51"/>
</polygon>
<rectangle x1="10.2" y1="3.11" x2="11.5" y2="4.51" layer="31"/>
<rectangle x1="10.1" y1="3.01" x2="11.6" y2="4.61" layer="29"/>
<text x="3" y="5.5" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SOT23-5L" urn="urn:adsk.eagle:footprint:26811/1">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; SOT23-5L&lt;p&gt;
Source: www.st.com LD3985.pdf</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="-1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="3" x="0.95" y="-1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="4" x="0.95" y="1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="5" x="-0.95" y="1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.4" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.4" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.4" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.4" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.4" layer="51"/>
<rectangle x1="-1.2875" y1="-1.8125" x2="-0.6125" y2="-0.5" layer="29"/>
<rectangle x1="-1.2125" y1="-1.7125" x2="-0.6875" y2="-0.5875" layer="31"/>
<rectangle x1="-0.3375" y1="-1.8125" x2="0.3375" y2="-0.5" layer="29"/>
<rectangle x1="-0.2625" y1="-1.7125" x2="0.2625" y2="-0.5875" layer="31"/>
<rectangle x1="0.6125" y1="-1.8125" x2="1.2875" y2="-0.5" layer="29"/>
<rectangle x1="0.6875" y1="-1.7125" x2="1.2125" y2="-0.5875" layer="31"/>
<rectangle x1="0.6125" y1="0.5" x2="1.2875" y2="1.8125" layer="29" rot="R180"/>
<rectangle x1="0.6875" y1="0.5875" x2="1.2125" y2="1.7125" layer="31" rot="R180"/>
<rectangle x1="-1.2875" y1="0.5" x2="-0.6125" y2="1.8125" layer="29" rot="R180"/>
<rectangle x1="-1.2125" y1="0.5875" x2="-0.6875" y2="1.7125" layer="31" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="LINEAR-REGULATOR">
<pin name="IN" x="-12.7" y="2.54" length="middle" direction="pwr"/>
<pin name="OUT" x="12.7" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="GND" x="0" y="-7.62" length="middle" direction="pwr" rot="R90"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="OKL-T/*">
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<pin name="VIN" x="-12.7" y="10.16" length="short"/>
<pin name="VOUT" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="GND2" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="ON/OFF" x="-12.7" y="5.08" length="short"/>
<pin name="TRIM" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="SENSE" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="SEQ" x="-12.7" y="0" length="short"/>
<pin name="NC1" x="-12.7" y="-2.54" length="short"/>
<pin name="NC2" x="-12.7" y="-5.08" length="short"/>
<pin name="NC3" x="-12.7" y="-7.62" length="short"/>
<pin name="GND1" x="12.7" y="-5.08" length="short" rot="R180"/>
<text x="-10.16" y="-12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-15.24" size="1.27" layer="96">&gt;VALUE</text>
<pin name="PGOOD" x="12.7" y="0" length="short" rot="R180"/>
</symbol>
<symbol name="ADP151">
<pin name="VIN" x="-12.7" y="2.54" length="middle" direction="pwr"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle" direction="in"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NJW4181" prefix="IC">
<gates>
<gate name="G$1" symbol="LINEAR-REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="U3" package="SOT89">
<connects>
<connect gate="G$1" pin="GND" pad="2 2.1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OKL-T/*" prefix="U">
<description>&lt;b&gt;Murata DC-DC Converter&lt;/b&gt;
&lt;p&gt;
Input Voltage: 12-5V&lt;br&gt; 
Output Voltage: 0.6-5.5V&lt;br&gt;
&lt;br&gt;
you can get this at Akizuki.&lt;br&gt;

&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="OKL-T/*" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="OKL-T">
<connects>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="7"/>
<connect gate="G$1" pin="NC1" pad="8"/>
<connect gate="G$1" pin="NC2" pad="11"/>
<connect gate="G$1" pin="NC3" pad="12"/>
<connect gate="G$1" pin="ON/OFF" pad="1"/>
<connect gate="G$1" pin="PGOOD" pad="10"/>
<connect gate="G$1" pin="SENSE" pad="5"/>
<connect gate="G$1" pin="SEQ" pad="9"/>
<connect gate="G$1" pin="TRIM" pad="6"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="4"/>
</connects>
<technologies>
<technology name="3-W5">
<attribute name="INPUT" value="2.4-5.5V" constant="no"/>
<attribute name="OUTPUT" value="?O?D?U?u-?R?D?U?R?u?^?R?`" constant="no"/>
</technology>
<technology name="6-W12">
<attribute name="INPUT" value="4.5-14V" constant="no"/>
<attribute name="OUTPUT" value="?O?D?T?XV-?T?D?T?u?^?U?`" constant="no"/>
</technology>
<technology name="6-W5">
<attribute name="INPUT" value="2.4-5.5V" constant="no"/>
<attribute name="OUTPUT" value="0.6V-3.63?u?^?U?`" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADP151" prefix="IC">
<gates>
<gate name="G$1" symbol="ADP151" x="0" y="0"/>
</gates>
<devices>
<device name="AUJZ" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Connector Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
Hideo Tanida&lt;br&gt;
Makoto Shimazu&lt;br&gt;
Mayu Kojima&lt;br&gt;
Takefumi Hiraki&lt;br&gt;
Soichiro Iwataki&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="AQM1248A-RN">
<pad name="9" x="0" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="10" x="-1.27" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="8" x="1.27" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="11" x="-2.54" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="7" x="2.54" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="12" x="-3.81" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="13" x="-5.08" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="14" x="-6.35" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="4" x="6.35" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="15" x="-7.62" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="3" x="7.62" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="2" x="8.89" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="17" x="-10.16" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="1" x="10.16" y="0" drill="0.6" shape="long" rot="R90"/>
<wire x1="-17" y1="0" x2="-17" y2="6.5" width="0.127" layer="21"/>
<wire x1="-17" y1="6.5" x2="-17" y2="20" width="0.127" layer="21"/>
<wire x1="-17" y1="20" x2="17" y2="20" width="0.127" layer="21"/>
<wire x1="17" y1="20" x2="17" y2="6.5" width="0.127" layer="21"/>
<wire x1="17" y1="6.5" x2="17" y2="0" width="0.127" layer="21"/>
<wire x1="17" y1="0" x2="-17" y2="0" width="0.127" layer="21"/>
<wire x1="-17" y1="6.5" x2="17" y2="6.5" width="0.127" layer="21"/>
<wire x1="-16" y1="8" x2="-16" y2="19" width="0.127" layer="21"/>
<wire x1="-16" y1="19" x2="16" y2="19" width="0.127" layer="21"/>
<wire x1="16" y1="19" x2="16" y2="8" width="0.127" layer="21"/>
<wire x1="16" y1="8" x2="-16" y2="8" width="0.127" layer="21"/>
<text x="-16.51" y="20.32" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="20.32" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="5251-02-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="2.1321" y="-3.6561" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<text x="-2.8941" y="-2.8941" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-1.27" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<wire x1="1.27" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="-1.27" y1="9.906" x2="-1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-1.016" y1="10.414" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="8.382" layer="51"/>
<rectangle x1="1.016" y1="0.254" x2="1.524" y2="8.382" layer="51"/>
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="48"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
<text x="2.286" y="-2.794" size="1.27" layer="51" rot="R90">1</text>
<text x="-1.016" y="-2.794" size="1.27" layer="51" rot="R90">2</text>
</package>
<package name="5251-02-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-2.5131" y="3.2751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="2.032" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-0.762" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">2</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<text x="2.5131" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="-2.54" y1="2.921" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="-1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.27" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.905" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.397" x2="1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.397" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
</package>
<package name="D3200S-2-TAB-HORIZONTAL">
<pad name="1" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.1" shape="long" rot="R270"/>
<text x="-5.715" y="14.605" size="3.81" layer="21" rot="SR0">AMP</text>
<text x="-2.8575" y="12.7" size="1.27" layer="21" rot="SR0">D-3200</text>
<text x="-8.89" y="24.13" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="-0.6" x2="9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="9.55" y1="-0.6" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="9.55" y1="0" x2="9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="9.55" y1="21.5" x2="-9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="-9.55" y1="21.5" x2="-9.55" y2="0" width="0.127" layer="27"/>
<text x="-8.89" y="22.225" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="0" x2="-9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="-9.55" y1="0" x2="9.55" y2="0" width="0.127" layer="27"/>
<hole x="-6.35" y="7.62" drill="3.2"/>
<hole x="6.35" y="7.62" drill="3.2"/>
<wire x1="-8.255" y1="6.858" x2="-7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="-7.112" y1="5.715" x2="-5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="-5.588" y1="5.715" x2="-4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-4.445" y1="6.858" x2="-4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="-4.445" y1="8.382" x2="-5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="-5.588" y1="9.525" x2="-7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="-7.112" y1="9.525" x2="-8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="-8.255" y1="8.382" x2="-8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="4.445" y1="6.858" x2="4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="4.445" y1="8.382" x2="5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="5.588" y1="9.525" x2="7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="7.112" y1="9.525" x2="8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="8.255" y1="8.382" x2="8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="8.255" y1="6.858" x2="7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="7.112" y1="5.715" x2="5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="5.588" y1="5.715" x2="4.445" y2="6.858" width="0.508" layer="42"/>
</package>
<package name="D3200S-2-TAB-VERTICAL">
<pad name="2" x="-2.54" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="2.54" y="0" drill="1" shape="long" rot="R270"/>
<text x="-9.525" y="6.985" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="4.7" x2="9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="4.7" x2="9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="-4.7" x2="-9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="-9.55" y1="-4.7" x2="-9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.68" x2="8.5" y2="3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="3.68" x2="8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="-3.68" x2="-8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.68" x2="-8.5" y2="3.68" width="0.127" layer="21"/>
<text x="-9.525" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.525" y1="0.555" x2="-8.525" y2="0.555" width="0.127" layer="21"/>
<wire x1="-8.525" y1="-1.445" x2="-9.525" y2="-1.445" width="0.127" layer="21"/>
<hole x="-5.715" y="-1.27" drill="2"/>
<wire x1="-6.985" y1="-1.778" x2="-6.985" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-6.985" y1="-0.762" x2="-6.223" y2="0" width="0.508" layer="42"/>
<wire x1="-6.223" y1="0" x2="-5.207" y2="0" width="0.508" layer="42"/>
<wire x1="-5.207" y1="0" x2="-4.445" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-4.445" y1="-0.762" x2="-4.445" y2="-1.778" width="0.508" layer="42"/>
<wire x1="-4.445" y1="-1.778" x2="-5.207" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-5.207" y1="-2.54" x2="-6.223" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-6.223" y1="-2.54" x2="-6.985" y2="-1.778" width="0.508" layer="42"/>
<hole x="5.715" y="-1.27" drill="2"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="-0.762" width="0.508" layer="42"/>
<wire x1="4.445" y1="-0.762" x2="5.207" y2="0" width="0.508" layer="42"/>
<wire x1="5.207" y1="0" x2="6.223" y2="0" width="0.508" layer="42"/>
<wire x1="6.223" y1="0" x2="6.985" y2="-0.762" width="0.508" layer="42"/>
<wire x1="6.985" y1="-0.762" x2="6.985" y2="-1.778" width="0.508" layer="42"/>
<wire x1="6.985" y1="-1.778" x2="6.223" y2="-2.54" width="0.508" layer="42"/>
<wire x1="6.223" y1="-2.54" x2="5.207" y2="-2.54" width="0.508" layer="42"/>
<wire x1="5.207" y1="-2.54" x2="4.445" y2="-1.778" width="0.508" layer="42"/>
</package>
<package name="D3200S-2-TAB-VERTICAL-AND-HORIZONTAL">
<pad name="1" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.1" shape="long" rot="R270"/>
<text x="-5.715" y="14.605" size="3.81" layer="21" rot="SR0">AMP</text>
<text x="-2.8575" y="12.7" size="1.27" layer="21" rot="SR0">D-3200</text>
<text x="-8.89" y="24.13" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="-0.6" x2="9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="9.55" y1="-0.6" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="9.55" y1="0" x2="9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="9.55" y1="21.5" x2="-9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="-9.55" y1="21.5" x2="-9.55" y2="0" width="0.127" layer="27"/>
<text x="-8.89" y="22.225" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="0" x2="-9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="-9.55" y1="0" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="-9.55" y1="-4.7" x2="9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="-4.7" x2="9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="4.7" x2="-9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="-9.55" y1="4.7" x2="-9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="8.5" y1="3.68" x2="8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="-3.68" x2="-8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.68" x2="-8.5" y2="3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.68" x2="8.5" y2="3.68" width="0.127" layer="21"/>
<hole x="-6.35" y="7.62" drill="3.2"/>
<hole x="6.35" y="7.62" drill="3.2"/>
<hole x="-6.35" y="-1.27" drill="2"/>
<hole x="6.35" y="-1.27" drill="2"/>
<wire x1="-8.255" y1="6.858" x2="-8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="-8.255" y1="8.382" x2="-7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="-7.112" y1="9.525" x2="-5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="-5.588" y1="9.525" x2="-4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="-4.445" y1="8.382" x2="-4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-4.445" y1="6.858" x2="-5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="-5.588" y1="5.715" x2="-7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="-7.112" y1="5.715" x2="-8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="4.445" y1="6.858" x2="4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="4.445" y1="8.382" x2="5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="5.588" y1="9.525" x2="7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="7.112" y1="9.525" x2="8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="8.255" y1="8.382" x2="8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="8.255" y1="6.858" x2="7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="7.112" y1="5.715" x2="5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="5.588" y1="5.715" x2="4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-7.62" y1="-1.778" x2="-7.62" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-7.62" y1="-0.762" x2="-6.858" y2="0" width="0.508" layer="42"/>
<wire x1="-6.858" y1="0" x2="-5.842" y2="0" width="0.508" layer="42"/>
<wire x1="-5.842" y1="0" x2="-5.08" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-5.08" y1="-0.762" x2="-5.08" y2="-1.778" width="0.508" layer="42"/>
<wire x1="-5.08" y1="-1.778" x2="-5.842" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-5.842" y1="-2.54" x2="-6.858" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-6.858" y1="-2.54" x2="-7.62" y2="-1.778" width="0.508" layer="42"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="-0.762" width="0.508" layer="42"/>
<wire x1="5.08" y1="-0.762" x2="5.842" y2="0" width="0.508" layer="42"/>
<wire x1="5.842" y1="0" x2="6.858" y2="0" width="0.508" layer="42"/>
<wire x1="6.858" y1="0" x2="7.62" y2="-0.762" width="0.508" layer="42"/>
<wire x1="7.62" y1="-0.762" x2="7.62" y2="-1.778" width="0.508" layer="42"/>
<wire x1="7.62" y1="-1.778" x2="6.858" y2="-2.54" width="0.508" layer="42"/>
<wire x1="6.858" y1="-2.54" x2="5.842" y2="-2.54" width="0.508" layer="42"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-1.778" width="0.508" layer="42"/>
</package>
<package name="B2P-VH">
<pad name="1" x="-1.98" y="0" drill="1.7" shape="long" rot="R90"/>
<pad name="2" x="1.98" y="0" drill="1.7" shape="long" rot="R90"/>
<wire x1="3.43" y1="-5.08" x2="-3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-5.08" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<text x="-2.54" y="-6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="XT30PW-F">
<pad name="P" x="2.5" y="0" drill="1.8"/>
<pad name="N" x="-2.5" y="0" drill="1.8"/>
<pad name="M$2" x="5.5" y="5" drill="1"/>
<pad name="M$1" x="-5.5" y="5" drill="1"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="7.62" width="0.127" layer="21"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="15.24" width="0.127" layer="21"/>
<wire x1="-5.08" y1="15.24" x2="-3.81" y2="15.24" width="0.127" layer="21"/>
<wire x1="-3.81" y1="15.24" x2="-1.27" y2="15.24" width="0.127" layer="21"/>
<wire x1="-1.27" y1="15.24" x2="0" y2="15.24" width="0.127" layer="21"/>
<wire x1="0" y1="15.24" x2="1.27" y2="15.24" width="0.127" layer="21"/>
<wire x1="1.27" y1="15.24" x2="3.81" y2="15.24" width="0.127" layer="21"/>
<wire x1="3.81" y1="15.24" x2="5.08" y2="15.24" width="0.127" layer="21"/>
<wire x1="5.08" y1="15.24" x2="5.08" y2="7.62" width="0.127" layer="21"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="0" width="0.127" layer="21"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="7.62" x2="-3.81" y2="7.62" width="0.127" layer="21"/>
<wire x1="-3.81" y1="7.62" x2="-1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="1.27" y1="7.62" x2="3.81" y2="7.62" width="0.127" layer="21"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.127" layer="21"/>
<wire x1="-3.81" y1="15.24" x2="-3.81" y2="7.62" width="0.127" layer="21"/>
<wire x1="-1.27" y1="15.24" x2="-1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="15.24" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="1.27" y1="15.24" x2="1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="3.81" y1="15.24" x2="3.81" y2="7.62" width="0.127" layer="21"/>
<text x="0" y="-2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="XT30PW-M">
<pad name="N" x="2.5" y="0" drill="1.8"/>
<pad name="P" x="-2.5" y="0" drill="1.8"/>
<pad name="M$2" x="5.5" y="10" drill="1"/>
<pad name="M$1" x="-5.5" y="10" drill="1"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.81" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="13.97" width="0.127" layer="21"/>
<wire x1="-5.08" y1="13.97" x2="5.08" y2="13.97" width="0.127" layer="21"/>
<wire x1="5.08" y1="13.97" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="0" width="0.127" layer="21"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.81" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<text x="0" y="-2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="IL-G-2P-S3T2-SA">
<pad name="1" x="-1.27" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-2.25" y1="3.07" x2="2.25" y2="3.07" width="0.127" layer="21"/>
<wire x1="2.25" y1="3.07" x2="2.25" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.25" y1="2.54" x2="2.25" y2="-2.93" width="0.127" layer="21"/>
<wire x1="2.25" y1="-2.93" x2="-2.25" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.93" x2="-2.25" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.54" x2="-2.25" y2="3.07" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.54" x2="2.25" y2="2.54" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="-2.54" size="1.27" layer="21">1</text>
</package>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="ML6">
<pad name="1" x="-2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="0.9144" shape="octagon"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-7.62" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="6.35" y2="4.699" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.699" x2="5.08" y2="4.699" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.445" x2="5.08" y2="4.699" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.699" x2="-6.35" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="4.699" x2="-6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.699" x2="-5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.445" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="6.604" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="6.604" y1="-3.429" x2="6.604" y2="3.429" width="0.0508" layer="21"/>
<wire x1="6.604" y1="3.429" x2="-6.604" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="3.429" x2="-6.604" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<text x="-7.62" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<wire x1="-2.032" y1="-4.445" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-3.81" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-3.81" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-7.62" y2="-4.445" width="0.1524" layer="21"/>
<text x="-0.381" y="-4.064" size="1.27" layer="21" ratio="10">6</text>
</package>
<package name="ML6L">
<pad name="1" x="-2.54" y="-6.35" drill="0.9144" shape="octagon"/>
<pad name="2" x="-2.54" y="-3.81" drill="0.9144" shape="octagon"/>
<pad name="3" x="0" y="-6.35" drill="0.9144" shape="octagon"/>
<pad name="4" x="0" y="-3.81" drill="0.9144" shape="octagon"/>
<pad name="5" x="2.54" y="-6.35" drill="0.9144" shape="octagon"/>
<pad name="6" x="2.54" y="-3.81" drill="0.9144" shape="octagon"/>
<text x="-4.6228" y="-6.7564" size="1.27" layer="21" ratio="10">1</text>
<text x="-4.6482" y="-4.7244" size="1.27" layer="21" ratio="10">2</text>
<text x="-7.6454" y="6.35" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-5.08" y1="5.207" x2="-2.54" y2="5.207" width="0.254" layer="21"/>
<wire x1="-2.54" y1="5.207" x2="-3.81" y2="2.667" width="0.254" layer="21"/>
<wire x1="-3.81" y1="2.667" x2="-5.08" y2="5.207" width="0.254" layer="21"/>
<text x="0.6096" y="6.35" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="5.842" x2="-2.159" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="5.842" x2="2.159" y2="5.842" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.842" x2="2.159" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.842" x2="7.62" y2="5.842" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="-2.159" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.651" x2="-2.159" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-3.048" x2="-1.905" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-3.048" x2="-0.635" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-3.048" x2="0.635" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-3.048" x2="1.905" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-3.048" x2="3.175" y2="-3.048" width="0.1524" layer="51"/>
<rectangle x1="-0.254" y1="-0.635" x2="0.254" y2="5.207" layer="21"/>
<wire x1="0" y1="5.08" x2="0" y2="5.334" width="0.508" layer="21"/>
<wire x1="3.302" y1="5.207" x2="3.302" y2="2.413" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.413" x2="7.112" y2="2.413" width="0.1524" layer="21"/>
<wire x1="7.112" y1="5.207" x2="7.112" y2="2.413" width="0.1524" layer="21"/>
<wire x1="7.112" y1="5.207" x2="3.302" y2="5.207" width="0.1524" layer="21"/>
<rectangle x1="-4.953" y1="4.699" x2="-2.667" y2="5.207" layer="21"/>
<rectangle x1="-4.699" y1="4.191" x2="-2.921" y2="4.699" layer="21"/>
<rectangle x1="-4.445" y1="3.683" x2="-3.175" y2="4.191" layer="21"/>
<rectangle x1="-4.191" y1="3.175" x2="-3.429" y2="3.683" layer="21"/>
<rectangle x1="-3.937" y1="2.921" x2="-3.683" y2="3.175" layer="21"/>
<text x="4.572" y="3.048" size="1.524" layer="21" ratio="10">6</text>
<wire x1="7.62" y1="5.842" x2="7.62" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-3.048" x2="6.477" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="5.842" x2="-7.62" y2="5.842" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="5.842" x2="-7.62" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-3.048" x2="-6.477" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-3.048" x2="5.461" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-3.048" x2="3.175" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-3.683" x2="6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-3.048" x2="6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-3.048" x2="5.461" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-3.048" x2="-5.461" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-3.048" x2="-3.175" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-3.683" x2="-6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-3.048" x2="-6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-3.048" x2="-5.461" y2="-3.048" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="-4.699" x2="-2.286" y2="-3.048" layer="51"/>
<rectangle x1="-2.794" y1="-5.461" x2="-2.286" y2="-4.699" layer="21"/>
<rectangle x1="-2.794" y1="-6.604" x2="-2.286" y2="-5.461" layer="51"/>
<rectangle x1="-0.254" y1="-4.699" x2="0.254" y2="-3.048" layer="51"/>
<rectangle x1="-0.254" y1="-5.461" x2="0.254" y2="-4.699" layer="21"/>
<rectangle x1="-0.254" y1="-6.604" x2="0.254" y2="-5.461" layer="51"/>
<rectangle x1="2.286" y1="-4.699" x2="2.794" y2="-3.048" layer="51"/>
<rectangle x1="2.286" y1="-5.461" x2="2.794" y2="-4.699" layer="21"/>
<rectangle x1="2.286" y1="-6.604" x2="2.794" y2="-5.461" layer="51"/>
</package>
<package name="IL-G-04-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-5.0531" y="3.7751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="4.5481" y="-1.9319" size="1.27" layer="21" ratio="14" rot="R90">1</text>
<text x="-2.8687" y="-1.9319" size="1.27" layer="21" ratio="14" rot="R90">4</text>
<pad name="4" x="-3.81" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="1" x="3.81" y="0" drill="0.9" shape="octagon" rot="R90"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<text x="5.0531" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<wire x1="5.83" y1="3.421" x2="5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="5.81" y1="3.421" x2="5.556" y2="3.421" width="0.254" layer="21"/>
<wire x1="5.556" y1="3.421" x2="-5.556" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.556" y1="3.421" x2="-5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.81" y1="3.421" x2="-5.83" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.83" y1="3.421" x2="-5.83" y2="-2.921" width="0.254" layer="21"/>
<wire x1="5.83" y1="3.421" x2="5.83" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-5.83" y1="-2.921" x2="-4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-4.445" y1="-2.921" x2="-4.445" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-4.445" y1="-2.286" x2="-3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.286" x2="-3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.921" x2="-4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.921" x2="4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.921" x2="3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.286" x2="4.445" y2="-2.286" width="0.254" layer="21"/>
<wire x1="4.445" y1="-2.286" x2="4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="4.445" y1="-2.921" x2="5.83" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-5.81" y1="2.405" x2="-5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.81" y1="2.405" x2="-5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="-5.556" y1="2.405" x2="5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="5.556" y1="2.405" x2="5.81" y2="2.405" width="0.254" layer="21"/>
<wire x1="5.81" y1="2.405" x2="5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.81" y1="2.405" x2="-5.556" y2="1.897" width="0.254" layer="21"/>
<wire x1="-5.556" y1="3.421" x2="-5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="-5.556" y1="1.897" x2="5.556" y2="1.897" width="0.254" layer="21"/>
<wire x1="5.556" y1="1.897" x2="5.81" y2="2.405" width="0.254" layer="21"/>
<wire x1="5.556" y1="3.421" x2="5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="-5.81" y1="3.04" x2="5.81" y2="3.04" width="0.3" layer="41"/>
</package>
<package name="VIA_3.2">
<pad name="P$1" x="0" y="0" drill="3.2" shape="octagon"/>
<circle x="0" y="0" radius="4" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3" width="0.127" layer="52"/>
</package>
</packages>
<symbols>
<symbol name="AQM1248A-RN">
<pin name="C1-" x="0" y="-5.08" length="middle" rot="R90"/>
<pin name="VOUT" x="-2.54" y="-5.08" length="middle" rot="R90"/>
<pin name="C2+" x="5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="C1+" x="2.54" y="-5.08" length="middle" rot="R90"/>
<pin name="VSS" x="-5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="VDD" x="-7.62" y="-5.08" length="middle" rot="R90"/>
<pin name="C2-" x="7.62" y="-5.08" length="middle" rot="R90"/>
<pin name="SDA" x="-10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="V1" x="10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="SCL" x="-12.7" y="-5.08" length="middle" rot="R90"/>
<pin name="V2" x="12.7" y="-5.08" length="middle" rot="R90"/>
<pin name="RS" x="-15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="V3" x="15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="!RST" x="-17.78" y="-5.08" length="middle" rot="R90"/>
<pin name="V4" x="17.78" y="-5.08" length="middle" rot="R90"/>
<pin name="!CS1B" x="-20.32" y="-5.08" length="middle" rot="R90"/>
<pin name="V5" x="20.32" y="-5.08" length="middle" rot="R90"/>
<wire x1="0" y1="0" x2="-22.86" y2="0" width="0.254" layer="94"/>
<wire x1="-22.86" y1="0" x2="-22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="-22.86" y1="10.16" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="-20.32" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="M-1">
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="PWRN">
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AQM1248" prefix="LCD">
<gates>
<gate name="G$1" symbol="AQM1248A-RN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AQM1248A-RN">
<connects>
<connect gate="G$1" pin="!CS1B" pad="1"/>
<connect gate="G$1" pin="!RST" pad="2"/>
<connect gate="G$1" pin="C1+" pad="10"/>
<connect gate="G$1" pin="C1-" pad="9"/>
<connect gate="G$1" pin="C2+" pad="11"/>
<connect gate="G$1" pin="C2-" pad="12"/>
<connect gate="G$1" pin="RS" pad="3"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="V1" pad="13"/>
<connect gate="G$1" pin="V2" pad="14"/>
<connect gate="G$1" pin="V3" pad="15"/>
<connect gate="G$1" pin="V4" pad="16"/>
<connect gate="G$1" pin="V5" pad="17"/>
<connect gate="G$1" pin="VDD" pad="6"/>
<connect gate="G$1" pin="VOUT" pad="8"/>
<connect gate="G$1" pin="VSS" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER-SOURCE" prefix="CON">
<gates>
<gate name="_VCC" symbol="M-1" x="5.08" y="5.08"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-5.08"/>
</gates>
<devices>
<device name="-M-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-H" package="D3200S-2-TAB-HORIZONTAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-V" package="D3200S-2-TAB-VERTICAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-V-H" package="D3200S-2-TAB-VERTICAL-AND-HORIZONTAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-VH" package="B2P-VH">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XT30PW-F" package="XT30PW-F">
<connects>
<connect gate="_GND" pin="S" pad="M$1 M$2 N"/>
<connect gate="_VCC" pin="S" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XT30PW-M" package="XT30PW-M">
<connects>
<connect gate="_GND" pin="S" pad="M$1 M$2 N"/>
<connect gate="_VCC" pin="S" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RT-2PIN" prefix="CON">
<gates>
<gate name="_SIGNAL" symbol="M-1" x="5.08" y="2.54"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-2.54"/>
</gates>
<devices>
<device name="-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IL-G-S" package="IL-G-2P-S3T2-SA">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVRISP-NEWSYMBOL" prefix="CON">
<gates>
<gate name="SCK" symbol="M" x="2.54" y="2.54"/>
<gate name="MISO" symbol="M" x="2.54" y="0"/>
<gate name="MOSI" symbol="M" x="2.54" y="-2.54"/>
<gate name="RESET" symbol="M" x="2.54" y="-5.08"/>
<gate name="PWR" symbol="PWRN" x="17.78" y="-2.54"/>
</gates>
<devices>
<device name="-PH" package="2X03">
<connects>
<connect gate="MISO" pin="S" pad="1"/>
<connect gate="MOSI" pin="S" pad="4"/>
<connect gate="PWR" pin="GND" pad="6"/>
<connect gate="PWR" pin="VCC" pad="2"/>
<connect gate="RESET" pin="S" pad="5"/>
<connect gate="SCK" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ML6" package="ML6">
<connects>
<connect gate="MISO" pin="S" pad="1"/>
<connect gate="MOSI" pin="S" pad="4"/>
<connect gate="PWR" pin="GND" pad="6"/>
<connect gate="PWR" pin="VCC" pad="2"/>
<connect gate="RESET" pin="S" pad="5"/>
<connect gate="SCK" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ML6L" package="ML6L">
<connects>
<connect gate="MISO" pin="S" pad="1"/>
<connect gate="MOSI" pin="S" pad="4"/>
<connect gate="PWR" pin="GND" pad="6"/>
<connect gate="PWR" pin="VCC" pad="2"/>
<connect gate="RESET" pin="S" pad="5"/>
<connect gate="SCK" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UART" prefix="CON">
<description>&lt;b&gt;Robotech &lt;br&gt; Universal Asynchronous Receiver Transmitter &lt;/b&gt;</description>
<gates>
<gate name="_RX" symbol="M-1" x="7.62" y="2.54"/>
<gate name="_TX" symbol="M-1" x="7.62" y="-2.54"/>
<gate name="_PWR" symbol="PWRN" x="-10.16" y="0" addlevel="request"/>
</gates>
<devices>
<device name="-S" package="IL-G-04-S">
<connects>
<connect gate="_PWR" pin="GND" pad="1"/>
<connect gate="_PWR" pin="VCC" pad="4"/>
<connect gate="_RX" pin="S" pad="2"/>
<connect gate="_TX" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EARTH" prefix="VIA_">
<gates>
<gate name="_EARTH" symbol="M-1" x="5.08" y="0"/>
</gates>
<devices>
<device name="_3.2" package="VIA_3.2">
<connects>
<connect gate="_EARTH" pin="S" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Passive_Robotech">
<description>&lt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="TP2">
<pad name="1" x="-2.54" y="-2.54" drill="1" shape="offset" rot="R270"/>
<pad name="2" x="0" y="-2.54" drill="1" shape="offset" rot="R270"/>
<pad name="3" x="2.54" y="-2.54" drill="1" shape="offset" rot="R270"/>
<pad name="6" x="2.54" y="2.54" drill="1" shape="offset" rot="R90"/>
<pad name="5" x="0" y="2.54" drill="1" shape="offset" rot="R90"/>
<pad name="4" x="-2.54" y="2.54" drill="1" shape="offset" rot="R90"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="1005">
<description>Metric Code Size 1005</description>
<smd name="1" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
</package>
<package name="1608">
<description>Metric Code Size 1608</description>
<smd name="1" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
</package>
<package name="2012-R">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="3216">
<description>Metric Code Size 3216</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="3225">
<description>Metric Code Size 3225</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
</package>
<package name="4532">
<description>Metric Code Size 4532</description>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="5650">
<description>Metric Code Size 5650</description>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
</package>
<package name="250-80">
<wire x1="12.5" y1="4" x2="-12.5" y2="4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="4" x2="-12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-4" x2="12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="12.5" y1="-4" x2="12.5" y2="4" width="0.127" layer="21"/>
<pad name="P$1" x="-15.24" y="0" drill="1.2" shape="octagon"/>
<pad name="P$2" x="15.24" y="0" drill="1.2" shape="octagon"/>
<rectangle x1="-15.24" y1="-0.508" x2="-12.573" y2="0.508" layer="51"/>
<rectangle x1="12.573" y1="-0.508" x2="15.24" y2="0.508" layer="51"/>
</package>
<package name="6032">
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.909" y1="1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="1.524" x2="2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.909" y1="1.524" x2="2.909" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="-1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="CEMENTR/48">
<description>Cement Resistor
Width=48mm, Depth=10mm, Height=10mm</description>
<pad name="1" x="-29.21" y="0" drill="1.5" shape="octagon"/>
<pad name="2" x="29.21" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="29.21" y1="0" x2="28.194" y2="0" width="0.6096" layer="51"/>
<wire x1="-29.21" y1="0" x2="-28.194" y2="0" width="0.6096" layer="51"/>
<rectangle x1="27.305" y1="-0.3048" x2="28.1686" y2="0.3048" layer="21"/>
<rectangle x1="-28.1686" y1="-0.3048" x2="-27.305" y2="0.3048" layer="21"/>
<wire x1="27.305" y1="0" x2="24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-27.305" y1="0" x2="-24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-24" y1="5" x2="24" y2="5" width="0.127" layer="21"/>
<wire x1="24" y1="5" x2="24" y2="-5" width="0.127" layer="21"/>
<wire x1="24" y1="-5" x2="-24" y2="-5" width="0.127" layer="21"/>
<wire x1="-24" y1="-5" x2="-24" y2="5" width="0.127" layer="21"/>
</package>
<package name="153CLV-0405">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="1.8" y="0" dx="2.6" dy="1.6" layer="1"/>
<smd name="-" x="-1.8" y="0" dx="2.6" dy="1.6" layer="1"/>
<wire x1="1.25" y1="-2.15" x2="-2.15" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-1.1" x2="-2.15" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="1.1" x2="-2.15" y2="2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="2.15" x2="1.25" y2="2.15" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.25" x2="2.15" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.1" x2="2.15" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.05" x2="2.15" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="2.15" y1="-1.25" x2="1.25" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="1.25" y1="2.15" x2="2.15" y2="1.25" width="0.2032" layer="21"/>
<text x="-2.24" y="2.48" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.275" y="-3.735" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.65" y1="-0.95" x2="1.65" y2="-0.95" width="0.2032" layer="21" curve="120.137" cap="flat"/>
<wire x1="-1.65" y1="-0.95" x2="-1.65" y2="0.95" width="0.2032" layer="51" curve="-59.863" cap="flat"/>
<wire x1="-1.65" y1="0.95" x2="1.65" y2="0.95" width="0.2032" layer="21" curve="-120.137" cap="flat"/>
<wire x1="1.65" y1="-0.95" x2="1.65" y2="0.95" width="0.2032" layer="51" curve="59.863" cap="flat"/>
</package>
<package name="153CLV-0505">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="-" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<wire x1="1.75" y1="-2.65" x2="-2.65" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-2.65" x2="-2.65" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-1.1" x2="-2.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="1.1" x2="-2.65" y2="2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="2.65" x2="1.75" y2="2.65" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.75" x2="2.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.1" x2="2.65" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="2.65" y1="-1.1" x2="2.65" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.75" x2="1.75" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.75" y1="2.65" x2="2.65" y2="1.75" width="0.2032" layer="21"/>
<text x="-2.74" y="2.98" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.775" y="-4.235" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.2" y1="-0.95" x2="2.2" y2="-0.95" width="0.2032" layer="21" curve="133.289" cap="flat"/>
<wire x1="-2.2" y1="-0.95" x2="-2.2" y2="0.95" width="0.2032" layer="51" curve="-46.7111" cap="flat"/>
<wire x1="-2.2" y1="0.95" x2="2.2" y2="0.95" width="0.2032" layer="21" curve="-133.289" cap="flat"/>
<wire x1="2.2" y1="-0.95" x2="2.2" y2="0.95" width="0.2032" layer="51" curve="46.7111" cap="flat"/>
</package>
<package name="153CLV-0605">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="-" x="-2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<wire x1="2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.1" x2="-3.3" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="2.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.1" x2="3.3" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.4" x2="2.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.4" y1="3.3" x2="3.3" y2="2.4" width="0.2032" layer="21"/>
<text x="-3.39" y="3.63" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.425" y="-4.885" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.95" y1="-0.9" x2="2.95" y2="-0.95" width="0.2032" layer="21" curve="145.181" cap="flat"/>
<wire x1="-2.95" y1="-0.9" x2="-2.95" y2="0.95" width="0.2032" layer="51" curve="-34.8186" cap="flat"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.9" width="0.2032" layer="21" curve="-145.181" cap="flat"/>
<wire x1="2.95" y1="-0.95" x2="2.95" y2="0.9" width="0.2032" layer="51" curve="34.8186" cap="flat"/>
</package>
<package name="153CLV-0807">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="3.05" y="0" dx="4" dy="1.6" layer="1"/>
<smd name="-" x="-3.05" y="0" dx="4" dy="1.6" layer="1"/>
<wire x1="3.3" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.1" x2="-4.2" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.1" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="3.3" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3.3" x2="4.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.1" x2="4.2" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.1" x2="4.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-3.3" x2="3.3" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="4.2" x2="4.2" y2="3.3" width="0.2032" layer="21"/>
<text x="-4.29" y="4.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-4.325" y="-5.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-3.75" y1="-1.05" x2="3.75" y2="-1.05" width="0.2032" layer="21" curve="148.008" cap="flat"/>
<wire x1="-3.75" y1="-1.05" x2="-3.75" y2="1.05" width="0.2032" layer="51" curve="-31.2845" cap="flat"/>
<wire x1="-3.75" y1="1.05" x2="3.75" y2="1.05" width="0.2032" layer="21" curve="-148.008" cap="flat"/>
<wire x1="3.75" y1="1.05" x2="3.75" y2="-1.05" width="0.2032" layer="51" curve="-31.2845" cap="flat"/>
</package>
<package name="153CLV-0810">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="3.25" y="0" dx="3.5" dy="2.5" layer="1"/>
<smd name="-" x="-3.25" y="0" dx="3.5" dy="2.5" layer="1"/>
<wire x1="3.3" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.5" x2="-4.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.5" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="3.3" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3.3" x2="4.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.5" x2="4.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.5" x2="4.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-3.3" x2="3.3" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="4.2" x2="4.2" y2="3.3" width="0.2032" layer="21"/>
<text x="-4.29" y="4.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-4.325" y="-5.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-3.65" y1="-1.4" x2="3.65" y2="-1.4" width="0.2032" layer="21" curve="138.03" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="-3.65" y2="1.4" width="0.2032" layer="51" curve="-41.9698" cap="flat"/>
<wire x1="-3.65" y1="1.4" x2="3.65" y2="1.4" width="0.2032" layer="21" curve="-138.03" cap="flat"/>
<wire x1="3.65" y1="-1.4" x2="3.65" y2="1.4" width="0.2032" layer="51" curve="41.9698" cap="flat"/>
</package>
<package name="153CLV-1010">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="4" y="0" dx="4" dy="2.5" layer="1"/>
<smd name="-" x="-4" y="0" dx="4" dy="2.5" layer="1"/>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<text x="-5.29" y="5.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-5.325" y="-6.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.13" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.13" cap="flat"/>
</package>
<package name="153CLV-1012">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<text x="-5.29" y="5.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-5.325" y="-6.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.13" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.13" cap="flat"/>
</package>
<package name="153CLV-1014">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<text x="-5.29" y="5.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-5.325" y="-6.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.13" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.13" cap="flat"/>
</package>
<package name="E2,5-5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 5 mm</description>
<text x="2.413" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="2.413" y="-2.413" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<wire x1="-1.651" y1="1.27" x2="-1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="-1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.27" x2="-1.143" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.27" x2="-1.397" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
</package>
<package name="E2,5-6">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 6 mm</description>
<text x="2.667" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<circle x="0" y="0" radius="2.794" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-1.651" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0.889" x2="-1.651" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="1.27" x2="-1.651" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E2,5-7">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 7 mm</description>
<text x="3.048" y="2.032" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="3.048" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="-0.381" x2="-2.667" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E5-10,5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 10.5 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="5.08" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="E5-13">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 13 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-0.889" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.27" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="6.3754" y="4.1148" size="1.778" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.572" y="-3.937" size="1.778" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="6.985" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.445" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.27" x2="0.889" y2="1.27" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="E5-5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 5 mm</description>
<pad name="+" x="-2.54" y="0" drill="0.8128" diameter="1.905"/>
<pad name="-" x="2.54" y="0" drill="0.8128" diameter="1.905" shape="octagon"/>
<wire x1="-2.1557" y1="1.3432" x2="2.1557" y2="1.3432" width="0.1524" layer="21" curve="-116.147" cap="flat"/>
<wire x1="-2.1557" y1="-1.3432" x2="2.1557" y2="-1.3432" width="0.1524" layer="21" curve="116.147" cap="flat"/>
<wire x1="-2.1557" y1="1.3432" x2="-2.1557" y2="-1.3432" width="0.1524" layer="51" curve="63.8534" cap="flat"/>
<wire x1="2.1557" y1="-1.3432" x2="2.1557" y2="1.3432" width="0.1524" layer="51" curve="63.8534" cap="flat"/>
<text x="2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.397" y1="0" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
</package>
<package name="E5-6">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 6 mm</description>
<pad name="+" x="-2.54" y="0" drill="0.8128" diameter="1.905"/>
<pad name="-" x="2.54" y="0" drill="0.8128" diameter="1.905" shape="octagon"/>
<wire x1="-2.8702" y1="1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="21" curve="-129.378" cap="flat"/>
<wire x1="-2.8702" y1="-1.3574" x2="2.8702" y2="-1.3574" width="0.1524" layer="21" curve="129.378" cap="flat"/>
<wire x1="-2.8702" y1="1.3574" x2="-2.8702" y2="-1.3574" width="0.1524" layer="51" curve="50.6216" cap="flat"/>
<wire x1="2.8702" y1="-1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="51" curve="50.6216" cap="flat"/>
<text x="3.048" y="1.778" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="3.048" y="-2.921" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.397" y1="0" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
</package>
<package name="E5-8,5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 8.5 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="4.1402" y="2.286" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.5146" y="-3.0226" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="4.445" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.032" x2="-3.302" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.651" x2="-2.921" y2="1.651" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="E5-10,5A">
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="5.08" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="5.08" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<wire x1="-5" y1="-2" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-2" x2="5" y2="-3" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="-22" width="0.127" layer="21"/>
<wire x1="5" y1="-22" x2="-5" y2="-22" width="0.127" layer="21"/>
<wire x1="-5" y1="-22" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-3" x2="-5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-3" x2="5" y2="-5" width="0.127" layer="21" curve="106.26"/>
<wire x1="-5" y1="-3" x2="-5" y2="-5" width="0.127" layer="21" curve="-106.26"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="-2" width="0.3048" layer="51"/>
<wire x1="2.5" y1="0" x2="2.5" y2="-2" width="0.3048" layer="51"/>
</package>
<package name="E5-10,5A2">
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="5.08" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="5.08" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<wire x1="5" y1="2" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="2" x2="-5" y2="3" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="22" width="0.127" layer="21"/>
<wire x1="-5" y1="22" x2="5" y2="22" width="0.127" layer="21"/>
<wire x1="5" y1="22" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="3" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="3" x2="-5" y2="5" width="0.127" layer="21" curve="106.26"/>
<wire x1="5" y1="3" x2="5" y2="5" width="0.127" layer="21" curve="-106.26"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="2" width="0.3048" layer="51"/>
<wire x1="2.5" y1="0" x2="2.5" y2="2" width="0.3048" layer="51"/>
</package>
<package name="E5-21">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 21 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.5" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.5" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-0.889" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.27" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="8.9154" y="6.6548" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="-3.937" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="10.541" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.445" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.27" x2="0.889" y2="1.27" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
</package>
<package name="2012-C">
<smd name="1" x="-0.9207" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="0.9206" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1.381" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.381" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.889" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-0.635" x2="0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="L5-7,5">
<pad name="P$1" x="2.5" y="0" drill="0.8"/>
<pad name="P$2" x="-2.5" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="0.5" y1="0" x2="1.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<text x="-3" y="-2.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.5" y="-4" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L22">
<pad name="P$1" x="11.43" y="0" drill="2.8" diameter="6.4516"/>
<pad name="P$2" x="-11.43" y="0" drill="2.8" diameter="6.4516"/>
<circle x="0" y="0" radius="11.359225" width="0.127" layer="21"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T50H">
<circle x="0" y="0" radius="2.83980625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="-6.35" drill="1"/>
<pad name="P$2" x="2.54" y="-6.35" drill="1"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T50V">
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T20V">
<pad name="P$1" x="-1.27" y="0" drill="0.8"/>
<pad name="P$2" x="1.27" y="0" drill="0.8"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<text x="-1.905" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-1.905" y="3.175" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="2125">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.8" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.8" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="SOLENOID-D5-L2.5">
<pad name="P$1" x="-1.27" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="27"/>
<wire x1="-0.635" y1="0" x2="0" y2="2.54" width="0.127" layer="27"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="0" width="0.127" layer="27"/>
</package>
<package name="SOLENOID-D5-L5">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
</package>
<package name="NR6028T">
<description>&lt;b&gt;NR6028&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2" layer="51"/>
<wire x1="-4.15" y1="4" x2="4.15" y2="4" width="0.1" layer="51"/>
<wire x1="4.15" y1="4" x2="4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="4.15" y1="-4" x2="-4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="-4.15" y1="-4" x2="-4.15" y2="4" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="21"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="TYS6045">
<smd name="1" x="-2.3" y="0" dx="6" dy="2.2" layer="1" rot="R90"/>
<smd name="2" x="2.3" y="0" dx="6" dy="2.2" layer="1" rot="R90"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="SRP1038">
<smd name="1" x="-4.4" y="0" dx="3.8" dy="3.5" layer="1"/>
<smd name="2" x="4.4" y="0" dx="3.8" dy="3.5" layer="1"/>
<wire x1="5" y1="5" x2="-5" y2="5" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="5" width="0.127" layer="21"/>
<text x="-2.54" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T37V">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<wire x1="-1.6256" y1="4.6736" x2="1.6256" y2="4.6736" width="0.127" layer="21"/>
<wire x1="1.6256" y1="4.6736" x2="1.6256" y2="-4.6736" width="0.127" layer="21"/>
<wire x1="1.6256" y1="-4.6736" x2="-1.6256" y2="-4.6736" width="0.127" layer="21"/>
<wire x1="-1.6256" y1="-4.6736" x2="-1.6256" y2="4.6736" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.127" layer="21"/>
<text x="-2.54" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SLIDINGSW">
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.635" y2="3.175" width="0.254" layer="94"/>
<pin name="P" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="5.08" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<pin name="O" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="-3.81" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="R">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="CC">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3764" cap="flat"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3729" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="L">
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TP2">
<gates>
<gate name="G$1" symbol="SLIDINGSW" x="-12.7" y="0"/>
<gate name="G$2" symbol="SLIDINGSW" x="10.16" y="0"/>
</gates>
<devices>
<device name="" package="TP2">
<connects>
<connect gate="G$1" pin="O" pad="1"/>
<connect gate="G$1" pin="P" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$2" pin="O" pad="4"/>
<connect gate="G$2" pin="P" pad="5"/>
<connect gate="G$2" pin="S" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6032" package="6032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/48" package="CEMENTR/48">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CC" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CC" x="0" y="0"/>
</gates>
<devices>
<device name="-153CLV-0405" package="153CLV-0405">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0505" package="153CLV-0505">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0605" package="153CLV-0605">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0807" package="153CLV-0807">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0810" package="153CLV-0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-1010" package="153CLV-1010">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-1012" package="153CLV-1012">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-1014" package="153CLV-1014">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5-5" package="E2,5-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5-6" package="E2,5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5-7" package="E2,5-7">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-10.5" package="E5-10,5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-13" package="E5-13">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-5" package="E5-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-6" package="E5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-8.5" package="E5-8,5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-10.5A" package="E5-10,5A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-10,5A2" package="E5-10,5A2">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-21" package="E5-21">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7.5" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5(VERTICAL)" package="L5-7,5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22(VERTICAL)" package="L22">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50H" package="T50H">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50V" package="T50V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T20V" package="T20V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2125" package="2125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L2.5" package="SOLENOID-D5-L2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L5" package="SOLENOID-D5-L5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR6028" package="NR6028T">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR8040" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TYS6045" package="TYS6045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SRP1038" package="SRP1038">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T37V" package="T37V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Sensor_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Sensor Device Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Shimazu Makoto&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="EC16B">
<description>Rotary Encoder</description>
<pad name="1" x="-5" y="-10.5" drill="1" shape="square"/>
<pad name="2" x="0" y="-10.5" drill="1" shape="long" rot="R90"/>
<pad name="3" x="5" y="-10.5" drill="1" shape="long" rot="R90"/>
<pad name="4" x="-8.4" y="0" drill="2.2"/>
<pad name="5" x="8.4" y="0" drill="2.2"/>
<wire x1="8" y1="-9.3" x2="8" y2="8.3" width="0.127" layer="21"/>
<wire x1="8" y1="8.3" x2="-8" y2="8.3" width="0.127" layer="21"/>
<wire x1="-8" y1="8.3" x2="-8" y2="-9.3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4.55" width="0.127" layer="21"/>
<circle x="0" y="-9" radius="1.5" width="0.127" layer="21"/>
<wire x1="-8" y1="-9.3" x2="-2" y2="-9.3" width="0.127" layer="21"/>
<wire x1="-2" y1="-9.3" x2="-2" y2="-12.5" width="0.127" layer="21"/>
<wire x1="-2" y1="-12.5" x2="2" y2="-12.5" width="0.127" layer="21"/>
<wire x1="2" y1="-12.5" x2="2" y2="-9.3" width="0.127" layer="21"/>
<wire x1="2" y1="-9.3" x2="8" y2="-9.3" width="0.127" layer="21"/>
</package>
<package name="EC12E">
<pad name="1" x="-2.5" y="-7.5" drill="1" shape="long" rot="R90"/>
<pad name="2" x="0" y="-7.5" drill="1" shape="long" rot="R90"/>
<pad name="3" x="2.5" y="-7.5" drill="1" shape="long" rot="R90"/>
<pad name="P$4" x="-7" y="0" drill="2.2" shape="square" rot="R90"/>
<pad name="P$5" x="7" y="0" drill="2.2" shape="square" rot="R90"/>
<wire x1="-7" y1="7" x2="7" y2="7" width="0.127" layer="21"/>
<wire x1="7" y1="7" x2="7" y2="-7" width="0.127" layer="21"/>
<wire x1="7" y1="-7" x2="-7" y2="-7" width="0.127" layer="21"/>
<wire x1="-7" y1="-7" x2="-7" y2="7" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4.123103125" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.828425" width="0.127" layer="21"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="EC16B">
<pin name="1" x="-7.62" y="5.08" length="middle" direction="pas"/>
<pin name="2" x="-7.62" y="0" length="middle" direction="pas"/>
<pin name="COM" x="-7.62" y="-5.08" length="middle" direction="pas"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="20.32" y2="-7.62" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="7.62" width="0.254" layer="94"/>
<wire x1="20.32" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<circle x="7.62" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="7.62" y="0" radius="5.08" width="0.254" layer="94"/>
<pin name="SHELL1" x="15.24" y="-12.7" length="middle" direction="pas" rot="R90"/>
<pin name="SHELL2" x="17.78" y="-12.7" length="middle" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EC16B">
<description>&lt;hr&gt;
&lt;b&gt;EC16B&lt;/b&gt;&lt;br&gt;
Alps&lt;br&gt;
Rotary Encoder&lt;br&gt;
&lt;hr&gt;</description>
<gates>
<gate name="G$1" symbol="EC16B" x="-7.62" y="0"/>
</gates>
<devices>
<device name="" package="EC16B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="COM" pad="3"/>
<connect gate="G$1" pin="SHELL1" pad="4"/>
<connect gate="G$1" pin="SHELL2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-EC12" package="EC12E">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="COM" pad="2"/>
<connect gate="G$1" pin="SHELL1" pad="P$4"/>
<connect gate="G$1" pin="SHELL2" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="40xx" urn="urn:adsk.eagle:library:80">
<description>&lt;b&gt;CMOS Logic Devices, 4000 Series&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola &lt;i&gt;CMOS LOGIC DATA&lt;/i&gt;; book, 02/88, DL131 REV 1
&lt;li&gt;http://www.elexp.com
&lt;li&gt;http://www.intersil.com
&lt;li&gt;http://www.ls3c.com.tw/product/1/COMOS.html
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16" urn="urn:adsk.eagle:footprint:721/1" library_version="2">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16" urn="urn:adsk.eagle:footprint:722/1" library_version="2">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="DIL16" urn="urn:adsk.eagle:package:822/2" type="model" library_version="2">
<description>Dual In Line Package</description>
<packageinstances>
<packageinstance name="DIL16"/>
</packageinstances>
</package3d>
<package3d name="SO16" urn="urn:adsk.eagle:package:821/2" type="model" library_version="2">
<description>Small Outline package 150 mil</description>
<packageinstances>
<packageinstance name="SO16"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="4044" urn="urn:adsk.eagle:symbol:756/1" library_version="2">
<wire x1="-7.62" y1="-17.78" x2="7.62" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="7.62" y2="17.78" width="0.4064" layer="94"/>
<wire x1="7.62" y1="17.78" x2="-7.62" y2="17.78" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-17.78" width="0.4064" layer="94"/>
<text x="-7.62" y="18.415" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="Q3" x="12.7" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="S0" x="-12.7" y="12.7" length="middle" direction="in"/>
<pin name="R0" x="-12.7" y="15.24" length="middle" direction="in"/>
<pin name="EN" x="-12.7" y="-15.24" length="middle" direction="in"/>
<pin name="R1" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="S1" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="Q1" x="12.7" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="Q2" x="12.7" y="0" length="middle" direction="out" rot="R180"/>
<pin name="S2" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="R2" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="Q0" x="12.7" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="R3" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="S3" x="-12.7" y="-10.16" length="middle" direction="in"/>
</symbol>
<symbol name="PWRN" urn="urn:adsk.eagle:symbol:713/1" library_version="2">
<text x="-1.27" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VDD</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">VSS</text>
<pin name="VSS" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VDD" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="4044" urn="urn:adsk.eagle:component:851/3" prefix="IC" library_version="2">
<description>Quad 3-state NAND R/S &lt;b&gt;LATCH&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="4044" x="20.32" y="0"/>
<gate name="P" symbol="PWRN" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="N" package="DIL16">
<connects>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="Q0" pad="13"/>
<connect gate="A" pin="Q1" pad="9"/>
<connect gate="A" pin="Q2" pad="10"/>
<connect gate="A" pin="Q3" pad="1"/>
<connect gate="A" pin="R0" pad="4"/>
<connect gate="A" pin="R1" pad="6"/>
<connect gate="A" pin="R2" pad="12"/>
<connect gate="A" pin="R3" pad="14"/>
<connect gate="A" pin="S0" pad="3"/>
<connect gate="A" pin="S1" pad="7"/>
<connect gate="A" pin="S2" pad="11"/>
<connect gate="A" pin="S3" pad="15"/>
<connect gate="P" pin="VDD" pad="16"/>
<connect gate="P" pin="VSS" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:822/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="Q0" pad="13"/>
<connect gate="A" pin="Q1" pad="9"/>
<connect gate="A" pin="Q2" pad="10"/>
<connect gate="A" pin="Q3" pad="1"/>
<connect gate="A" pin="R0" pad="4"/>
<connect gate="A" pin="R1" pad="6"/>
<connect gate="A" pin="R2" pad="12"/>
<connect gate="A" pin="R3" pad="14"/>
<connect gate="A" pin="S0" pad="3"/>
<connect gate="A" pin="S1" pad="7"/>
<connect gate="A" pin="S2" pad="11"/>
<connect gate="A" pin="S3" pad="15"/>
<connect gate="P" pin="VDD" pad="16"/>
<connect gate="P" pin="VSS" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:821/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Supply symbol library&lt;br&gt;
$Rev: 25542 $ 
&lt;p&gt;
since 2008&lt;br&gt;
by&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="VCC_1">
<pin name="VCC_1" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="VCC_2">
<pin name="VCC_2" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_1" prefix="P+">
<gates>
<gate name="G$1" symbol="VCC_1" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_2" prefix="P+">
<gates>
<gate name="G$1" symbol="VCC_2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Diode Library &lt;br&gt;
$Rev: 25542 $

&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="D">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1SS193" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC59">
<connects>
<connect gate="G$1" pin="A" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Transistor_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Transitor Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SC70">
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="E" x="0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="B" x="-0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<rectangle x1="-0.15" y1="0.625" x2="0.15" y2="1.05" layer="51"/>
<rectangle x1="0.5" y1="-1.05" x2="0.8" y2="-0.625" layer="51"/>
<rectangle x1="-0.8" y1="-1.05" x2="-0.5" y2="-0.625" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
</package>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="NPN">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DTC123E" prefix="TR">
<description>ROHM Digital Transistor</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="UA" package="SC70">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA" package="SC59">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MicroControler_Robotech">
<packages>
<package name="MLF32">
<description>&lt;b&gt;32M1-A&lt;/b&gt; Micro Lead Frame package (MLF)</description>
<rectangle x1="-2.5" y1="1.6" x2="-2.05" y2="1.9" layer="51"/>
<smd name="1" x="-2.35" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="1.1" x2="-2.05" y2="1.4" layer="51"/>
<smd name="2" x="-2.35" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="0.6" x2="-2.05" y2="0.9" layer="51"/>
<smd name="3" x="-2.35" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="0.1" x2="-2.05" y2="0.4" layer="51"/>
<smd name="4" x="-2.35" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-0.4" x2="-2.05" y2="-0.1" layer="51"/>
<smd name="5" x="-2.35" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-0.9" x2="-2.05" y2="-0.6" layer="51"/>
<smd name="6" x="-2.35" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-1.4" x2="-2.05" y2="-1.1" layer="51"/>
<smd name="7" x="-2.35" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-1.9" x2="-2.05" y2="-1.6" layer="51"/>
<smd name="8" x="-2.35" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-1.9" y1="-2.5" x2="-1.6" y2="-2.05" layer="51"/>
<smd name="9" x="-1.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-1.4" y1="-2.5" x2="-1.1" y2="-2.05" layer="51"/>
<smd name="10" x="-1.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.9" y1="-2.5" x2="-0.6" y2="-2.05" layer="51"/>
<smd name="11" x="-0.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.4" y1="-2.5" x2="-0.1" y2="-2.05" layer="51"/>
<smd name="12" x="-0.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.1" y1="-2.5" x2="0.4" y2="-2.05" layer="51"/>
<smd name="13" x="0.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.6" y1="-2.5" x2="0.9" y2="-2.05" layer="51"/>
<smd name="14" x="0.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="1.1" y1="-2.5" x2="1.4" y2="-2.05" layer="51"/>
<smd name="15" x="1.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="1.6" y1="-2.5" x2="1.9" y2="-2.05" layer="51"/>
<smd name="16" x="1.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-1.9" x2="2.5" y2="-1.6" layer="51"/>
<smd name="17" x="2.35" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-1.4" x2="2.5" y2="-1.1" layer="51"/>
<smd name="18" x="2.35" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-0.9" x2="2.5" y2="-0.6" layer="51"/>
<smd name="19" x="2.35" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-0.4" x2="2.5" y2="-0.1" layer="51"/>
<smd name="20" x="2.35" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="0.1" x2="2.5" y2="0.4" layer="51"/>
<smd name="21" x="2.35" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="0.6" x2="2.5" y2="0.9" layer="51"/>
<smd name="22" x="2.35" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="1.1" x2="2.5" y2="1.4" layer="51"/>
<smd name="23" x="2.35" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="1.6" x2="2.5" y2="1.9" layer="51"/>
<smd name="24" x="2.35" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="1.6" y1="2.05" x2="1.9" y2="2.5" layer="51"/>
<smd name="25" x="1.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="1.1" y1="2.05" x2="1.4" y2="2.5" layer="51"/>
<smd name="26" x="1.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.6" y1="2.05" x2="0.9" y2="2.5" layer="51"/>
<smd name="27" x="0.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.1" y1="2.05" x2="0.4" y2="2.5" layer="51"/>
<smd name="28" x="0.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.4" y1="2.05" x2="-0.1" y2="2.5" layer="51"/>
<smd name="29" x="-0.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.9" y1="2.05" x2="-0.6" y2="2.5" layer="51"/>
<smd name="30" x="-0.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-1.4" y1="2.05" x2="-1.1" y2="2.5" layer="51"/>
<smd name="31" x="-1.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-1.9" y1="2.05" x2="-1.6" y2="2.5" layer="51"/>
<smd name="32" x="-1.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<wire x1="-2.35" y1="2.05" x2="-2.05" y2="2.35" width="0.254" layer="21"/>
<wire x1="-2.05" y1="2.35" x2="2.05" y2="2.35" width="0.254" layer="51"/>
<wire x1="2.05" y1="2.35" x2="2.35" y2="2.05" width="0.254" layer="21"/>
<wire x1="2.35" y1="2.05" x2="2.35" y2="-2.05" width="0.254" layer="51"/>
<wire x1="2.35" y1="-2.05" x2="2.05" y2="-2.35" width="0.254" layer="21"/>
<wire x1="2.05" y1="-2.35" x2="-2.05" y2="-2.35" width="0.254" layer="51"/>
<wire x1="-2.05" y1="-2.35" x2="-2.35" y2="-2.05" width="0.254" layer="21"/>
<wire x1="-2.35" y1="-2.05" x2="-2.35" y2="2.05" width="0.254" layer="51"/>
<circle x="-1.55" y="1.55" radius="0.15" width="0.254" layer="21"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.55" y1="-1.55" x2="1.55" y2="1.55" layer="1"/>
</package>
<package name="TQFP32-08">
<description>&lt;B&gt;Thin Plasic Quad Flat Package&lt;/B&gt; Grid 0.8 mm</description>
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<circle x="-2.7432" y="2.7432" radius="0.3592" width="0.1524" layer="21"/>
<smd name="1" x="-4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<text x="-2.7686" y="5.08" size="0.8128" layer="25">&gt;NAME</text>
<text x="-3.0226" y="-1.27" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<smd name="8" x="-4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<smd name="9" x="-2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<smd name="10" x="-2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<smd name="15" x="2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<smd name="16" x="2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<smd name="17" x="4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<smd name="24" x="4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<smd name="25" x="2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<smd name="26" x="2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<smd name="27" x="1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<smd name="28" x="0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<smd name="31" x="-2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
<smd name="32" x="-2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGA48/88/168-TQFP/QFN32">
<wire x1="-25.4" y1="30.48" x2="25.4" y2="30.48" width="0.254" layer="94"/>
<wire x1="25.4" y1="30.48" x2="25.4" y2="-33.02" width="0.254" layer="94"/>
<wire x1="25.4" y1="-33.02" x2="-25.4" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-33.02" x2="-25.4" y2="30.48" width="0.254" layer="94"/>
<pin name="PB5(SCK/PCINT5)" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2/PCINT7)" x="-30.48" y="7.62" length="middle"/>
<pin name="PB6(XTAL1/TOSC1/PCINT6)" x="-30.48" y="12.7" length="middle"/>
<pin name="GND@1" x="-30.48" y="-27.94" length="middle" direction="pwr"/>
<pin name="VCC@1" x="-30.48" y="0" length="middle" direction="pwr"/>
<pin name="AGND" x="-30.48" y="-22.86" length="middle" direction="pwr"/>
<pin name="AREF" x="-30.48" y="-10.16" length="middle" direction="pas"/>
<pin name="AVCC" x="-30.48" y="-7.62" length="middle" direction="pas"/>
<pin name="PB4(MISO/PCINT4)" x="30.48" y="-27.94" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2A/PCINT3)" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B/PCINT2)" x="30.48" y="-22.86" length="middle" rot="R180"/>
<pin name="PB1(OC1A/PCINT1)" x="30.48" y="-20.32" length="middle" rot="R180"/>
<pin name="PB0(ICP1/CLKO/PCINT0)" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="PD7(AIN1/PCINT23)" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="PD6(AIN0/OC0A/PCINT22)" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="PD5(T1/OC0B/PCINT21)" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="PD4(T0/XCK/PCINT20)" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="PD3(INT1/OC2B/PCINT19)" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="PD2(INT0/PCINT18)" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="PD1(TXD/PCINT17)" x="30.48" y="2.54" length="middle" rot="R180"/>
<pin name="PD0(RXD/PCINT16)" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL/PCINT13)" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA/PCINT12)" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="PC3(ADC3/PCINT11)" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="PC2(ADC2/PCINT10)" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="PC1(ADC1/PCINT9)" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="PC0(ADC0/PCINT8)" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="PC6(/RESET/PCINT14)" x="-30.48" y="27.94" length="middle"/>
<text x="-25.4" y="33.02" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<text x="-25.4" y="-35.56" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND@2" x="-30.48" y="-30.48" length="middle" direction="pwr"/>
<pin name="VCC@2" x="-30.48" y="-2.54" length="middle" direction="pwr"/>
<pin name="ADC7" x="30.48" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="ADC6" x="30.48" y="12.7" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA*8" prefix="IC">
<gates>
<gate name="1" symbol="ATMEGA48/88/168-TQFP/QFN32" x="0" y="0"/>
</gates>
<devices>
<device name="PA-MU" package="MLF32">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="16"/>
<technology name="4"/>
<technology name="8"/>
</technologies>
</device>
<device name="PA-AU" package="TQFP32-08">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="16"/>
<technology name="4"/>
<technology name="8"/>
</technologies>
</device>
<device name="P-AU" package="TQFP32-08">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="32"/>
</technologies>
</device>
<device name="P-MU" package="MLF32">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="32"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="IL-G-8P-S3T2-SA">
<pad name="4" x="0" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-9" y1="3.07" x2="11.54" y2="3.07" width="0.127" layer="21"/>
<wire x1="11.54" y1="3.07" x2="11.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="11.54" y1="2.54" x2="11.54" y2="-2.93" width="0.127" layer="21"/>
<wire x1="11.54" y1="-2.93" x2="-9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-9" y1="-2.93" x2="-9" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="-9" y2="3.07" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="11.54" y2="2.54" width="0.127" layer="21"/>
<text x="-8.89" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.89" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-10.16" y="-2.54" size="1.27" layer="21">1</text>
<pad name="8" x="10.16" y="0" drill="1.1" shape="long" rot="R90"/>
</package>
<package name="5251-06-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="7.4661" y="-3.6561" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<text x="7.0881" y="-2.4399" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-7.0373" y="-1.7511" size="1.27" layer="51" ratio="14" rot="R270">6</text>
<pad name="2" x="3.81" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="6.35" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51" rot="R180"/>
<text x="-7.9741" y="-3.1481" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="3.556" y1="0.254" x2="4.064" y2="8.382" layer="51"/>
<rectangle x1="6.096" y1="0.254" x2="6.604" y2="8.382" layer="51"/>
<pad name="3" x="1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="1.016" y1="0.254" x2="1.524" y2="8.382" layer="51"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="8.382" layer="51"/>
<pad name="4" x="-1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="-4.064" y1="0.254" x2="-3.556" y2="8.382" layer="51"/>
<pad name="5" x="-3.81" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="-6.604" y1="0.254" x2="-6.096" y2="8.382" layer="51"/>
<pad name="6" x="-6.35" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51" rot="R180"/>
<wire x1="-6.35" y1="1.905" x2="-1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="6.35" y1="1.905" x2="7.62" y2="1.905" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="9.906" width="0.254" layer="21"/>
<wire x1="-6.35" y1="9.906" x2="-6.096" y2="10.414" width="0.254" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="9.906" width="0.254" layer="21"/>
<wire x1="6.35" y1="9.906" x2="6.096" y2="10.414" width="0.254" layer="21"/>
<wire x1="6.096" y1="10.414" x2="1.27" y2="10.414" width="0.254" layer="21"/>
<wire x1="1.27" y1="10.414" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="10.414" width="0.254" layer="21"/>
<wire x1="-1.27" y1="10.414" x2="-6.096" y2="10.414" width="0.254" layer="21"/>
<wire x1="-7.62" y1="-3.175" x2="-7.62" y2="1.905" width="0.254" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-3.175" width="0.254" layer="21"/>
<wire x1="7.62" y1="-3.175" x2="-7.62" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-7.62" y1="-0.762" x2="7.62" y2="-0.762" width="0.254" layer="51"/>
<wire x1="-7.62" y1="1.905" x2="-6.35" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="6.35" y2="1.905" width="0.254" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="6.35" y2="2.54" width="0.3" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANSVERTERCTL" prefix="CON">
<gates>
<gate name="VBAT" symbol="M" x="2.54" y="7.62"/>
<gate name="VREG" symbol="M" x="2.54" y="5.08"/>
<gate name="B0" symbol="M" x="2.54" y="2.54"/>
<gate name="B1" symbol="M" x="2.54" y="0"/>
<gate name="RXEN" symbol="M" x="2.54" y="-2.54"/>
<gate name="TXEN" symbol="M" x="2.54" y="-5.08"/>
<gate name="GND" symbol="M" x="2.54" y="-7.62"/>
<gate name="VSTBY" symbol="M" x="2.54" y="-10.16"/>
</gates>
<devices>
<device name="" package="IL-G-8P-S3T2-SA">
<connects>
<connect gate="B0" pin="S" pad="1"/>
<connect gate="B1" pin="S" pad="2"/>
<connect gate="GND" pin="S" pad="3"/>
<connect gate="RXEN" pin="S" pad="5"/>
<connect gate="TXEN" pin="S" pad="6"/>
<connect gate="VBAT" pin="S" pad="7"/>
<connect gate="VREG" pin="S" pad="8"/>
<connect gate="VSTBY" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LIPOBOARD" prefix="CON">
<gates>
<gate name="GND" symbol="M" x="2.54" y="12.7"/>
<gate name="ACPRES" symbol="M" x="2.54" y="10.16"/>
<gate name="ACSEL" symbol="M" x="2.54" y="7.62"/>
<gate name="ENABLE" symbol="M" x="2.54" y="5.08"/>
<gate name="IBAT" symbol="M" x="2.54" y="2.54"/>
<gate name="ALARM" symbol="M" x="2.54" y="0"/>
</gates>
<devices>
<device name="A" package="5251-06-A">
<connects>
<connect gate="ACPRES" pin="S" pad="2"/>
<connect gate="ACSEL" pin="S" pad="3"/>
<connect gate="ALARM" pin="S" pad="6"/>
<connect gate="ENABLE" pin="S" pad="4"/>
<connect gate="GND" pin="S" pad="1"/>
<connect gate="IBAT" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-coax" urn="urn:adsk.eagle:library:133">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="BU-SMA-V" urn="urn:adsk.eagle:footprint:6176/1" library_version="1">
<description>FEMALE &lt;b&gt;SMA CONNECTOR&lt;/b&gt;&lt;p&gt;
Radiall&lt;p&gt;
distributor RS 112-3794</description>
<wire x1="-1.1" y1="3.2" x2="1.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-3.2" x2="-1.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-3.1999" y1="3.1999" x2="3.2" y2="3.2" width="0.2032" layer="51"/>
<wire x1="3.2" y1="3.2" x2="3.1999" y2="-3.1999" width="0.2032" layer="51"/>
<wire x1="3.1999" y1="-3.1999" x2="-3.2" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-3.2" x2="-3.1999" y2="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.27"/>
<pad name="2" x="-2.5499" y="2.5499" drill="1.778"/>
<pad name="3" x="2.5499" y="2.5499" drill="1.778"/>
<pad name="4" x="2.5499" y="-2.5499" drill="1.778"/>
<pad name="5" x="-2.5499" y="-2.5499" drill="1.778"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="BU-SMA-V" urn="urn:adsk.eagle:package:6211/1" type="box" library_version="1">
<description>FEMALE SMA CONNECTOR
Radiall
distributor RS 112-3794</description>
<packageinstances>
<packageinstance name="BU-SMA-V"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="BNC-FGND" urn="urn:adsk.eagle:symbol:6174/1" library_version="1">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BU-SMA-V" urn="urn:adsk.eagle:component:6237/1" prefix="X" library_version="1">
<description>FEMALE &lt;b&gt;SMA CONNECTOR&lt;/b&gt;&lt;p&gt;
Radiall&lt;p&gt;
distributor RS 112-3794</description>
<gates>
<gate name="G$1" symbol="BNC-FGND" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="BU-SMA-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2 3 4 5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6211/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="AnalogDevicesPlls" deviceset="ADF4351" device="H"/>
<part name="OS1" library="IQD-Frequency-Products" library_urn="urn:adsk.eagle:library:90" deviceset="OSC-OE" device="-CFPS-39" package3d_urn="urn:adsk.eagle:package:3367/1" value="ECS-TXO-3225-200-TR"/>
<part name="IC2" library="IC_Robotech" deviceset="NJW4181" device="U3"/>
<part name="IC3" library="IC_Robotech" deviceset="NJW4181" device="U3"/>
<part name="IC4" library="IC_Robotech" deviceset="NJW4181" device="U3"/>
<part name="U1" library="IC_Robotech" deviceset="OKL-T/*" device="N" technology="6-W12"/>
<part name="LCD1" library="Connector_Robotech" deviceset="AQM1248" device=""/>
<part name="U$1" library="Passive_Robotech" deviceset="TP2" device=""/>
<part name="U$2" library="Passive_Robotech" deviceset="TP2" device=""/>
<part name="U$3" library="Sensor_Robotech" deviceset="EC16B" device="-EC12"/>
<part name="IC6" library="40xx" library_urn="urn:adsk.eagle:library:80" deviceset="4044" device="D" package3d_urn="urn:adsk.eagle:package:821/2"/>
<part name="CON1" library="Connector_Robotech" deviceset="POWER-SOURCE" device="-M-A"/>
<part name="CON2" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="R1" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R2" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY1" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY2" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY3" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY4" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY5" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY6" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY7" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="D1" library="Diode_Robotech" deviceset="1SS193" device=""/>
<part name="TR1" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="C1" library="Passive_Robotech" deviceset="CC" device="-3225" value="10u"/>
<part name="C2" library="Passive_Robotech" deviceset="CC" device="-3225" value="10u"/>
<part name="C3" library="Passive_Robotech" deviceset="CC" device="-3225" value="10u"/>
<part name="C4" library="Passive_Robotech" deviceset="CC" device="-3225" value="10u"/>
<part name="C5" library="Passive_Robotech" deviceset="CC" device="-3225" value="1u"/>
<part name="C6" library="Passive_Robotech" deviceset="CC" device="-3225" value="1u"/>
<part name="C7" library="Passive_Robotech" deviceset="CC" device="-3225" value="10u"/>
<part name="C8" library="Passive_Robotech" deviceset="CC" device="-3225" value="10u"/>
<part name="SUPPLY8" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY9" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY10" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY11" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY12" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY13" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY14" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY15" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY16" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R3" library="Passive_Robotech" deviceset="R" device="-2012" value="1.34k"/>
<part name="SUPPLY17" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY18" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="P+1" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="P+2" library="Supply_Robotech" deviceset="VCC_2" device=""/>
<part name="IC5" library="MicroControler_Robotech" deviceset="ATMEGA*8" device="P-AU" technology="32"/>
<part name="CON3" library="Connector_Robotech" deviceset="AVRISP-NEWSYMBOL" device="-ML6L"/>
<part name="CON4" library="Connector_Robotech" deviceset="UART" device="-S"/>
<part name="P+3" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="P+4" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="P+5" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="P+6" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="SUPPLY19" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C9" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C10" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C11" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C12" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C13" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C14" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C15" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C16" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C17" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C18" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C19" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="C20" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="SUPPLY20" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY21" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY22" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY23" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY24" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY25" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY26" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY27" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY28" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY29" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY30" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY31" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY32" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R4" library="Passive_Robotech" deviceset="R" device="-1608" value="5.1k"/>
<part name="SUPPLY34" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY35" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C21" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C22" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C23" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C24" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C25" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C26" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="SUPPLY33" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY36" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY37" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY38" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY39" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY40" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C27" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C28" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="SUPPLY41" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY42" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="P+7" library="Supply_Robotech" deviceset="VCC_2" device=""/>
<part name="C29" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C30" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C31" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C32" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C33" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C34" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="P+8" library="Supply_Robotech" deviceset="VCC_2" device=""/>
<part name="SUPPLY43" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY44" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY45" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY46" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY47" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY48" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="P+9" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="SUPPLY49" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R5" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R6" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R7" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="R8" library="Passive_Robotech" deviceset="R" device="-2012" value="1k"/>
<part name="R9" library="Passive_Robotech" deviceset="R" device="-2012" value="1k"/>
<part name="R10" library="Passive_Robotech" deviceset="R" device="-2012" value="2.2k"/>
<part name="SUPPLY50" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY51" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY52" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY53" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY54" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY55" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY56" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY57" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C35" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C38" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="C39" library="Passive_Robotech" deviceset="C" device="-1608" value="3.9n"/>
<part name="C40" library="Passive_Robotech" deviceset="C" device="-1608" value="0.1u"/>
<part name="C41" library="Passive_Robotech" deviceset="C" device="-1608" value="560p"/>
<part name="R11" library="Passive_Robotech" deviceset="R" device="-1608" value="620"/>
<part name="R12" library="Passive_Robotech" deviceset="R" device="-1608" value="n/a"/>
<part name="R13" library="Passive_Robotech" deviceset="R" device="-1608" value="3.9k"/>
<part name="SUPPLY59" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY60" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY61" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY62" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L1" library="Passive_Robotech" deviceset="L" device="-2125"/>
<part name="L2" library="Passive_Robotech" deviceset="L" device="-1608" value="15n"/>
<part name="L3" library="Passive_Robotech" deviceset="L" device="-1608" value="12n"/>
<part name="L4" library="Passive_Robotech" deviceset="L" device="-1608" value="12n"/>
<part name="L5" library="Passive_Robotech" deviceset="L" device="-1608" value="120n"/>
<part name="L6" library="Passive_Robotech" deviceset="L" device="-1608" value="30n"/>
<part name="L7" library="Passive_Robotech" deviceset="L" device="-1608" value="30n"/>
<part name="C42" library="Passive_Robotech" deviceset="C" device="-1608" value="2.2p"/>
<part name="C43" library="Passive_Robotech" deviceset="C" device="-1608" value="2.2p"/>
<part name="C44" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C45" library="Passive_Robotech" deviceset="C" device="-1608" value="5.6p"/>
<part name="C46" library="Passive_Robotech" deviceset="C" device="-1608" value="5.6p"/>
<part name="C47" library="Passive_Robotech" deviceset="C" device="-1608" value="120p"/>
<part name="SUPPLY63" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY64" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C48" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="C49" library="Passive_Robotech" deviceset="C" device="-1608" value="0.01u"/>
<part name="SUPPLY65" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY66" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON5" library="connector" deviceset="TRANSVERTERCTL" device=""/>
<part name="CON6" library="connector" deviceset="LIPOBOARD" device="A"/>
<part name="SUPPLY67" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="IC7" library="IC_Robotech" deviceset="ADP151" device="AUJZ"/>
<part name="C50" library="Passive_Robotech" deviceset="C" device="-2012" value="10u"/>
<part name="C51" library="Passive_Robotech" deviceset="C" device="-2012" value="1u"/>
<part name="SUPPLY68" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY69" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY70" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C52" library="Passive_Robotech" deviceset="C" device="-1608" value="10p"/>
<part name="C54" library="Passive_Robotech" deviceset="C" device="-2012" value="0.1u"/>
<part name="SUPPLY71" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY73" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L8" library="Passive_Robotech" deviceset="L" device="-2125"/>
<part name="X2" library="con-coax" library_urn="urn:adsk.eagle:library:133" deviceset="BU-SMA-V" device="" package3d_urn="urn:adsk.eagle:package:6211/1"/>
<part name="X3" library="con-coax" library_urn="urn:adsk.eagle:library:133" deviceset="BU-SMA-V" device="" package3d_urn="urn:adsk.eagle:package:6211/1"/>
<part name="SUPPLY75" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="VIA_1" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_2" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_3" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="VIA_4" library="Connector_Robotech" deviceset="EARTH" device="_3.2"/>
<part name="U$4" library="Passive_Robotech" deviceset="TP2" device=""/>
<part name="P+10" library="Supply_Robotech" deviceset="VCC_1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="76.2" y="66.04"/>
<instance part="OS1" gate="A" x="96.52" y="15.24"/>
<instance part="IC2" gate="G$1" x="-106.68" y="139.7"/>
<instance part="IC3" gate="G$1" x="-25.4" y="139.7"/>
<instance part="IC4" gate="G$1" x="-25.4" y="119.38"/>
<instance part="U1" gate="G$1" x="-66.04" y="124.46"/>
<instance part="LCD1" gate="G$1" x="30.48" y="132.08"/>
<instance part="U$1" gate="G$1" x="0" y="2.54" rot="R180"/>
<instance part="U$2" gate="G$1" x="25.4" y="0" rot="R180"/>
<instance part="U$3" gate="G$1" x="50.8" y="7.62"/>
<instance part="IC6" gate="A" x="-106.68" y="104.14"/>
<instance part="IC6" gate="P" x="-91.44" y="134.62"/>
<instance part="CON1" gate="_VCC" x="-129.54" y="142.24" rot="R180"/>
<instance part="CON1" gate="_GND" x="-129.54" y="139.7" rot="R180"/>
<instance part="CON2" gate="_SIGNAL" x="-132.08" y="119.38" rot="R180"/>
<instance part="CON2" gate="_GND" x="-142.24" y="111.76" rot="R180"/>
<instance part="R1" gate="G$1" x="-119.38" y="124.46" rot="R180"/>
<instance part="R2" gate="G$1" x="-119.38" y="129.54" rot="R180"/>
<instance part="SUPPLY1" gate="GND" x="-121.92" y="88.9"/>
<instance part="SUPPLY2" gate="GND" x="-134.62" y="109.22"/>
<instance part="SUPPLY3" gate="GND" x="-106.68" y="129.54"/>
<instance part="SUPPLY4" gate="GND" x="-124.46" y="137.16"/>
<instance part="SUPPLY5" gate="GND" x="-53.34" y="109.22"/>
<instance part="SUPPLY6" gate="GND" x="-25.4" y="109.22"/>
<instance part="SUPPLY7" gate="GND" x="-25.4" y="129.54"/>
<instance part="D1" gate="G$1" x="-116.84" y="78.74" rot="R180"/>
<instance part="TR1" gate="G$1" x="-121.92" y="63.5" rot="MR0"/>
<instance part="C1" gate="G$1" x="-119.38" y="139.7"/>
<instance part="C2" gate="G$1" x="-86.36" y="139.7"/>
<instance part="C3" gate="G$1" x="-73.66" y="149.86"/>
<instance part="C4" gate="G$1" x="-45.72" y="111.76"/>
<instance part="C5" gate="G$1" x="-38.1" y="116.84"/>
<instance part="C6" gate="G$1" x="-38.1" y="137.16"/>
<instance part="C7" gate="G$1" x="-12.7" y="137.16"/>
<instance part="C8" gate="G$1" x="-10.16" y="116.84"/>
<instance part="SUPPLY8" gate="GND" x="-119.38" y="132.08"/>
<instance part="SUPPLY9" gate="GND" x="-86.36" y="132.08"/>
<instance part="SUPPLY10" gate="GND" x="-73.66" y="142.24"/>
<instance part="SUPPLY11" gate="GND" x="-38.1" y="129.54"/>
<instance part="SUPPLY12" gate="GND" x="-12.7" y="129.54"/>
<instance part="SUPPLY13" gate="GND" x="-45.72" y="104.14"/>
<instance part="SUPPLY14" gate="GND" x="-38.1" y="109.22"/>
<instance part="SUPPLY15" gate="GND" x="-10.16" y="109.22"/>
<instance part="SUPPLY16" gate="GND" x="-124.46" y="55.88"/>
<instance part="R3" gate="G$1" x="-48.26" y="124.46" rot="R90"/>
<instance part="SUPPLY17" gate="GND" x="-48.26" y="114.3"/>
<instance part="SUPPLY18" gate="GND" x="-91.44" y="124.46"/>
<instance part="P+1" gate="G$1" x="-12.7" y="144.78"/>
<instance part="P+2" gate="G$1" x="-7.62" y="124.46"/>
<instance part="IC5" gate="1" x="-40.64" y="58.42"/>
<instance part="CON3" gate="SCK" x="25.4" y="27.94"/>
<instance part="CON3" gate="MISO" x="25.4" y="33.02"/>
<instance part="CON3" gate="MOSI" x="25.4" y="30.48"/>
<instance part="CON3" gate="RESET" x="-76.2" y="86.36" rot="R180"/>
<instance part="CON3" gate="PWR" x="-20.32" y="7.62"/>
<instance part="CON4" gate="_RX" x="10.16" y="63.5"/>
<instance part="CON4" gate="_TX" x="10.16" y="60.96"/>
<instance part="P+3" gate="G$1" x="5.08" y="127"/>
<instance part="P+4" gate="G$1" x="50.8" y="93.98"/>
<instance part="P+5" gate="G$1" x="-83.82" y="58.42"/>
<instance part="P+6" gate="G$1" x="-20.32" y="17.78"/>
<instance part="SUPPLY19" gate="GND" x="-20.32" y="-2.54"/>
<instance part="C9" gate="G$1" x="-91.44" y="53.34"/>
<instance part="C10" gate="G$1" x="-86.36" y="53.34"/>
<instance part="C11" gate="G$1" x="-81.28" y="53.34"/>
<instance part="C12" gate="G$1" x="22.86" y="114.3"/>
<instance part="C13" gate="G$1" x="27.94" y="114.3"/>
<instance part="C14" gate="G$1" x="33.02" y="114.3"/>
<instance part="C15" gate="G$1" x="38.1" y="114.3"/>
<instance part="C16" gate="G$1" x="43.18" y="114.3"/>
<instance part="C17" gate="G$1" x="48.26" y="114.3"/>
<instance part="C18" gate="G$1" x="53.34" y="114.3"/>
<instance part="C19" gate="G$1" x="58.42" y="114.3"/>
<instance part="C20" gate="G$1" x="63.5" y="114.3"/>
<instance part="SUPPLY20" gate="GND" x="22.86" y="106.68"/>
<instance part="SUPPLY21" gate="GND" x="27.94" y="106.68"/>
<instance part="SUPPLY22" gate="GND" x="43.18" y="106.68"/>
<instance part="SUPPLY23" gate="GND" x="48.26" y="106.68"/>
<instance part="SUPPLY24" gate="GND" x="53.34" y="106.68"/>
<instance part="SUPPLY25" gate="GND" x="58.42" y="106.68"/>
<instance part="SUPPLY26" gate="GND" x="63.5" y="106.68"/>
<instance part="SUPPLY27" gate="GND" x="66.04" y="40.64"/>
<instance part="SUPPLY28" gate="GND" x="68.58" y="40.64"/>
<instance part="SUPPLY29" gate="GND" x="71.12" y="40.64"/>
<instance part="SUPPLY30" gate="GND" x="73.66" y="40.64"/>
<instance part="SUPPLY31" gate="GND" x="76.2" y="40.64"/>
<instance part="SUPPLY32" gate="GND" x="78.74" y="40.64"/>
<instance part="R4" gate="G$1" x="53.34" y="50.8" rot="R270"/>
<instance part="SUPPLY34" gate="GND" x="53.34" y="43.18"/>
<instance part="SUPPLY35" gate="GND" x="25.4" y="124.46"/>
<instance part="C21" gate="G$1" x="48.26" y="50.8"/>
<instance part="C22" gate="G$1" x="38.1" y="50.8"/>
<instance part="C23" gate="G$1" x="86.36" y="38.1"/>
<instance part="C24" gate="G$1" x="91.44" y="38.1"/>
<instance part="C25" gate="G$1" x="33.02" y="50.8"/>
<instance part="C26" gate="G$1" x="43.18" y="50.8"/>
<instance part="SUPPLY33" gate="GND" x="48.26" y="43.18"/>
<instance part="SUPPLY36" gate="GND" x="43.18" y="43.18"/>
<instance part="SUPPLY37" gate="GND" x="38.1" y="43.18"/>
<instance part="SUPPLY38" gate="GND" x="33.02" y="43.18"/>
<instance part="SUPPLY39" gate="GND" x="86.36" y="30.48"/>
<instance part="SUPPLY40" gate="GND" x="91.44" y="30.48"/>
<instance part="C27" gate="G$1" x="43.18" y="88.9"/>
<instance part="C28" gate="G$1" x="38.1" y="88.9"/>
<instance part="SUPPLY41" gate="GND" x="38.1" y="81.28"/>
<instance part="SUPPLY42" gate="GND" x="43.18" y="81.28"/>
<instance part="P+7" gate="G$1" x="71.12" y="106.68"/>
<instance part="C29" gate="G$1" x="83.82" y="106.68"/>
<instance part="C30" gate="G$1" x="88.9" y="106.68"/>
<instance part="C31" gate="G$1" x="93.98" y="106.68"/>
<instance part="C32" gate="G$1" x="99.06" y="106.68"/>
<instance part="C33" gate="G$1" x="104.14" y="106.68"/>
<instance part="C34" gate="G$1" x="109.22" y="106.68"/>
<instance part="P+8" gate="G$1" x="83.82" y="111.76"/>
<instance part="SUPPLY43" gate="GND" x="83.82" y="99.06"/>
<instance part="SUPPLY44" gate="GND" x="88.9" y="99.06"/>
<instance part="SUPPLY45" gate="GND" x="93.98" y="99.06"/>
<instance part="SUPPLY46" gate="GND" x="99.06" y="99.06"/>
<instance part="SUPPLY47" gate="GND" x="104.14" y="99.06"/>
<instance part="SUPPLY48" gate="GND" x="109.22" y="99.06"/>
<instance part="P+9" gate="G$1" x="-10.16" y="2.54"/>
<instance part="SUPPLY49" gate="GND" x="0" y="-12.7"/>
<instance part="R5" gate="G$1" x="22.86" y="12.7" rot="R270"/>
<instance part="R6" gate="G$1" x="10.16" y="12.7" rot="R270"/>
<instance part="R7" gate="G$1" x="-2.54" y="12.7" rot="R270"/>
<instance part="R8" gate="G$1" x="5.08" y="17.78"/>
<instance part="R9" gate="G$1" x="17.78" y="17.78"/>
<instance part="R10" gate="G$1" x="27.94" y="7.62" rot="R90"/>
<instance part="SUPPLY50" gate="GND" x="27.94" y="-12.7"/>
<instance part="SUPPLY51" gate="GND" x="43.18" y="0"/>
<instance part="SUPPLY52" gate="GND" x="66.04" y="-7.62"/>
<instance part="SUPPLY53" gate="GND" x="68.58" y="-7.62"/>
<instance part="SUPPLY54" gate="GND" x="-91.44" y="45.72"/>
<instance part="SUPPLY55" gate="GND" x="-86.36" y="45.72"/>
<instance part="SUPPLY56" gate="GND" x="-81.28" y="45.72"/>
<instance part="SUPPLY57" gate="GND" x="-71.12" y="22.86"/>
<instance part="C35" gate="G$1" x="-73.66" y="43.18"/>
<instance part="C38" gate="G$1" x="78.74" y="15.24"/>
<instance part="C39" gate="G$1" x="106.68" y="55.88"/>
<instance part="C40" gate="G$1" x="114.3" y="55.88"/>
<instance part="C41" gate="G$1" x="127" y="53.34"/>
<instance part="R11" gate="G$1" x="114.3" y="43.18" rot="R270"/>
<instance part="R12" gate="G$1" x="104.14" y="43.18" rot="R270"/>
<instance part="R13" gate="G$1" x="121.92" y="58.42"/>
<instance part="SUPPLY59" gate="GND" x="114.3" y="35.56"/>
<instance part="SUPPLY60" gate="GND" x="106.68" y="48.26"/>
<instance part="SUPPLY61" gate="GND" x="127" y="43.18"/>
<instance part="SUPPLY62" gate="GND" x="78.74" y="5.08"/>
<instance part="L1" gate="G$1" x="71.12" y="20.32" rot="R90"/>
<instance part="L2" gate="G$1" x="101.6" y="88.9"/>
<instance part="L3" gate="G$1" x="109.22" y="88.9"/>
<instance part="L4" gate="G$1" x="116.84" y="81.28" rot="R90"/>
<instance part="L5" gate="G$1" x="134.62" y="76.2"/>
<instance part="L6" gate="G$1" x="137.16" y="76.2"/>
<instance part="L7" gate="G$1" x="142.24" y="66.04" rot="R90"/>
<instance part="C42" gate="G$1" x="114.3" y="83.82" rot="R90"/>
<instance part="C43" gate="G$1" x="109.22" y="78.74"/>
<instance part="C44" gate="G$1" x="124.46" y="83.82" rot="R90"/>
<instance part="C45" gate="G$1" x="137.16" y="60.96"/>
<instance part="C46" gate="G$1" x="139.7" y="68.58" rot="R90"/>
<instance part="C47" gate="G$1" x="152.4" y="68.58" rot="R90"/>
<instance part="SUPPLY63" gate="GND" x="109.22" y="71.12"/>
<instance part="SUPPLY64" gate="GND" x="137.16" y="53.34"/>
<instance part="C48" gate="G$1" x="142.24" y="81.28" rot="R90"/>
<instance part="C49" gate="G$1" x="116.84" y="93.98" rot="R90"/>
<instance part="SUPPLY65" gate="GND" x="121.92" y="91.44"/>
<instance part="SUPPLY66" gate="GND" x="147.32" y="78.74"/>
<instance part="CON5" gate="VBAT" x="-127" y="152.4" rot="R180"/>
<instance part="CON5" gate="VREG" x="-43.18" y="147.32"/>
<instance part="CON5" gate="B0" x="7.62" y="50.8"/>
<instance part="CON5" gate="B1" x="7.62" y="53.34"/>
<instance part="CON5" gate="RXEN" x="7.62" y="55.88"/>
<instance part="CON5" gate="TXEN" x="7.62" y="58.42"/>
<instance part="CON6" gate="GND" x="-55.88" y="17.78"/>
<instance part="CON6" gate="ACPRES" x="2.54" y="83.82"/>
<instance part="CON6" gate="ACSEL" x="2.54" y="81.28"/>
<instance part="CON6" gate="ENABLE" x="2.54" y="78.74"/>
<instance part="CON6" gate="IBAT" x="2.54" y="76.2"/>
<instance part="CON6" gate="ALARM" x="2.54" y="73.66"/>
<instance part="SUPPLY67" gate="GND" x="-58.42" y="-2.54"/>
<instance part="IC7" gate="G$1" x="-25.4" y="167.64"/>
<instance part="C50" gate="G$1" x="-5.08" y="167.64"/>
<instance part="C51" gate="G$1" x="-43.18" y="162.56"/>
<instance part="SUPPLY68" gate="GND" x="-43.18" y="154.94"/>
<instance part="SUPPLY69" gate="GND" x="-12.7" y="162.56"/>
<instance part="SUPPLY70" gate="GND" x="-5.08" y="160.02"/>
<instance part="C52" gate="G$1" x="83.82" y="127"/>
<instance part="C54" gate="G$1" x="93.98" y="127"/>
<instance part="SUPPLY71" gate="GND" x="83.82" y="119.38"/>
<instance part="SUPPLY73" gate="GND" x="93.98" y="119.38"/>
<instance part="L8" gate="G$1" x="109.22" y="129.54" rot="R90"/>
<instance part="X2" gate="G$1" x="165.1" y="83.82" rot="MR0"/>
<instance part="X3" gate="G$1" x="165.1" y="68.58" rot="MR0"/>
<instance part="SUPPLY75" gate="GND" x="160.02" y="58.42"/>
<instance part="VIA_1" gate="_EARTH" x="-53.34" y="10.16"/>
<instance part="VIA_2" gate="_EARTH" x="-53.34" y="7.62"/>
<instance part="VIA_3" gate="_EARTH" x="-53.34" y="5.08"/>
<instance part="VIA_4" gate="_EARTH" x="-53.34" y="2.54"/>
<instance part="U$4" gate="G$1" x="12.7" y="2.54" rot="R180"/>
<instance part="P+10" gate="G$1" x="66.04" y="22.86"/>
<instance part="CON5" gate="GND" x="-127" y="149.86" rot="R180"/>
<instance part="CON5" gate="VSTBY" x="-88.9" y="147.32"/>
</instances>
<busses>
</busses>
<nets>
<net name="OUT" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="OUT"/>
<pinref part="IC6" gate="P" pin="VDD"/>
<wire x1="-93.98" y1="142.24" x2="-91.44" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="142.24" x2="-88.9" y2="142.24" width="0.1524" layer="91"/>
<junction x="-91.44" y="142.24"/>
<pinref part="IC6" gate="A" pin="EN"/>
<wire x1="-119.38" y1="88.9" x2="-119.38" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="83.82" x2="-88.9" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="83.82" x2="-88.9" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="86.36" x2="-88.9" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="124.46" x2="-88.9" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="124.46" x2="-88.9" y2="124.46" width="0.1524" layer="91"/>
<junction x="-88.9" y="124.46"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-114.3" y1="129.54" x2="-114.3" y2="124.46" width="0.1524" layer="91"/>
<junction x="-114.3" y="124.46"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="-88.9" y1="142.24" x2="-86.36" y2="142.24" width="0.1524" layer="91"/>
<junction x="-88.9" y="142.24"/>
<pinref part="CON5" gate="VSTBY" pin="S"/>
<wire x1="-91.44" y1="147.32" x2="-91.44" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<wire x1="-53.34" y1="134.62" x2="-50.8" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="134.62" x2="-45.72" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="134.62" x2="-45.72" y2="121.92" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="IN"/>
<wire x1="-45.72" y1="121.92" x2="-45.72" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="142.24" x2="-38.1" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="IN"/>
<wire x1="-38.1" y1="121.92" x2="-45.72" y2="121.92" width="0.1524" layer="91"/>
<junction x="-45.72" y="121.92"/>
<pinref part="C6" gate="G$1" pin="+"/>
<wire x1="-38.1" y1="139.7" x2="-38.1" y2="142.24" width="0.1524" layer="91"/>
<junction x="-38.1" y="142.24"/>
<pinref part="C5" gate="G$1" pin="+"/>
<wire x1="-38.1" y1="119.38" x2="-38.1" y2="121.92" width="0.1524" layer="91"/>
<junction x="-38.1" y="121.92"/>
<pinref part="C4" gate="G$1" pin="+"/>
<wire x1="-45.72" y1="114.3" x2="-45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="-45.72" y="134.62"/>
<pinref part="CON5" gate="VREG" pin="S"/>
<wire x1="-45.72" y1="147.32" x2="-45.72" y2="142.24" width="0.1524" layer="91"/>
<junction x="-45.72" y="142.24"/>
<pinref part="IC7" gate="G$1" pin="EN"/>
<wire x1="-38.1" y1="165.1" x2="-43.18" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="165.1" x2="-45.72" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="165.1" x2="-45.72" y2="147.32" width="0.1524" layer="91"/>
<junction x="-45.72" y="147.32"/>
<pinref part="IC7" gate="G$1" pin="VIN"/>
<wire x1="-38.1" y1="170.18" x2="-45.72" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="170.18" x2="-45.72" y2="165.1" width="0.1524" layer="91"/>
<junction x="-45.72" y="165.1"/>
<pinref part="C51" gate="G$1" pin="1"/>
<junction x="-43.18" y="165.1"/>
<pinref part="U1" gate="G$1" pin="SENSE"/>
<wire x1="-53.34" y1="127" x2="-50.8" y2="127" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="127" x2="-50.8" y2="134.62" width="0.1524" layer="91"/>
<junction x="-50.8" y="134.62"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC6" gate="A" pin="Q0"/>
<wire x1="-93.98" y1="119.38" x2="-83.82" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="119.38" x2="-83.82" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="ON/OFF"/>
<wire x1="-83.82" y1="129.54" x2="-78.74" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="CON1" gate="_VCC" pin="S"/>
<pinref part="IC2" gate="G$1" pin="IN"/>
<wire x1="-124.46" y1="142.24" x2="-119.38" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="142.24" x2="-119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="152.4" x2="-81.28" y2="152.4" width="0.1524" layer="91"/>
<junction x="-119.38" y="142.24"/>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="-81.28" y1="152.4" x2="-78.74" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="+"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="-78.74" y1="152.4" x2="-78.74" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="152.4" x2="-78.74" y2="152.4" width="0.1524" layer="91"/>
<junction x="-78.74" y="152.4"/>
<pinref part="CON5" gate="VBAT" pin="S"/>
<wire x1="-124.46" y1="152.4" x2="-119.38" y2="152.4" width="0.1524" layer="91"/>
<junction x="-119.38" y="152.4"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="CON2" gate="_SIGNAL" pin="S"/>
<pinref part="IC6" gate="A" pin="R0"/>
<wire x1="-127" y1="119.38" x2="-119.38" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-124.46" y1="129.54" x2="-127" y2="129.54" width="0.1524" layer="91"/>
<wire x1="-127" y1="129.54" x2="-127" y2="119.38" width="0.1524" layer="91"/>
<junction x="-127" y="119.38"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="-119.38" y1="78.74" x2="-127" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-127" y1="78.74" x2="-127" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-124.46" y1="124.46" x2="-124.46" y2="116.84" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="S0"/>
<wire x1="-124.46" y1="116.84" x2="-119.38" y2="116.84" width="0.1524" layer="91"/>
<pinref part="TR1" gate="G$1" pin="C"/>
<wire x1="-124.46" y1="68.58" x2="-124.46" y2="116.84" width="0.1524" layer="91"/>
<junction x="-124.46" y="116.84"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="CON2" gate="_GND" pin="S"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="-137.16" y1="111.76" x2="-134.62" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON1" gate="_GND" pin="S"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="-124.46" y1="149.86" x2="-124.46" y2="139.7" width="0.1524" layer="91"/>
<junction x="-124.46" y="139.7"/>
<pinref part="CON5" gate="GND" pin="S"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="R1"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="-119.38" y1="111.76" x2="-121.92" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="111.76" x2="-121.92" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="S1"/>
<wire x1="-121.92" y1="109.22" x2="-121.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="104.14" x2="-121.92" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="101.6" x2="-121.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="96.52" x2="-121.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="93.98" x2="-121.92" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="109.22" x2="-121.92" y2="109.22" width="0.1524" layer="91"/>
<junction x="-121.92" y="109.22"/>
<pinref part="IC6" gate="A" pin="R2"/>
<wire x1="-119.38" y1="104.14" x2="-121.92" y2="104.14" width="0.1524" layer="91"/>
<junction x="-121.92" y="104.14"/>
<pinref part="IC6" gate="A" pin="S2"/>
<wire x1="-119.38" y1="101.6" x2="-121.92" y2="101.6" width="0.1524" layer="91"/>
<junction x="-121.92" y="101.6"/>
<pinref part="IC6" gate="A" pin="R3"/>
<wire x1="-119.38" y1="96.52" x2="-121.92" y2="96.52" width="0.1524" layer="91"/>
<junction x="-121.92" y="96.52"/>
<pinref part="IC6" gate="A" pin="S3"/>
<wire x1="-119.38" y1="93.98" x2="-121.92" y2="93.98" width="0.1524" layer="91"/>
<junction x="-121.92" y="93.98"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GND2"/>
<wire x1="-53.34" y1="111.76" x2="-53.34" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND1"/>
<wire x1="-53.34" y1="119.38" x2="-53.34" y2="116.84" width="0.1524" layer="91"/>
<junction x="-53.34" y="116.84"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="-"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="-"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="-"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="-"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="-"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="-"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="-"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="-"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR1" gate="G$1" pin="E"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="-48.26" y1="119.38" x2="-48.26" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC6" gate="P" pin="VSS"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON3" gate="PWR" pin="GND"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="DGND"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SDGND"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="CPGND"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="AGND"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="EPAD"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCOGND"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="O"/>
<wire x1="0" y1="-2.54" x2="0" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="0" y1="-10.16" x2="12.7" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="O"/>
<wire x1="12.7" y1="-10.16" x2="25.4" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-10.16" x2="25.4" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="-10.16" width="0.1524" layer="91"/>
<junction x="12.7" y="-10.16"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<junction x="0" y="-10.16"/>
<pinref part="U$4" gate="G$1" pin="O"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="COM"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="SHELL1"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="SHELL2"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
<pinref part="IC5" gate="1" pin="GND@2"/>
<wire x1="-71.12" y1="25.4" x2="-71.12" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="GND@1"/>
<wire x1="-71.12" y1="30.48" x2="-71.12" y2="27.94" width="0.1524" layer="91"/>
<junction x="-71.12" y="27.94"/>
<pinref part="IC5" gate="1" pin="AGND"/>
<wire x1="-71.12" y1="35.56" x2="-71.12" y2="30.48" width="0.1524" layer="91"/>
<junction x="-71.12" y="30.48"/>
<wire x1="-71.12" y1="35.56" x2="-73.66" y2="35.56" width="0.1524" layer="91"/>
<junction x="-71.12" y="35.56"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="-73.66" y1="35.56" x2="-73.66" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="SUPPLY60" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
<wire x1="127" y1="48.26" x2="127" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OS1" gate="A" pin="VSS"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="83.82" y1="10.16" x2="78.74" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY62" gate="GND" pin="GND"/>
<wire x1="78.74" y1="7.62" x2="78.74" y2="10.16" width="0.1524" layer="91"/>
<junction x="78.74" y="10.16"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<pinref part="SUPPLY64" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="2"/>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<pinref part="SUPPLY66" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON6" gate="GND" pin="S"/>
<pinref part="SUPPLY67" gate="GND" pin="GND"/>
<wire x1="-58.42" y1="0" x2="-58.42" y2="2.54" width="0.1524" layer="91"/>
<pinref part="VIA_1" gate="_EARTH" pin="S"/>
<wire x1="-58.42" y1="2.54" x2="-58.42" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="5.08" x2="-58.42" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="7.62" x2="-58.42" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="10.16" x2="-58.42" y2="17.78" width="0.1524" layer="91"/>
<junction x="-58.42" y="10.16"/>
<pinref part="VIA_2" gate="_EARTH" pin="S"/>
<junction x="-58.42" y="7.62"/>
<pinref part="VIA_3" gate="_EARTH" pin="S"/>
<junction x="-58.42" y="5.08"/>
<pinref part="VIA_4" gate="_EARTH" pin="S"/>
<junction x="-58.42" y="2.54"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="SUPPLY68" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="GND"/>
<pinref part="SUPPLY69" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="SUPPLY70" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="SUPPLY71" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<pinref part="SUPPLY73" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="2"/>
<pinref part="SUPPLY75" gate="GND" pin="GND"/>
<wire x1="162.56" y1="81.28" x2="160.02" y2="81.28" width="0.1524" layer="91"/>
<wire x1="160.02" y1="81.28" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="2"/>
<wire x1="160.02" y1="66.04" x2="160.02" y2="60.96" width="0.1524" layer="91"/>
<wire x1="162.56" y1="66.04" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="160.02" y="66.04"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TRIM"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="129.54" x2="-48.26" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DVCC" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="+"/>
<pinref part="IC3" gate="G$1" pin="OUT"/>
<wire x1="-12.7" y1="139.7" x2="-12.7" y2="142.24" width="0.1524" layer="91"/>
<pinref part="P+1" gate="G$1" pin="VCC_1"/>
<wire x1="-12.7" y1="144.78" x2="-12.7" y2="142.24" width="0.1524" layer="91"/>
<junction x="-12.7" y="142.24"/>
<label x="-10.16" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P+3" gate="G$1" pin="VCC_1"/>
<wire x1="5.08" y1="127" x2="5.08" y2="119.38" width="0.1524" layer="91"/>
<wire x1="5.08" y1="119.38" x2="12.7" y2="119.38" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="VDD"/>
<wire x1="12.7" y1="119.38" x2="22.86" y2="119.38" width="0.1524" layer="91"/>
<wire x1="22.86" y1="119.38" x2="22.86" y2="127" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="!RST"/>
<wire x1="12.7" y1="127" x2="12.7" y2="119.38" width="0.1524" layer="91"/>
<junction x="12.7" y="119.38"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="22.86" y1="116.84" x2="22.86" y2="119.38" width="0.1524" layer="91"/>
<junction x="22.86" y="119.38"/>
</segment>
<segment>
<pinref part="IC5" gate="1" pin="VCC@1"/>
<pinref part="P+5" gate="G$1" pin="VCC_1"/>
<wire x1="-71.12" y1="58.42" x2="-81.28" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="VCC@2"/>
<wire x1="-81.28" y1="58.42" x2="-83.82" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="55.88" x2="-71.12" y2="58.42" width="0.1524" layer="91"/>
<junction x="-71.12" y="58.42"/>
<pinref part="IC5" gate="1" pin="AVCC"/>
<wire x1="-71.12" y1="50.8" x2="-71.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="-71.12" y="55.88"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="55.88" x2="-81.28" y2="58.42" width="0.1524" layer="91"/>
<junction x="-81.28" y="58.42"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-86.36" y1="55.88" x2="-86.36" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="58.42" x2="-83.82" y2="58.42" width="0.1524" layer="91"/>
<junction x="-83.82" y="58.42"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="55.88" x2="-91.44" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="58.42" x2="-86.36" y2="58.42" width="0.1524" layer="91"/>
<junction x="-86.36" y="58.42"/>
</segment>
<segment>
<pinref part="P+6" gate="G$1" pin="VCC_1"/>
<pinref part="CON3" gate="PWR" pin="VCC"/>
<wire x1="-20.32" y1="17.78" x2="-20.32" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="CE"/>
<wire x1="53.34" y1="71.12" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="DVDD"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<wire x1="50.8" y1="88.9" x2="71.12" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="PDB"/>
<wire x1="53.34" y1="66.04" x2="50.8" y2="66.04" width="0.1524" layer="91"/>
<wire x1="50.8" y1="66.04" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<junction x="50.8" y="71.12"/>
<pinref part="P+4" gate="G$1" pin="VCC_1"/>
<wire x1="50.8" y1="93.98" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="50.8" y="88.9"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="50.8" y1="91.44" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="50.8" y="91.44"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="38.1" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<junction x="43.18" y="91.44"/>
</segment>
<segment>
<pinref part="P+9" gate="G$1" pin="VCC_1"/>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="0" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="0" x2="-10.16" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="S"/>
<wire x1="20.32" y1="-5.08" x2="20.32" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-7.62" x2="7.62" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-7.62" x2="-5.08" y2="-7.62" width="0.1524" layer="91"/>
<junction x="7.62" y="-7.62"/>
<pinref part="U$1" gate="G$1" pin="S"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-7.62" x2="-5.08" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-5.08" y="-7.62"/>
<pinref part="U$4" gate="G$1" pin="S"/>
</segment>
<segment>
<pinref part="P+10" gate="G$1" pin="VCC_1"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="66.04" y1="22.86" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AVCC" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="OUT"/>
<pinref part="C8" gate="G$1" pin="+"/>
<wire x1="-12.7" y1="121.92" x2="-10.16" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="121.92" x2="-10.16" y2="119.38" width="0.1524" layer="91"/>
<pinref part="P+2" gate="G$1" pin="VCC_2"/>
<wire x1="-7.62" y1="124.46" x2="-10.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="124.46" x2="-10.16" y2="121.92" width="0.1524" layer="91"/>
<junction x="-10.16" y="121.92"/>
<label x="-7.62" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P+7" gate="G$1" pin="VCC_2"/>
<wire x1="71.12" y1="106.68" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
<wire x1="71.12" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="SDVDD"/>
<wire x1="73.66" y1="101.6" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VP"/>
<wire x1="76.2" y1="88.9" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<junction x="73.66" y="101.6"/>
<pinref part="IC1" gate="G$1" pin="AVDD"/>
<wire x1="78.74" y1="88.9" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="101.6" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
<junction x="76.2" y="101.6"/>
<label x="68.58" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P+8" gate="G$1" pin="VCC_2"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="83.82" y1="111.76" x2="83.82" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="83.82" y1="109.22" x2="88.9" y2="109.22" width="0.1524" layer="91"/>
<junction x="83.82" y="109.22"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="88.9" y1="109.22" x2="93.98" y2="109.22" width="0.1524" layer="91"/>
<junction x="88.9" y="109.22"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="93.98" y1="109.22" x2="99.06" y2="109.22" width="0.1524" layer="91"/>
<junction x="93.98" y="109.22"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="99.06" y1="109.22" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<junction x="99.06" y="109.22"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="104.14" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<junction x="104.14" y="109.22"/>
<label x="88.9" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PB3(MOSI/OC2A/PCINT3)"/>
<pinref part="LCD1" gate="G$1" pin="SDA"/>
<wire x1="-10.16" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<wire x1="20.32" y1="33.02" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="DATA"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="127" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<junction x="20.32" y="76.2"/>
<pinref part="CON3" gate="MISO" pin="S"/>
<wire x1="22.86" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<junction x="20.32" y="33.02"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PB5(SCK/PCINT5)"/>
<pinref part="LCD1" gate="G$1" pin="SCL"/>
<wire x1="-10.16" y1="27.94" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="CLK"/>
<wire x1="17.78" y1="78.74" x2="17.78" y2="127" width="0.1524" layer="91"/>
<wire x1="53.34" y1="78.74" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<junction x="17.78" y="78.74"/>
<pinref part="CON3" gate="SCK" pin="S"/>
<wire x1="22.86" y1="27.94" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<junction x="17.78" y="27.94"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PC6(/RESET/PCINT14)"/>
<pinref part="CON3" gate="RESET" pin="S"/>
<wire x1="-73.66" y1="86.36" x2="-71.12" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="CON3" gate="MOSI" pin="S"/>
<pinref part="IC5" gate="1" pin="PB4(MISO/PCINT4)"/>
<wire x1="22.86" y1="30.48" x2="-10.16" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PB2(SS/OC1B/PCINT2)"/>
<pinref part="LCD1" gate="G$1" pin="RS"/>
<wire x1="-10.16" y1="35.56" x2="15.24" y2="35.56" width="0.1524" layer="91"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PB1(OC1A/PCINT1)"/>
<wire x1="-10.16" y1="38.1" x2="12.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="12.7" y1="38.1" x2="12.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="12.7" y1="109.22" x2="10.16" y2="109.22" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="!CS1B"/>
<wire x1="10.16" y1="109.22" x2="10.16" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LE" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LE"/>
<wire x1="22.86" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="22.86" y1="55.88" x2="22.86" y2="73.66" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="PB0(ICP1/CLKO/PCINT0)"/>
<wire x1="-10.16" y1="40.64" x2="22.86" y2="40.64" width="0.1524" layer="91"/>
<wire x1="22.86" y1="40.64" x2="22.86" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="CON4" gate="_RX" pin="S"/>
<pinref part="IC5" gate="1" pin="PD0(RXD/PCINT16)"/>
<wire x1="5.08" y1="63.5" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PD1(TXD/PCINT17)"/>
<pinref part="CON4" gate="_TX" pin="S"/>
<wire x1="-10.16" y1="60.96" x2="5.08" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="VOUT"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="27.94" y1="127" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="C1-"/>
<wire x1="30.48" y1="127" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="30.48" y1="109.22" x2="33.02" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="LCD1" gate="G$1" pin="C1+"/>
<wire x1="33.02" y1="116.84" x2="33.02" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="C2+"/>
<wire x1="35.56" y1="127" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="35.56" y1="109.22" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="LCD1" gate="G$1" pin="C2-"/>
<wire x1="38.1" y1="116.84" x2="38.1" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V1"/>
<wire x1="40.64" y1="127" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="40.64" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V2"/>
<wire x1="43.18" y1="127" x2="43.18" y2="119.38" width="0.1524" layer="91"/>
<wire x1="43.18" y1="119.38" x2="48.26" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="48.26" y1="119.38" x2="48.26" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V3"/>
<wire x1="45.72" y1="127" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="45.72" y1="121.92" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<wire x1="53.34" y1="121.92" x2="53.34" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V4"/>
<wire x1="48.26" y1="127" x2="48.26" y2="124.46" width="0.1524" layer="91"/>
<wire x1="48.26" y1="124.46" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="55.88" y1="124.46" x2="58.42" y2="124.46" width="0.1524" layer="91"/>
<wire x1="58.42" y1="124.46" x2="58.42" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V5"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="50.8" y1="127" x2="63.5" y2="127" width="0.1524" layer="91"/>
<wire x1="63.5" y1="127" x2="63.5" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="RSET"/>
<wire x1="53.34" y1="55.88" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="43.18" y1="53.34" x2="48.26" y2="53.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="53.34" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<junction x="48.26" y="53.34"/>
<pinref part="IC1" gate="G$1" pin="VREF"/>
<wire x1="48.26" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VCOM"/>
<wire x1="53.34" y1="63.5" x2="45.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="45.72" y1="63.5" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<wire x1="45.72" y1="55.88" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="38.1" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="33.02" y1="55.88" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="38.1" y1="53.34" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
<junction x="38.1" y="55.88"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TEMP"/>
<wire x1="81.28" y1="43.18" x2="81.28" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="81.28" y1="40.64" x2="86.36" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="86.36" y1="40.64" x2="91.44" y2="40.64" width="0.1524" layer="91"/>
<junction x="86.36" y="40.64"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="22.86" y1="5.08" x2="22.86" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="12.7" y1="17.78" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
<junction x="10.16" y="17.78"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="22.86" y1="17.78" x2="27.94" y2="17.78" width="0.1524" layer="91"/>
<wire x1="27.94" y1="17.78" x2="27.94" y2="12.7" width="0.1524" layer="91"/>
<junction x="22.86" y="17.78"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="0" y1="17.78" x2="-2.54" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="17.78" x2="-2.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="-2.54" y="17.78"/>
<pinref part="IC5" gate="1" pin="ADC6"/>
<wire x1="-2.54" y1="71.12" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="1"/>
<wire x1="43.18" y1="12.7" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<wire x1="43.18" y1="25.4" x2="2.54" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="PD6(AIN0/OC0A/PCINT22)"/>
<wire x1="-10.16" y1="48.26" x2="2.54" y2="48.26" width="0.1524" layer="91"/>
<wire x1="2.54" y1="48.26" x2="2.54" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="2"/>
<wire x1="43.18" y1="7.62" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="7.62" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<wire x1="40.64" y1="22.86" x2="0" y2="22.86" width="0.1524" layer="91"/>
<wire x1="0" y1="22.86" x2="0" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="PD7(AIN1/PCINT23)"/>
<wire x1="0" y1="45.72" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="IC5" gate="1" pin="AREF"/>
<wire x1="-71.12" y1="48.26" x2="-73.66" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="48.26" x2="-73.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="-114.3" y1="78.74" x2="-81.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="78.74" x2="-81.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="91.44" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="91.44" x2="-7.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="91.44" x2="-7.62" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="ADC7"/>
<wire x1="-7.62" y1="68.58" x2="-10.16" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="TR1" gate="G$1" pin="B"/>
<wire x1="-119.38" y1="63.5" x2="-111.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="63.5" x2="-111.76" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="76.2" x2="-111.76" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="81.28" x2="-83.82" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="81.28" x2="-83.82" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="93.98" x2="-7.62" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="93.98" x2="-7.62" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="93.98" x2="-5.08" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="PC0(ADC0/PCINT8)"/>
<wire x1="-5.08" y1="86.36" x2="-10.16" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="OS1" gate="A" pin="OUT"/>
<wire x1="109.22" y1="15.24" x2="111.76" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="REFIN"/>
<wire x1="99.06" y1="55.88" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="101.6" y1="55.88" x2="101.6" y2="27.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="27.94" x2="104.14" y2="27.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="27.94" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
<wire x1="111.76" y1="27.94" x2="111.76" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC5" gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)"/>
<wire x1="-71.12" y1="71.12" x2="-96.52" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="71.12" x2="-96.52" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-17.78" x2="111.76" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-17.78" x2="111.76" y2="15.24" width="0.1524" layer="91"/>
<junction x="111.76" y="15.24"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VTUNE"/>
<wire x1="99.06" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="127" y1="63.5" x2="127" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<junction x="127" y="58.42"/>
<wire x1="127" y1="58.42" x2="127" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CPOUT"/>
<wire x1="99.06" y1="60.96" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="106.68" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<junction x="106.68" y="58.42"/>
<junction x="114.3" y="58.42"/>
<wire x1="114.3" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="114.3" y1="48.26" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="104.14" y1="38.1" x2="111.76" y2="38.1" width="0.1524" layer="91"/>
<wire x1="111.76" y1="38.1" x2="111.76" y2="48.26" width="0.1524" layer="91"/>
<wire x1="111.76" y1="48.26" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
<junction x="114.3" y="48.26"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="104.14" y1="48.26" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="SW"/>
<wire x1="104.14" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="OS1" gate="A" pin="VDD"/>
<wire x1="83.82" y1="20.32" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="78.74" y1="20.32" x2="78.74" y2="17.78" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="76.2" y1="20.32" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<junction x="78.74" y="20.32"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RFOUTA+"/>
<wire x1="99.06" y1="78.74" x2="104.14" y2="78.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="78.74" x2="104.14" y2="83.82" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="104.14" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="111.76" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<junction x="109.22" y="83.82"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="1"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="111.76" y1="81.28" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="81.28" x2="106.68" y2="81.28" width="0.1524" layer="91"/>
<junction x="109.22" y="81.28"/>
<wire x1="106.68" y1="81.28" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="RFOUTA-"/>
<wire x1="106.68" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="101.6" y1="76.2" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
<wire x1="101.6" y1="83.82" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="101.6" y="76.2"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="119.38" y1="83.82" x2="121.92" y2="83.82" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="121.92" y1="81.28" x2="121.92" y2="83.82" width="0.1524" layer="91"/>
<junction x="121.92" y="83.82"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RFOUTB-"/>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="99.06" y1="68.58" x2="99.06" y2="66.04" width="0.1524" layer="91"/>
<wire x1="99.06" y1="66.04" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="134.62" y1="66.04" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="137.16" y1="63.5" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<junction x="137.16" y="66.04"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="134.62" y1="71.12" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<junction x="134.62" y="66.04"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RFOUTB+"/>
<wire x1="99.06" y1="71.12" x2="101.6" y2="71.12" width="0.1524" layer="91"/>
<wire x1="101.6" y1="71.12" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="101.6" y1="68.58" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="137.16" y1="71.12" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<junction x="137.16" y="68.58"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="147.32" y1="66.04" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="147.32" y1="68.58" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="149.86" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<junction x="147.32" y="68.58"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="101.6" y1="93.98" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="109.22" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<junction x="109.22" y="93.98"/>
<wire x1="114.3" y1="93.98" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<junction x="114.3" y="93.98"/>
<pinref part="L5" gate="G$1" pin="1"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="134.62" y1="81.28" x2="137.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="137.16" y1="81.28" x2="139.7" y2="81.28" width="0.1524" layer="91"/>
<junction x="137.16" y="81.28"/>
<wire x1="114.3" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="137.16" y1="101.6" x2="137.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="2"/>
<wire x1="114.3" y1="129.54" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<junction x="114.3" y="101.6"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="VOUT"/>
<pinref part="IC1" gate="G$1" pin="VVCO"/>
<wire x1="-12.7" y1="170.18" x2="-5.08" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="170.18" x2="81.28" y2="170.18" width="0.1524" layer="91"/>
<wire x1="81.28" y1="170.18" x2="81.28" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="1"/>
<junction x="-5.08" y="170.18"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="81.28" y1="129.54" x2="81.28" y2="88.9" width="0.1524" layer="91"/>
<wire x1="83.82" y1="129.54" x2="81.28" y2="129.54" width="0.1524" layer="91"/>
<junction x="81.28" y="129.54"/>
<wire x1="88.9" y1="129.54" x2="86.36" y2="129.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<junction x="83.82" y="129.54"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="93.98" y1="129.54" x2="88.9" y2="129.54" width="0.1524" layer="91"/>
<wire x1="99.06" y1="129.54" x2="93.98" y2="129.54" width="0.1524" layer="91"/>
<junction x="93.98" y="129.54"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="104.14" y1="129.54" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="1"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="162.56" y1="83.82" x2="129.54" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="X3" gate="G$1" pin="1"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="162.56" y1="68.58" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="P"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PC3(ADC3/PCINT11)"/>
<pinref part="CON6" gate="ENABLE" pin="S"/>
<wire x1="0" y1="78.74" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PC2(ADC2/PCINT10)"/>
<pinref part="CON6" gate="ACSEL" pin="S"/>
<wire x1="0" y1="81.28" x2="-10.16" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PC1(ADC1/PCINT9)"/>
<pinref part="CON6" gate="ACPRES" pin="S"/>
<wire x1="0" y1="83.82" x2="-10.16" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PC4(ADC4/SDA/PCINT12)"/>
<pinref part="CON6" gate="IBAT" pin="S"/>
<wire x1="0" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PC5(ADC5/SCL/PCINT13)"/>
<pinref part="CON6" gate="ALARM" pin="S"/>
<wire x1="0" y1="73.66" x2="-10.16" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="430_EN" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PD5(T1/OC0B/PCINT21)"/>
<pinref part="CON5" gate="B0" pin="S"/>
<wire x1="5.08" y1="50.8" x2="-10.16" y2="50.8" width="0.1524" layer="91"/>
<label x="-2.54" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="1200_EN" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PD4(T0/XCK/PCINT20)"/>
<pinref part="CON5" gate="B1" pin="S"/>
<wire x1="5.08" y1="53.34" x2="-10.16" y2="53.34" width="0.1524" layer="91"/>
<label x="-2.54" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_EN" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PD3(INT1/OC2B/PCINT19)"/>
<pinref part="CON5" gate="RXEN" pin="S"/>
<wire x1="5.08" y1="55.88" x2="-10.16" y2="55.88" width="0.1524" layer="91"/>
<label x="-2.54" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_EN" class="0">
<segment>
<pinref part="IC5" gate="1" pin="PD2(INT0/PCINT18)"/>
<pinref part="CON5" gate="TXEN" pin="S"/>
<wire x1="5.08" y1="58.42" x2="-10.16" y2="58.42" width="0.1524" layer="91"/>
<label x="-2.54" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
